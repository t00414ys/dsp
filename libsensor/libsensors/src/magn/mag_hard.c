/*!******************************************************************************
 * @file    mag_hard.c
 * @brief   utility code for Hard-Iron Comp
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "frizz_mem.h"
#include "frizz_const.h"
#include "matrix.h"
#include "frizz_type.h"
#include "frizz_math.h"
#include "mag_hard.h"

#define SAMPLE_NUM 32				// number of samples for approximation
#define SAMPLE_THR 16				// sample of lower limit (threshold)
#define LPF_NUM 8					// remove chattering
#define THL_LPF_POW (0.4*0.4)		// allowable distance for removing chattering
#define THL_SAMPLE_POW (0.2*0.2)	// minimum distance of sample

typedef struct {
	int			cnt;				// number of samples
	frizz_fp4w	lpf[LPF_NUM];		// 1 x 3: last time data

	// for elimination of Hard-Iron
	Matrix		*tap;	// SAMPLE_NUM x 4: sample data
	Matrix		*pow;	// SAMPLE_NUM x 1: power of sample
	Matrix		*bet;	// 4x1: beta

	Matrix		*Inv4x4;			// 4 x 4:

	Matrix		*T0_4x1;			// 9 x 1: Temp
	Matrix		*T0_4x4;			// 4 x 4: Temp
} HardIron_t;

static void vec_set( frizz_fp4w *vec, frizz_fp *data )
{
	frizz_fp *dst = ( frizz_fp* )vec;
	dst[0] = data[0];
	dst[1] = data[1];
	dst[2] = data[2];
	dst[3] = FRIZZ_CONST_ZERO;
}

static frizz_fp calc_vec_pow( frizz_fp4w *vec )
{
	frizz_fp4w pow;
	frizz_fp norm;
	frizz_fp *p = ( frizz_fp* )&pow;
	pow = *vec * *vec;
	p[3] = FRIZZ_CONST_ZERO;
	norm = frizz_tie_vreduc( pow );
	return norm;
}

/**
 * @brief create HardIron
 *
 * @return object
 */
HardIron_h HardIron_New( void )
{
	HardIron_t *o = ( HardIron_t* )frizz_malloc( sizeof( HardIron_t ) );
	if( o != NULL ) {
		memset(o,0,sizeof(HardIron_t));
		// Matrix
		o->tap = alloc_matrix( SAMPLE_NUM, 4 );
		o->pow = alloc_matrix( SAMPLE_NUM, 1 );
		o->bet = alloc_matrix( 4, 1 );

		o->Inv4x4 = alloc_matrix( 4, 4 );

		o->T0_4x1 = alloc_matrix( 4, 1 );
		o->T0_4x4 = alloc_matrix( 4, 4 );

		HardIron_Init( o );
	}

	return o;
}

/**
 * @brief initialize HardIron
 *
 * @param [in]	h HardIron Handle
 */
void HardIron_Init( HardIron_h h )
{
	HardIron_t* o = ( HardIron_t* )h;
	o->cnt = 0;
}

/**
 * @brief destroy HardIron
 *
 * @param [in]	h HardIron Handle
 */
void HardIron_Delete( HardIron_h h )
{
	HardIron_t* o = ( HardIron_t* )h;

	// Matrix
	free_matrix( o->tap );
	free_matrix( o->pow );
	free_matrix( o->bet );

	free_matrix( o->Inv4x4 );

	free_matrix( o->T0_4x1 );
	free_matrix( o->T0_4x4 );

	frizz_free( o );
}

/**
 * @brief set calibration data
 *
 * @param [in]	h HardIron Handle
 * @param mag[3] magnet data
 *
 * @return
 */
int HardIron_CalibPush( HardIron_h h, frizz_fp mag[3] )
{
	frizz_fp4w vec, dif;
	HardIron_t* o = ( HardIron_t* )h;
	frizz_fp pow;
	int i;

	// set Vector
	vec_set( &vec, mag );
	// remove pulse
	for( i = 0; i < LPF_NUM; i++ ) {
		dif = vec - o->lpf[i];
		pow = calc_vec_pow( &dif );
		if( as_frizz_fp( THL_LPF_POW ) < pow ) {
			break;
		}
	}
	// LPF move & set
	for( i = LPF_NUM - 1;  0 < i; i-- ) {
		o->lpf[i] = o->lpf[i - 1];
	}
	o->lpf[0] = vec;
	if( as_frizz_fp( THL_LPF_POW ) < pow ) {
		// noise & judgment
		return 0;
	}
	// select
	for( i = 0; i < o->cnt; i++ ) {
		dif = vec - o->tap->vec[i][0];
		pow = calc_vec_pow( &dif );
		if( pow < as_frizz_fp( THL_SAMPLE_POW ) ) {
			// near distance
			return 0;
		}
	}
	// sample move
	for( i = SAMPLE_NUM - 1; 0 < i; i-- ) {
		o->tap->vec[i][0] = o->tap->vec[i - 1][0];
		o->pow->data[i][0] = o->pow->data[i - 1][0];
	}
	// sample decision
	o->tap->vec[0][0] = vec;
	o->tap->data[0][3] = FRIZZ_CONST_ONE;
	o->pow->data[0][0] = calc_vec_pow( &vec );
	if( o->cnt < SAMPLE_NUM ) {
		o->cnt++;
	}

	// remain sample
	return ( SAMPLE_THR <= o->cnt );
}

/**
 * @brief calibration calculation
 *
 * @param [in]	h HardIron Handle
 * @param [out]	hard Hard-Iron Matrix: 3 x 1
 *
 * @return -1:can't calculate, 0:success
 */
int HardIron_CalibCalc( HardIron_h h, Matrix *hard )
{
	HardIron_t* o = ( HardIron_t* )h;

	int i;
	// setting TAP & POW
	for( i = o->cnt; i < SAMPLE_NUM; i++ ) {
		o->tap->vec[i][0] = o->tap->vec[i - ( SAMPLE_NUM - o->cnt )][0];
	}
	// calculate
	mul_trmatrix2( o->tap, o->tap, o->T0_4x4 );
	if( invert_matrix( o->T0_4x4, o->Inv4x4, o->T0_4x4 ) == 0 ) {
		frizz_fp **b = o->bet->data;
		mul_trmatrix2( o->tap, o->pow, o->T0_4x1 );
		mul_matrix( o->Inv4x4, o->T0_4x1, o->bet );
		scale_matrix( o->bet, FRIZZ_CONST_HALF );
		hard->data[0][0] = b[0][0];
		hard->data[1][0] = b[1][0];
		hard->data[2][0] = b[2][0];
		//		printf("%e %e %e\n", b[0][0], b[1][0], b[2][0]);
		return 0;
	}
	return -1;
}

