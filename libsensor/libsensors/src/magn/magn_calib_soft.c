/*!******************************************************************************
 * @file    magn_calib_soft.c
 * @brief   virtual magnet calibration soft sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "mag_util.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "libsensors_id.h"
#include "magn_calib_soft.h"
#include "if/magn_param_if.h"
#include "if/magn_calib_soft_if.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

#define UNCALIB_SOFT_IRON	// not apply calibration for soft-iron

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[2];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_magn;
	sensor_if_get_t		*p_para;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp4w			data;
	// parameter
	frizz_fp4w_t		mat[3];
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 3;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_MAGNET_RAW ) {
		g_device.p_magn = gettor;
	}
	if( gettor->id() == SENSOR_ID_MAGNET_PARAMETER ) {
		g_device.p_para = gettor;
	}
}

static void notify_updated( unsigned char sen_id, SENSOR_NOTIFY ntfy );
static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_magn->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_magn->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_para->id(), g_device.f_active );
		if( f_active ) {
			notify_updated( SENSOR_ID_MAGNET_PARAMETER, SENSOR_NOTIFY_UPDATED_DATA );
		}
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	if( g_device.tick != tick ) {
		g_device.tick = tick;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_magn->id(), g_device.tick );
	}
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void notify_updated( unsigned char sen_id, SENSOR_NOTIFY ntfy )
{
	if( sen_id == SENSOR_ID_MAGNET_PARAMETER ) {
		magnet_parameter_data_t *p;
		g_device.p_para->data( ( void** )&p, &g_device.ts );
		if( p->f_calib ) {
			// soft iron calibration if you need
			g_device.mat[0].z[0] = p->param[0];
			g_device.mat[0].z[1] = p->param[1];
			g_device.mat[0].z[2] = p->param[2];
			g_device.mat[1].z[0] = p->param[3];
			g_device.mat[1].z[1] = p->param[4];
			g_device.mat[1].z[2] = p->param[5];
			g_device.mat[2].z[0] = p->param[6];
			g_device.mat[2].z[1] = p->param[7];
			g_device.mat[2].z[2] = p->param[8];
		}
	}
}

static int calculate( void )
{
	frizz_fp4w_t data;
	frizz_fp4w *p;
	g_device.p_magn->data( ( void** )&p, &g_device.ts );
#ifndef UNCALIB_SOFT_IRON
	// soft iron calibration if you need
	data.z[0] = frizz_tie_vreduc( *p * g_device.mat[0].w );
	data.z[1] = frizz_tie_vreduc( *p * g_device.mat[1].w );
	data.z[2] = frizz_tie_vreduc( *p * g_device.mat[2].w );
#else
	data.w = *p;
#endif
	g_device.data = data.w;
	g_device.f_need = 0;
	return 1;
}

sensor_if_t* DEF_INIT( magn_calib_soft )( void )
{
	// ID
	g_device.id = MAGNET_CALIB_SOFT_ID;
	g_device.par_ls[0] = SENSOR_ID_MAGNET_RAW;
	g_device.par_ls[1] = SENSOR_ID_MAGNET_PARAMETER;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = notify_updated;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.tick = 20;
	g_device.f_need = 0;
	g_device.ts = 0;
	// data
	g_device.data *= as_frizz_fp( 0.0f );
	// parameter
	g_device.mat[0].z[0] =
		g_device.mat[1].z[1] =
			g_device.mat[2].z[2] = as_frizz_fp( 1.0f );
	g_device.mat[0].z[1] =
		g_device.mat[0].z[2] =
			g_device.mat[0].z[3] =
				g_device.mat[1].z[0] =
					g_device.mat[1].z[2] =
						g_device.mat[1].z[3] =
							g_device.mat[2].z[0] =
								g_device.mat[2].z[1] =
									g_device.mat[2].z[3] = as_frizz_fp( 0.0f );

	return &( g_device.pif );
}

