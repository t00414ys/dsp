/*!******************************************************************************
 * @file    gyro_lpf.c
 * @brief   virtual gyro lpf sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdlib.h>
#include "frizz_type.h"
#include "frizz_math.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "libsensors_id.h"
#include "gyro_lpf.h"
#include "if/gyro_lpf_if.h"
#include "sensor_statistics.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(1)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#include "frizz_debug.h"
#define POS_GYROLPF				 POS_MACRO(GYRO_LPF_ID, 0x01)		// File No. 01

#define DEF_INIT(x) x ## _init

#define GYRO_LPF_INTERVAL_TICK	50	// 20 Hz

#define GYRO_ABS_THRESHOLD	as_frizz_fp(0.001)
#define ACCL_ABS_THRESHOLD	as_frizz_fp(0.001)
#define CALC_WAIT_TIME 1000 * 60 * 5	//Latency after outputting data[msec]
#define GYRO_STAT_BUFF_NUM 64		// 64 sample -> 3.2 sec window
#define ACCL_STAT_BUFF_NUM 32		// 32 sample -> 1.6 sec window
#define GYRO_CHECK_COUNT_NUM 5		// checking count to get parameters

typedef enum {
	GYRO_LPF_STAT_DOING = 0,		///< calculating for calibration
	GYRO_LPF_STAT_DONE,			///< done about calibration
} gyro_lpf_stat_e;

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[2];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_gyro;
	sensor_if_get_t		*p_accl;
	// status
	int					f_active;
	int					f_need;
	unsigned int		ts;
	gyro_lpf_stat_e		stat;
	unsigned int 		last_calib_ts;
	// statistics data
	sensor_stat_t	stat_data_gyro;
	sensor_stat_t	stat_data_accl;
	frizz_fp4w		accl_buff[ACCL_STAT_BUFF_NUM];
	frizz_fp4w		gyro_buff[GYRO_STAT_BUFF_NUM];
	// threshold
	frizz_fp		gyro_var_com_threshold;
	frizz_fp		accl_var_com_threshold;
	// median buffering
	frizz_fp4w		gyro_avg_buff;
	unsigned int	continuous_count;
	// data
	union {
		gyro_lpf_data_t		d;
		frizz_fp4w			w;
	} data;
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.last_calib_ts;
	}
	return sizeof( g_device.data ) / sizeof( int );
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_GYRO_RAW ) {
		g_device.p_gyro = gettor;
	}
	if( gettor->id() == SENSOR_ID_ACCEL_RAW ) {
		g_device.p_accl = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_accl->id(), GYRO_LPF_INTERVAL_TICK );
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_gyro->id(), GYRO_LPF_INTERVAL_TICK );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_accl->id(), g_device.f_active );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_gyro->id(), g_device.f_active );
		// init module
		sensor_stat_init( &g_device.stat_data_gyro, g_device.gyro_buff, GYRO_STAT_BUFF_NUM );
		sensor_stat_init( &g_device.stat_data_accl, g_device.accl_buff, ACCL_STAT_BUFF_NUM );
		g_device.stat = GYRO_LPF_STAT_DOING;
		g_device.continuous_count = 0;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	return 0;
}

static int get_interval( void )
{
	return 0;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION: {
			ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
			break;
		}
	case SET_DRIFT_CORRECTION_PARAM: {
			if( param == NULL ) {
				ret = -2;
			} else {
				frizz_fp* drift_calib_param = ( frizz_fp* )param;
				g_device.data.d.data[0] = drift_calib_param[0];
				g_device.data.d.data[1] = drift_calib_param[1];
				g_device.data.d.data[2] = drift_calib_param[2];
				g_device.data.d.degree = drift_calib_param[3];
				// init module
				if( g_device.stat != GYRO_LPF_STAT_DOING ) {
					sensor_stat_init( &g_device.stat_data_gyro, g_device.gyro_buff, GYRO_STAT_BUFF_NUM );
					sensor_stat_init( &g_device.stat_data_accl, g_device.accl_buff, ACCL_STAT_BUFF_NUM );
					g_device.continuous_count = 0;
					g_device.stat = GYRO_LPF_STAT_DOING;
				}
				ret = 0;
			}
			break;
		}
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int interval = GYRO_LPF_INTERVAL_TICK;
	g_device.ts = ts;
	if( g_device.stat == GYRO_LPF_STAT_DONE ) {
		int diff;
		diff = ts - g_device.last_calib_ts;
		if( CALC_WAIT_TIME <= diff ) {
			// init module
			sensor_stat_init( &g_device.stat_data_gyro, g_device.gyro_buff, GYRO_STAT_BUFF_NUM );
			sensor_stat_init( &g_device.stat_data_accl, g_device.accl_buff, ACCL_STAT_BUFF_NUM );
			g_device.continuous_count = 0;
			g_device.stat = GYRO_LPF_STAT_DOING;
		} else if( 0 < diff ) {
			// set next time for sleeping
			interval = CALC_WAIT_TIME - diff;
		}
	}
	if( g_device.stat == GYRO_LPF_STAT_DOING ) {
		g_device.f_need = 1;
	}
	return ts + interval;
}

static const sensor_stat_data_t* check_threshold( sensor_stat_t *st, frizz_fp thd )
{
	const sensor_stat_data_t *o = NULL;
	if( sensor_stat_is_available( st ) != 0 ) {
		frizz_fp var;
		o = sensor_stat_get_data( st );
		var = frizz_tie_vreduc( o->var_com );
		if( thd <= var ) {
			o = NULL;
		}
	}
	return o;
}

static int calculate( void )
{
	const sensor_stat_data_t *gyro_stat;
	frizz_fp4w *gyro;
	frizz_fp4w *accl;
	int ret = 0;

	g_device.p_gyro->data( ( void** )&gyro, &g_device.ts );
	g_device.p_accl->data( ( void** )&accl, 0 );

	//calculate variance
	sensor_stat_push_data( &g_device.stat_data_gyro, *gyro );
	sensor_stat_push_data( &g_device.stat_data_accl, *accl );

	// check threshold
	gyro_stat = check_threshold( &g_device.stat_data_gyro, g_device.gyro_var_com_threshold );
	if( gyro_stat != NULL ) {
		if( check_threshold( &g_device.stat_data_accl, g_device.accl_var_com_threshold ) != NULL ) {
			g_device.continuous_count++;
			if( g_device.continuous_count == ( ( GYRO_CHECK_COUNT_NUM + 1 ) >> 1 ) ) {
				g_device.gyro_avg_buff = gyro_stat->avg_vec;
			} else if( GYRO_CHECK_COUNT_NUM <= g_device.continuous_count ) {
				g_device.data.w = g_device.gyro_avg_buff;
				g_device.last_calib_ts = g_device.ts;
				g_device.stat = GYRO_LPF_STAT_DONE;
				ret = 1;
			}
		} else {
			// clear caused by some motion
			g_device.continuous_count = 0;
		}
	} else {
		// clear caused by some motion
		g_device.continuous_count = 0;
	}
	g_device.f_need = 0;
	return ret;
}

sensor_if_t* DEF_INIT( gyro_lpf )( void )
{
	// ID
	g_device.id = GYRO_LPF_ID;
	g_device.par_ls[0] = SENSOR_ID_GYRO_RAW;
	g_device.par_ls[1] = SENSOR_ID_ACCEL_RAW;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.last_calib_ts = 0;
	g_device.stat = GYRO_LPF_STAT_DOING;
	g_device.accl_var_com_threshold = ACCL_ABS_THRESHOLD;
	g_device.gyro_var_com_threshold = GYRO_ABS_THRESHOLD;
	// data
	g_device.data.d.data[0] =
		g_device.data.d.data[1] =
			g_device.data.d.data[2] = as_frizz_fp( 0.0f );
	g_device.data.d.degree = as_frizz_fp( 0.0f );
	// init module
	sensor_stat_init( &g_device.stat_data_gyro, g_device.gyro_buff, GYRO_STAT_BUFF_NUM );
	sensor_stat_init( &g_device.stat_data_accl, g_device.accl_buff, ACCL_STAT_BUFF_NUM );
	return &( g_device.pif );
}
