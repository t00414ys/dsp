/*!******************************************************************************
 * @file    rot_grav_vec.h
 * @brief   virtual rotation gravity vector sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ROTATION_GRAVITY_VECTOR ROTATION GRAVITY VECTOR
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief rotation gravity vector sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief This sensor for taking out rotation vector (quaternion).<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_ROTATION_GRAVITY_VECTOR<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_ACCEL_RAW, #SENSOR_ID_GYRO_RAW<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_ROTATION_GRAVITY_VECTOR" color=red];
 *		struct2 [label="SENSOR_ID_ACCEL_RAW" color=blue];
 *		struct3 [label="SENSOR_ID_GYRO_RAW" color=blue];
 *		struct2 -> struct1; struct3 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)<br>
 */

#ifndef __ROT_GRAV_VEC_H__
#define __ROT_GRAV_VEC_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup ROTATION_GRAVITY_VECTOR ROTATION GRAVITY VECTOR
 *  @{
 */

/**
 *@brief	Initialize rotation gravity vector sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 */
sensor_if_t* rot_grav_vec_init( void );

/** @} */
#endif
