/*!******************************************************************************
 * @file    group_rotation.h
 * @brief   group of virtual rotation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GROUP_ROTATION_H__
#define __GROUP_ROTATION_H__

#include "orientation.h"
#include "rot_grav_vec.h"
#include "rot_lpf_vec.h"
#include "rot_vec.h"
#include "gravity.h"

#endif
