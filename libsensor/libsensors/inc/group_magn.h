/*!******************************************************************************
 * @file    group_magn.h
 * @brief   group of virtual magnet sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief magn_calib_hard, magn_calib_soft, magn_lpf, magn_param and magn_uncalib sensor header group
 */

#ifndef __GROUP_MAGNET_H__
#define __GROUP_MAGNET_H__

#include "magn_calib_hard.h"
#include "magn_calib_soft.h"
#include "magn_calib_raw.h"
#include "magn_lpf.h"
#include "magn_param.h"
#include "magn_uncalib.h"

#endif
