/*!******************************************************************************
 * @file    prox_raw_if.h
 * @brief   physical proximity raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup PROX_RAW PROXIMITY RAW
*  @ingroup physicalgroup Physical Sensor
*  @brief proximity raw sensor<BR>
*  @brief -# <B>Contents</B><BR>
*  @brief A sensor for taking out a proximity component.<BR>
*  <BR>
*  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
*  @brief Sensor ID : #SENSOR_ID_PROXIMITY_RAW <BR>
*  Parent Sensor ID : none<BR>
*  <BR>
*  @brief -# <B>Detection Timing</B><BR>
*  @brief Detection Timing : On-detected (This sensor type output sensor
* data when sensor exceed the limit of setting value.)
*/

#ifndef __PROX_RAW_IF_H__
#define __PROX_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup PROX_RAW PROXIMITY RAW
 *  @{
 */

#define PROXIMITY_RAW_ID	SENSOR_ID_PROXIMITY_RAW //!< Distance value from Physical Sensor

/**
 * @struct prox_raw_data_t
 * @brief Output data structure for a proximity raw sensor
 */
typedef struct {
	FLOAT		distance;	//!< proximity [cm]
} prox_raw_data_t;
/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
