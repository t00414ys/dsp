/*!******************************************************************************
 * @file    accl_fall_down_if.h
 * @brief   virtual accel fall down sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_FALL_DOWN_IF_H__
#define __ACCL_FALL_DOWN_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ACCEL_FALL_DOWN ACCEL FALL DOWN SENSOR
 *  @{
 */
#define ACCEL_FALL_DOWN_ID	SENSOR_ID_ACCEL_FALL_DOWN //!< Sensor ID. Accel via FALL DOWN

/**	@struct	accel_fall_down_data_t
 *	@brief	Output data structure for accel fall down sensor
 */
typedef struct {
	int		fall_down_data;	//!< 0 : non fall down, other : fall down
} accel_fall_down_data_t;

/**
 * @name Command List
 *
 */
/**
 * @brief Set falling down sensitivity level
 * @return	0:OK other:NG
 */
#define ACCEL_FALL_DOWN_CMD_SET_SENSITIVITY		0x01
/**
 * @brief Set threshold
 * @return	0:OK other:NG
 */
#define ACCEL_FALL_DOWN_CMD_SET_PARAMETER		0x02
/**
 * @brief Get threshold
 * @return	0:OK other:NG
 */
#define ACCEL_FALL_DOWN_CMD_GET_PARAMETER		0x03
//@}
/** @} */
#endif
