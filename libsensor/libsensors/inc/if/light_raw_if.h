/*!******************************************************************************
 * @file    light_raw_if.h
 * @brief   physical light raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup LIGHT_RAW LIGHT RAW
 *  @ingroup physicalgroup Physical Sensor
 *  @brief light raw sensor
 *  @brief -# <B>Contents</B><BR>
 *  @brief A sensor for taking out an illuminance sensor component.<BR>
 *  <BR>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><BR>
 *  @brief Sensor ID : #SENSOR_ID_LIGHT_RAW <BR>
 *  @brief Parent Sensor ID : none<BR>
 *  <BR>
 *  @brief -# <B>Detection Timing</B><BR>
 *  @brief	Detection Timing : On-detected (This sensor type output sensor
 *			data when sensor exceed the limit of setting value.)
 */

#ifndef __LIGHT_RAW_IF_H__
#define __LIGHT_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup LIGHT_RAW LIGHT RAW
 *  @{
 */

#define LIGHT_RAW_ID	SENSOR_ID_LIGHT_RAW //!< Light value from Physical Sensor

/**
 * @struct light_raw_data_t
 * @brief Output data structure for light raw sensor
 */
typedef struct {
	FLOAT		light;	//!< [lux]
} light_raw_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif

