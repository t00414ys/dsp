/*!******************************************************************************
 * @file    queue_in.h
 * @brief   virtual queue in sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __QUEUE_IN_H__
#define __QUEUE_IN_H__

#include "sensor_if.h"
#include "libsensors_id.h"

/**
 * @brief QUEUE input for debug
 *
 * @param pop data function callback
 *
 * @note id: #SENSOR_ID_DEBUG_QUEUE_IN
 */
sensor_if_t* queue_in_init( int ( *pop )( void ) );

#endif
