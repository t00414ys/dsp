/*!******************************************************************************
 * @file    magn_raw.c
 * @brief   sample program for control magnet raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "sensor_if.h"
#include "magn_driver.h"
#include "if/magn_raw_if.h"
#include "if/magn_calib_raw_if.h"
#include "config.h"

#define DEF_INIT(x) x ## _init

EXTERN_C sensor_if_t* DEF_INIT( magn_raw )( void );

/* magnet phy sensor list */
static	pdriver_if_t	g_devif[] = {
	MAGNEMU_DATA					// Emulation
	HMC5883_DATA					// HMC5883
	AK0991X_DATA					// AK09911/AK09912
	AK8963_DATA						// AK8963
	MMC351X_DATA					// MMC351X
	PM131_DATA						// PM131 (TXC)
	BMM150_DATA						// BMM150
	TBLEND_DATA						// TBLEND
};
static	pdriver_if_t	*g_pDevIF;

typedef struct {
	// ID
	unsigned char		id;
#if defined(USE_MMC351X)
	unsigned char		par_ls[1];
#endif
	// IF
	sensor_if_t			pif;
#if defined(USE_MMC351X)
	sensor_if_get_t		*p_accl;
#endif
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	unsigned int		remain_total;
	// data
	frizz_fp4w			data;
} device_sensor_t;

static device_sensor_t g_device;
unsigned int	g_magn_name = 0;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
#if defined(USE_MMC351X)
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
#else
	return 0;
#endif
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 3;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
#if defined(USE_MMC351X)
	if( gettor->id() == SENSOR_ID_ACCEL_RAW ) {
		g_device.p_accl = gettor;
	}
#endif
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		if( g_pDevIF->ctrl != 0 ) {
			( *g_pDevIF->ctrl )( f_active );
		}
		g_device.f_active = f_active;
		g_device.remain_total = 0;
#if defined(USE_MMC351X)
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_accl->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_accl->id(), g_device.f_active );
#endif
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		if( g_pDevIF->get_ver != 0 ) {
			ret = ( *g_pDevIF->get_ver )();
		}
		break;
	case DEVICE_GET_NAME:
		ret = g_magn_name;
		break;
	case SENSOR_SET_DIRECTION:
		if( g_pDevIF->set_param != 0 ) {
			setting_direction_t param_direction;
			int *p = ( int* )param;
			param_direction.map_x = p[0];
			param_direction.map_y = p[1];
			param_direction.map_z = p[2];
			param_direction.negate_x = p[3];
			param_direction.negate_y = p[4];
			param_direction.negate_z = p[5];
			ret = ( *g_pDevIF->set_param )( &param_direction );
		} else {
			ret = -1;
		}
		break;
	case MAGNET_CALIB_RAW_GET_STATUS:
		if( g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_GET_STATUS] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_GET_STATUS] )( ( magnet_calib_raw_result_t * ) param );
		} else {
			ret = -1;
		}
		break;
	case MAGNET_CALIB_RAW_SET_CALIBDATA:
		if( g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_SET_CALIBDATA] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_SET_CALIBDATA] )( ( magnet_calib_raw_data_t * ) param );
			ret = 0;
		} else {
			ret = -1;
		}
		break;
	case MAGNET_CALIB_RAW_GET_CALIBDATA:
		if( g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_GET_CALIBDATA] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_MAGNET_CALIB_RAW_GET_CALIBDATA] )( ( magnet_calib_raw_data_t * ) param );
			ret = 0;
		} else {
			ret = -1;
		}
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int remain = 0;
	if( g_pDevIF->recv != 0 ) {
		remain = ( *g_pDevIF->recv )( g_device.tick );
	}
	if( remain == 0 ) {
		remain = g_device.tick;
		g_device.ts = ts;
		g_device.f_need = 1;
	} else {
		g_device.remain_total = g_device.remain_total + remain;
		if( g_device.remain_total >= g_device.tick ) {
			g_device.remain_total = 0;
			g_device.ts = ts;
			g_device.f_need = 1;
		}
	}
	return ts + remain;
}

static int calculate( void )
{
	int			result = 0;

	if( g_pDevIF->conv != 0 ) {
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
	}
	g_device.f_need = 0;
	return result;
}

static unsigned int condition( void )
{
	unsigned int	result = 0, res_cond;

	result = get_device_condition( g_device.id );
	if( g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] != 0 ) {
		res_cond = ( *g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] )( 0 );
		if( ( D_RAW_DEVICE_ERR_READ & res_cond ) != 0 ) {
			set_device_condition_phyerr( g_device.id );
		} else {
			reset_device_condition_phyerr( g_device.id );
		}
	}
	return result;
}

sensor_if_t* DEF_INIT( magn_raw )( void )
{
	// ID
	g_device.id = MAGNET_RAW_ID;
	// init hardware
	if( ( g_pDevIF = basedevice_init( &g_devif[0], NELEMENT( g_devif ), g_device.id ) ) == 0 ) {
		return 0;
	}
	if( g_pDevIF->get_name != 0 ) {
		g_magn_name = ( *g_pDevIF->get_name )();
	}

	// init parameter
#if defined(USE_MMC351X)
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;
#endif
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.get.condition = condition;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	// param
	g_device.f_active = 0;
	g_device.tick = 1000;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.remain_total = 0;
	// data
	g_device.data *= as_frizz_fp( 0.0f );

	return &( g_device.pif );
}

