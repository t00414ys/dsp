/*!******************************************************************************
 * @file    base_driver.h
 * @brief   base phy driver prototype
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __BASE_PDRIVER_H__
#define __BASE_PDRIVER_H__

#include "frizz_type.h"


#define	MAX_INDEX_EXTRA			(4)									// device function array max size

// all
#define	INDEX_GET_DEVICE_CONDITION				(0)					// device condition get

// g-sensor
#define	INDEX_ACCL_GET_REAL_RAW_DATA			(1)

// magnet
#define	INDEX_MAGNET_CALIB_RAW_GET_STATUS		(1)
#define	INDEX_MAGNET_CALIB_RAW_GET_CALIBDATA	(2)
#define	INDEX_MAGNET_CALIB_RAW_SET_CALIBDATA	(3)

// ppg-sensor
#define	INDEX_PPG_GET_REAL_RAW_DATA				(1)

// spo2-sensor
#define	INDEX_SPO2_GET_REAL_RAW_DATA				(1)

// Physical device part(device_condition)
#define	D_RAW_DEVICE_ERR_READ	(1  <<  0)							// device is read error


typedef	struct {
	int	( *init )( unsigned int param );					// initial routine
	void	( *ctrl )( int f_ena );								// set active / deactive
	unsigned int	( *recv )( unsigned int tick );						// do_notify
	int	( *conv )( frizz_fp *data );						// calclate
	int	( *set_param )( void *ptr );						// set param
	unsigned int	( *get_ver )( void );								// get version(4byte)
	unsigned int	( *get_name )( void );								// get sensor name(4byte)
	unsigned int	data;											// option data
	int	( *extra_function[MAX_INDEX_EXTRA] )( void *ptr );	// extra function
} pdriver_if_t;


// setting direction
typedef struct {
	unsigned char	map_x;
	unsigned char	map_y;
	unsigned char	map_z;
	char			negate_x;
	char			negate_y;
	char			negate_z;
} setting_direction_t;


#define     DEFAULT_MAP_X     (0)					// 0:array index [0]
#define     DEFAULT_MAP_Y     (1)					// 1:array index [1]
#define     DEFAULT_MAP_Z     (2)					// 2:array index [2]
#define     DEFAULT_MAP_X1     (3)					// 3:array index [3]
#define     DEFAULT_MAP_Y1     (4)					// 4:array index [4]
#define     DEFAULT_MAP_Z1     (5)					// 5:array index [5]

#define     SETTING_DIRECTION_ASSERT	(0)			// 0:set front / 1:set back
#define     SETTING_DIRECTION_NEGATE	(1)			// 0:set front / 1:set back

#define	TBLEND_DATA	{	0,	0,	0,	0, 	0,	0,	0,	0, 	{ 0,	0,	0,	0} 	}


#define		COMPRESS_MAP(map_x,map_y,map_z,negate_x,negate_y,negate_z)										\
					( ((map_x & 0x0F)    << 28) | ((map_y    & 0x0F) << 24) | ((map_z & 0x0F)    << 20) |	\
					  ((negate_x & 0x0F) << 16) | ((negate_y & 0x0F) << 12) | ((negate_z & 0x0F) <<  8)  )

#define		EXPAND_MAP(data,map_x,map_y,map_z,negate_x,negate_y,negate_z)				\
					do {																\
						map_x    = (data >> 28) & 0x0F;									\
						map_y    = (data >> 24) & 0x0F;									\
						map_z    = (data >> 20) & 0x0F;									\
						negate_x = (data >> 16) & 0x0F;									\
						negate_y = (data >> 12) & 0x0F;									\
						negate_z = (data >>  8) & 0x0F;									\
					} while(0);

#ifdef __cplusplus
extern "C" {
#endif

extern unsigned char get_device_condition( int device_id );
extern void set_device_condition_regist( int device_id );
extern void set_device_condition_initerr( int device_id );
extern void set_device_condition_phyerr( int device_id );
extern void reset_device_condition_phyerr( int device_id );
extern unsigned long check_board_clock( void );


extern pdriver_if_t  *basedevice_init( pdriver_if_t *dev, int size, int device_id );


#ifdef __cplusplus
}
#endif


#endif /*__BASE_PDRIVER_H__  */
