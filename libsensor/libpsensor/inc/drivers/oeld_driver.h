/*!******************************************************************************
 * @file    oeld_driver.h
 * @brief   sample program for oled sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __OLED_DRIVER_H__
#define __OLED_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_SSD1306)
#include "ssd1306_api.h"
#define	SSD1306_DATA	{	ssd1306_init,			ssd1306_ctrl,		ssd1306_rcv ,		ssd1306_conv, 	\
							ssd1306_setparam,		ssd1306_get_ver,	ssd1306_get_name ,	0,				\
							{ssd1306_get_condition,	0,					0,					0}				},
#else
#define	SSD1306_DATA
#endif

#if defined(USE_OELD_MOCK)
#include "raw_mock_api.h"
#define	OLEDMOCK_DATA	{	raw_mock_init,			raw_mock_ctrl,		raw_mock_rcv ,		raw_mock_conv, 	\
							raw_mock_setparam,		raw_mock_get_ver,	raw_mock_get_name ,	0,				\
							{raw_mock_get_condition,	0,					0,					0}			},
#else
#define	OLEDMOCK_DATA
#endif

#endif /*  */
