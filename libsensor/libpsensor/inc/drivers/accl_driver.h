/*!******************************************************************************
 * @file    accl_driver.h
 * @brief   sample program for accel sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_DRIVER_H__
#define __ACCL_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_ADS8332)
#include "ads8332_api.h"
#if	!defined(MAP_ADS8332)
#define	MAP_ADS8332		(0)
#endif
#define ADS8332_DATA	{	ads8332_init,			ads8332_ctrl_accl,		ads8332_rcv_accl ,	ads8332_conv_accl, 	\
							ads8332_setparam_accl,	ads8332_get_ver,		ads8332_get_name ,	MAP_ADS8332,			\
						{	ads8332_get_condition,	ads8332_get_raw_data,	0,					0}				},
#else
#define	ADS8332_DATA
#endif

#if defined(USE_ADXL362)
#include "adxl362_api.h"
#if	!defined(MAP_ADXL362)
#define	MAP_ADXL362		(0)
#endif
#define	ADXL362_DATA	{	adxl362_init,				adxl362_ctrl_accl,	adxl362_rcv_accl ,	adxl362_conv_accl, 	\
						adxl362_setparam_accl,		adxl362_get_ver,		adxl362_get_name ,	MAP_ADXL362,			\
						{adxl362_get_condition,		adxl362_get_raw_data,			0,			0}				},
#else
#define	ADXL362_DATA
#endif

#if defined(USE_MC3413)
#include "mc3413_api.h"
#if	!defined(MAP_MC3413)
#define	MAP_MC3413		(0)
#endif
#define	MC3413_DATA	{	mc3413_init,				mc3413_ctrl_accl,	mc3413_rcv_accl ,	mc3413_conv_accl, 	\
						mc3413_setparam_accl,		mc3413_get_ver,		mc3413_get_name ,	MAP_MC3413,			\
						{mc3413_get_condition,		mc3413_get_raw_data,			0,			0}				},
#else
#define	MC3413_DATA
#endif

#if defined(USE_MPUXXXX)
#include "mpuxxxx_api.h"
#if	!defined(MAP_MPUXXXX)
#define	MAP_MPUXXXX		(0)
#endif
#define	MPUXXXX_DATA	{	mpuxxxx_init,					mpuxxxx_ctrl_accl,		mpuxxxx_rcv_accl ,		mpuxxxx_conv_accl, 	\
							mpuxxxx_setparam_accl,			mpuxxxx_get_ver,		mpuxxxx_get_name ,		MAP_MPUXXXX,		\
							{mpuxxxx_accl_get_condition,	mpuxxxx_accl_get_raw_data,			0,			0}					},
#else
#define	MPUXXXX_DATA
#endif

#if defined(USE_BMI160)
#include "bmi160_api.h"
#if	!defined(MAP_BMI160)
#define	MAP_BMI160		(0)
#endif
#define	BMI160_DATA		{	bmi160_init,					bmi160_ctrl_accl,		bmi160_rcv_accl ,		bmi160_conv_accl, 	\
							bmi160_setparam_accl,			bmi160_get_ver,			bmi160_get_name ,		MAP_BMI160,			\
							{bmi160_accl_get_condition,		bmi160_accl_get_raw_data,						0,						0}					},
#else
#define	BMI160_DATA
#endif

#if defined(USE_LSM330)
#include "lsm330_api.h"
#if	!defined(MAP_LSM330)
#define	MAP_LSM330		(0)
#endif
#define	LSM330_DATA		{	lsm330_init,					lsm330_ctrl_accl,		lsm330_rcv_accl ,		lsm330_conv_accl, 	\
							lsm330_setparam_accl,			lsm330_get_ver,			lsm330_get_name ,		MAP_LSM330,			\
							{lsm330_accl_get_condition,		lsm330_accl_get_raw_data,						0,						0}					},
#else
#define	LSM330_DATA
#endif

#if defined(USE_BMA2XX)
#include "bma2xx_api.h"
#if	!defined(MAP_BMA2XX)
#define	MAP_BMA2XX		(0)
#endif
#define	BMA2XX_DATA		{	bma2xx_init,					bma2xx_ctrl_accl,		bma2xx_rcv_accl ,		bma2xx_conv_accl, 	\
							bma2xx_setparam_accl,			bma2xx_get_ver,			bma2xx_get_name ,		MAP_BMA2XX,			\
							{bma2xx_accl_get_condition,		bma2xx_accl_get_raw_data,						0,						0}					},
#else
#define	BMA2XX_DATA
#endif

#if defined(USE_LIS2DH)
#include "lis2dh_api.h"
#if	!defined(MAP_LIS2DH)
#define	MAP_LIS2DH	(0)
#endif
#define	LIS2DH_DATA		{	lis2dh_init,					lis2dh_ctrl_accl,		lis2dh_rcv_accl ,		lis2dh_conv_accl, 	\
							lis2dh_setparam_accl,			lis2dh_get_ver,			lis2dh_get_name ,		MAP_LIS2DH,			\
							{lis2dh_accl_get_condition,		lis2dh_accl_get_raw_data,						0,						0}					},
#else
#define LIS2DH_DATA
#endif

#if defined(USE_LIS2DS12)
#include "lis2ds12_api.h"
#if	!defined(MAP_LIS2DS12)
#define	MAP_LIS2DS12	(0)
#endif
#define	LIS2DS12_DATA	{	lis2ds12_init,					lis2ds12_ctrl_accl,		lis2ds12_rcv_accl ,		lis2ds12_conv_accl, 	\
							lis2ds12_setparam_accl,			lis2ds12_get_ver,			lis2ds12_get_name ,		MAP_LIS2DS12,		\
							{lis2ds12_accl_get_condition,		lis2ds12_accl_get_raw_data,						0,						0}					},
#else
#define LIS2DS12_DATA
#endif

#if defined(USE_LSM6DS3)
#include "lsm6ds3_api.h"
#if	!defined(MAP_LSM6DDS3)
#define	MAP_LSM6DS3	(0)
#endif
#define	LSM6DS3_DATA	{	lsm6ds3_init,					lsm6ds3_ctrl_accl,		lsm6ds3_rcv_accl ,		lsm6ds3_conv_accl, 	\
							lsm6ds3_setparam_accl,			lsm6ds3_get_ver,			lsm6ds3_get_name ,		MAP_LSM6DS3,		\
							{lsm6ds3_accl_get_condition,		lsm6ds3_accl_get_raw_data,						0,						0}					},
#else
#define LSM6DS3_DATA
#endif

#if defined(USE_STK8313)
#include "stk8313_api.h"
#if	!defined(MAP_STK8313)
#define	MAP_STK8313		(0)
#endif
#define	STK8313_DATA	{	stk8313_init,					stk8313_ctrl_accl,		stk8313_rcv_accl ,		stk8313_conv_accl, 	\
							stk8313_setparam_accl,			stk8313_get_ver,			stk8313_get_name ,		MAP_STK8313,			\
							{stk8313_accl_get_condition,		stk8313_accl_get_raw_data,						0,						0}					},
#else
#define STK8313_DATA
#endif

#if defined(USE_ACCL_EMU)
#include "acclemu_api.h"
#if	!defined(MAP_ACCLEMU)
#define	MAP_ACCLEMU		(0)
#endif
#define	ACCLEMU_DATA	{	acclemu_init,					acclemu_ctrl_accl,		acclemu_rcv_accl ,		acclemu_conv_accl, 	\
							acclemu_setparam_accl,			acclemu_get_ver,		acclemu_get_name ,		MAP_ACCLEMU,		\
							{acclemu_get_condition,			acclemu_accl_get_raw_data,						0,						0}					},
#else
#define	ACCLEMU_DATA
#endif

#endif /* __ACCL_DRIVER_H__ */
