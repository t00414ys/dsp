/*!******************************************************************************
 * @file    ppg_driver.h
 * @brief   sample program for ppg sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __PPG_DRIVER_H__
#define __PPG_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_LT1PH03)
#include "lt_1ph03_api.h"
#define	LT1PH03_DATA	{	lt1ph03_init,			lt1ph03_ctrl,			lt1ph03_rcv ,		lt1ph03_conv, 	\
							lt1ph03_setparam,		lt1ph03_get_ver,		lt1ph03_get_name ,	0,				\
							{lt1ph03_get_condition,	0,						0,					0}				},
#else
#define	LT1PH03_DATA
#endif

#if defined(USE_PAH8001)
#include "pah8001_api.h"
#define	PAH8001_DATA	{	pah8001_init,			pah8001_ctrl,			pah8001_rcv ,		pah8001_conv, 	\
							pah8001_setparam,		pah8001_get_ver,		pah8001_get_name ,	0,				\
							{pah8001_get_condition,	pah8001_get_raw_data,	0,					0}				},
#else
#define	PAH8001_DATA
#endif

#if defined(USE_ADPD142)
#include "adpd142_api.h"
#define	ADPD142_DATA	{	adpd142_ppg_init,			adpd142_ppg_ctrl,	adpd142_ppg_rcv ,	adpd142_ppg_conv, 	\
							adpd142_ppg_setparam,		adpd142_get_ver,	adpd142_get_name ,	0,					\
							{adpd142_get_condition,	0,	0,					0}					},
#else
#define	ADPD142_DATA
#endif


#if defined(USE_PPG_EMU)
#include "ppgemu_api.h"
#define	PPGEMU_DATA		{	ppgemu_init,			ppgemu_ctrl,			ppgemu_rcv ,		ppgemu_conv, 		\
							ppgemu_setparam,		ppgemu_get_ver,			ppgemu_get_name ,	0,					\
							{ppgemu_get_condition,	0,						0,					0}					},
#else
#define	PPGEMU_DATA
#endif


#endif /*  */
