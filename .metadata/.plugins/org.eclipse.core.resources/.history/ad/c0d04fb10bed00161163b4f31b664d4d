/*!******************************************************************************
 * @file    orientation.c
 * @brief   virtual orientation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include "frizz_type.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "frizz_fft.h"
#include "quaternion_base.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "orientation.h"
#include "libsensors_id.h"
#include "if/gyro_posture_if.h"
#include "gpio.h"
#include "../../../../libbase/libhub/inc/hub_mgr_if.h"
#include "../../../../libbase/libfrizz_driver/inc/frizz_peri.h"

//#define TESTDATA

#if 0 //�@
defined( TESTDATA )
#	include "gyro_testdata.c"
#endif //�@end

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[1];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_par;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp			data[14];//Host に送るデータ数（4bite）
} device_sensor_t;

static device_sensor_t	g_device;
static int				first = 1;
static frizz_fp				t;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 14; //host に送るデータ数（dataの変数宣言部分と数字を合わせる）
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == g_device.par_ls[0] ) {
		g_device.p_par = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		//hub_mgr_set_sensor_interval( g_device.id, g_device.p_par->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_par->id(), g_device.f_active );
	}
	first = 1;
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void interrupt_event( void *ptr )
{
	// to change high clock mode has already changed in the int-lowpri-dispatcher.S file with an assembler (library libs/libxtos_int1.a)
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) & (~0x0800);	// internal high clock OSC power on
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) | 0x0200;	// to high clock mod

	gpio_set_interrupt( DISABLING_INTERRUPT );
	//set_active( 1 );

	//gpio_set_interrupt( ENABLING_INTERRUPT );

}

/*
 * azimuth: North:0, East:90, South:180, West:270
 * pitch: toward down is positive, toward up is negative, range : -180 <= pitch <= 180
 * roll: toward right down is negative, toward left down is positive, range: -90<= roll <= 90
 */
#define AVERAGENUM 50
#define num 25 //フィルタに使うデータの数(11, 25, 51)
#define nn 24 // num - 1 (10, 24, 50)
#define order 3 //フィルタ次数の決定（2次or3次）
//#define TAP_LOG 1	//N=2^TAP_LOG1 (6より大きい場合現状overflow)
//#define TAP_NUM (1<<TAP_LOG)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if 0 //バターワースフィルタの係数決定で使用
//自作の定数の定義
//sqrt(2)
const float CONST_R2f = 1.414213562373095f;
#define CONST_R2 (*(const frizz_fp*)&CONST_R2f)
//π^2
const float CONST_PIPOW2f = 9.869604401089358f;
#define CONST_PIPOW2 (*(const frizz_fp*)&CONST_PIPOW2f)
//π^3
const float CONST_PIPOW3f = 31.006276680299816f;;
#define CONST_PIPOW3 (*(const frizz_fp*)&CONST_PIPOW3f)
//π^4
const float CONST_PIPOW4f = 97.409091034002420f;
#define CONST_PIPOW4 (*(const frizz_fp*)&CONST_PIPOW4f)
#endif

//姿勢制御の重み付き係数β
const float BETAf =  0.075574973f;
#define beta (*(const frizz_fp*)&BETAf)

//ただの移動平均の係数を定義
//const float COEFFqq_value = frizz_div(FRIZZ_CONST_ONE, num);
const float COEFF_value = 0.0400f;
#define coeffq_value (*(const frizz_fp*)&COEFF_value);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int calculate( void )
{
gpio_set_data( GPIO_NO_3, 1 );
#if 1
	//gpio_set_interrupt( DISABLING_INTERRUPT );

	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_IN_PINT );	// Interrupt Setting
	//gpio_init( interrupt_event, 0 );

	//_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3 );			// enable interrupt level 1
	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );


	//変数の宣言
	frizz_fp4w_t *accl_data;	// 名前が紛らわしいですが、ads8332で取得した加速度とジャイロのデータが入ります。

	//繰り返しを数える
	static int flag = 0;
	//flag_return: 間引きを定義する変数
	static int flag_return = 0;
	//static unsigned char pin = 0;

	// 整数の定義	                                                        //2
	//frizz_fp 	CONST_3  =  frizz_tie_add(FRIZZ_CONST_ONE, FRIZZ_CONST_TWO);	//3
	//frizz_fp	CONST_4  =  frizz_tie_add(FRIZZ_CONST_TWO, FRIZZ_CONST_TWO);	//4
	//frizz_fp	CONST_5  =  frizz_tie_add(FRIZZ_CONST_TWO, CONST_3);            //5
	//frizz_fp	CONST_6  =  frizz_tie_add(CONST_3, CONST_3);                    //6
	//frizz_fp	CONST_7  =  frizz_tie_add(CONST_3, CONST_4);                    //7
	//frizz_fp	CONST_8  =  frizz_tie_mul(FRIZZ_CONST_TWO, CONST_4);            //8
	//frizz_fp	CONST_9  =  frizz_tie_mul(CONST_3, CONST_3);                    //9
	//frizz_fp	CONST_10 =  frizz_tie_mul(FRIZZ_CONST_TWO, CONST_5);            //10
	//frizz_fp	CONST_16 =  frizz_tie_mul(FRIZZ_CONST_TWO, CONST_8);            //16

	//frizz_fp    CONST_PIPI = FRIZZ_MATH_PI * FRIZZ_MATH_PI;                     //π^2
	//frizz_fp	CONST_R2 =  frizz_sqrt(FRIZZ_CONST_TWO);	                    //sqrt(2)

	// 時間を計測する変数
	//unsigned long		cycle = 0;
	//cycle = frizz_time_measure( 0 );
	//unsigned long	    cycle_fft = 0;
	char				time[24];

	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q_raw[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q_fil[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qq[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		acl[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qDot[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		s[4];
	//__attribute__( ( aligned( 16 ) ) ) frizz_fp				cq[4];
	//__attribute__( ( aligned( 16 ) ) ) frizz_fp				eq[4];
	//__attribute__( ( aligned( 16 ) ) ) frizz_fp				aq[4];

	int				    size, i, k, ii, jj;//, iii, kkk;
	static int			movAvgIx = 0;
	static int         j = 0;
	//static int			jjj = 0;
	//static frizz_fp		m[3][3];	//回転行列のための3x3の配列
	//static const int  num = 25;	//フィルタに使うデータの数

	frizz_fp4w			*q4w     = (frizz_fp4w *) &q[0];
	frizz_fp4w			*q_fil4w = (frizz_fp4w *) &q_fil[0];
	frizz_fp4w			*qDot4w  = (frizz_fp4w *) &qDot[0];
	frizz_fp4w			*qq4w    = (frizz_fp4w *) &qq[0];
	frizz_fp4w			*acl4w   = (frizz_fp4w *) &acl[0];
	frizz_fp4w			*s4w     = (frizz_fp4w *) &s[0];
	frizz_fp4w			*q_raw4w = (frizz_fp4w *) &q_raw[0];
	//frizz_fp4w			*cq4w    = (frizz_fp4w *) &cq[0];
	//frizz_fp4w			*eq4w    = (frizz_fp4w *) &eq[0];
	//frizz_fp4w			*aq4w    = (frizz_fp4w *) &aq[0];
	frizz_fp			normA, normS, normQ;

	frizz_fp			gx, gy, gz; //[rad/s]
	frizz_fp			gx_d, gy_d, gz_d; //[deg/s]
	frizz_fp			ax, ay, az;
	frizz_fp			temp;
	frizz_fp			g_raw[3];
	frizz_fp			a_raw[3];
	frizz_fp			m_raw[3];
	frizz_fp			_2q[4];
	frizz_fp			_4q[4];
	frizz_fp			_8q[4];


	frizz_fp			dummy[4];
	frizz_fp4w			*dummy4w = (frizz_fp4w *) &dummy[0];

	frizz_fp			dummyacl[4];
	frizz_fp4w			*dummyacl4w = (frizz_fp4w *) &dummyacl[0];
	frizz_fp			dummys[4];
	frizz_fp4w			*dummys4w = (frizz_fp4w *) &dummys[0];

	//frizz_fp			data[12];
	frizz_fp			testtest[4];

	g_device.p_par->data( ( void** )&accl_data, &g_device.ts );

	// クォータニオンの増加分の初期値を0とする
	qDot[0] = FRIZZ_CONST_ZERO;
	qDot[1] = FRIZZ_CONST_ZERO;
	qDot[2] = FRIZZ_CONST_ZERO;
	qDot[3] = FRIZZ_CONST_ZERO;

	//センサからのローデータを格納
	a_raw[0]  = ((frizz_fp*)accl_data)[0];
	a_raw[1]  = ((frizz_fp*)accl_data)[1];
	a_raw[2]  = ((frizz_fp*)accl_data)[2];
	g_raw[0]  = ((frizz_fp*)accl_data)[4];
	g_raw[1]  = ((frizz_fp*)accl_data)[5];
	g_raw[2]  = ((frizz_fp*)accl_data)[6];
	m_raw[0]  = ((frizz_fp*)accl_data)[8];
	m_raw[1]  = ((frizz_fp*)accl_data)[9];
	m_raw[2]  = ((frizz_fp*)accl_data)[10];

	// 最初の1回だけはクォータニオンに初期値を入れる
	if ( 1 == first ) {
		first = 0;
		q[0] = FRIZZ_CONST_ONE;//as_frizz_fp(1.0f);
		q[1] = FRIZZ_CONST_ZERO;
		q[2] = FRIZZ_CONST_ZERO;
		q[3] = FRIZZ_CONST_ZERO;
		q_raw[0] = FRIZZ_CONST_ONE;//as_frizz_fp(1.0f);
		q_raw[1] = FRIZZ_CONST_ZERO;
		q_raw[2] = FRIZZ_CONST_ZERO;
		q_raw[3] = FRIZZ_CONST_ZERO;
	}
	else{
		//return 0;
	}

	static frizz_fp		movAvg[num][6];	//加速度，角速度データを保存するためのnumx6の配列
	static frizz_fp		buffq[num][4];	//クォータニオンを保存するためのnumx4の配列

	frizz_fp4w			*movAvg4w = (frizz_fp4w *) &movAvg[0][0];
	frizz fp4w          *buffq4w  = (frizz_fp4w *) &buffq[0][4];

	static float    coeff[num];	//角速度，加速度データにかけるフィルタの係数（手前のデータを使用）
	//	static float    coeff_c[num];	//角速度，加速度データにかけるフィルタの係数（過去と未来のデータを使用）
	//static frizz_fp    coeffq[num];	//計算したクォータニオンにかけるフィルタの係数
	//static frizz_fp coeffq_value;
	//coeffq_value = frizz_div(FRIZZ_CONST_ONE, num); //移動平均の係数を計算

	// クォータニオンにかけるただの移動平均の係数
	//for (jj = 0; jj < num; jj++){
	//	coeffq[jj] = coeffq_value;
	//}
	static frizz_fp coeffq;
	coeffq = coeffq_value;

	// Savitzky-Golay Smoothing Filter 係数の設定（Matlab で算出）
	// 3次の近似式
	if (order == 3){
		if (num == 11){
#if 0 //�C
			// 3次11項目（手前のデータを使用）
			coeff[ 0] = -0.0839160839160841;
			coeff[ 1] = 0.0559440559440559;
			coeff[ 2] = 0.0909090909090909;
			coeff[ 3] = 0.055944055944056;
			coeff[ 4] = -0.013986013986014;
			coeff[ 5] = -0.0839160839160839;
			coeff[ 6] = -0.118881118881119;
			coeff[ 7] = -0.083916083916084;
			coeff[ 8] = 0.0559440559440559;
			coeff[ 9] = 0.335664335664336;
			coeff[10] = 0.79020979020979;
#else
			//3次11項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[ 0] = -0.0839160839160840;
			coeff[ 1] = 0.0209790209790209;
			coeff[ 2] = 0.102564102564102;
			coeff[ 3] = 0.160839160839161;
			coeff[ 4] = 0.195804195804196;
			coeff[ 5] = 0.207459207459207;
			coeff[ 6] = 0.195804195804196;
			coeff[ 7] = 0.160839160839161;
			coeff[ 8] = 0.102564102564103;
			coeff[ 9] = 0.0209790209790209;
			coeff[10] = -0.0839160839160840;
#endif //�Cend
		}
		if (num == 25){
#if 0 //�D
			//3次25項目（手前のデータを使用）
			coeff[ 0] = -0.0864957264957265;
			coeff[ 1] = -0.0300854700854701;
			coeff[ 2] = 0.0109401709401709;
			coeff[ 3] = 0.0382905982905983;
			coeff[ 4] = 0.0536752136752137;
			coeff[ 5] = 0.0588034188034188;
			coeff[ 6] = 0.0553846153846154;
			coeff[ 7] = 0.0451282051282051;
			coeff[ 8] = 0.0297435897435898;
			coeff[ 9] = 0.0109401709401710;
			coeff[10] = -0.00957264957264957;
			coeff[11] = -0.0300854700854701;
			coeff[12] = -0.0488888888888889;
			coeff[13] = -0.0642735042735043;
			coeff[14] = -0.0745299145299145;
			coeff[15] = -0.0779487179487180;
			coeff[16] = -0.0728205128205128;
			coeff[17] = -0.0574358974358974;
			coeff[18] = -0.0300854700854701;
			coeff[19] = 0.0109401709401710;
			coeff[20] = 0.0673504273504274;
			coeff[21] = 0.140854700854701;
			coeff[22] = 0.233162393162393;
			coeff[23] = 0.345982905982906;
			coeff[24] = 0.481025641025641;
#else
			//3次25項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[ 0] = -0.048888888888889;
			coeff[ 1] = -0.0266666666666667;
			coeff[ 2] = -0.00637681159420293;
			coeff[ 3] = 0.0119806763285024;
			coeff[ 4] = 0.0284057971014493;
			coeff[ 5] = 0.0428985507246377;
			coeff[ 6] = 0.0554589371980676;
			coeff[ 7] = 0.0660869565217391;
			coeff[ 8] = 0.0747826086956522;
			coeff[ 9] = 0.0815458937198068;
			coeff[10] = 0.0863768115942029;
			coeff[11] = 0.0892753623188406;
			coeff[12] = 0.0902415458937198;
			coeff[13] = 0.0892753623188406;
			coeff[14] = 0.0863768115942029;
			coeff[15] = 0.0815458937198067;
			coeff[16] = 0.0747826086956522;
			coeff[17] = 0.0660869565217391;
			coeff[18] = 0.0554589371980676;
			coeff[19] = 0.0428985507246377;
			coeff[20] = 0.0284057971014493;
			coeff[21] = 0.0119806763285024;
			coeff[22] = -0.00637681159420292;
			coeff[23] = -0.0266666666666667;
			coeff[24] = -0.0488888888888889;
#endif //�Dend
		}
		if (num == 51){
#if 0 //�E
			//3次51項目（手前のデータを使用）
			coeff[ 0] = -0.0582575232963687;
			coeff[ 1] = -0.0404235875933987;
			coeff[ 2] = -0.0248188938532999;
			coeff[ 3] = -0.0113327704892631;
			coeff[ 4] = 0.000145454085520816;
			coeff[ 5] = 0.00972645145786108;
			coeff[ 6] = 0.017520893214567;
			coeff[ 7] = 0.0236394509424476;
			coeff[ 8] = 0.0281927962283123;
			coeff[ 9] = 0.0312916006589702;
			coeff[10] = 0.0330465358212306;
			coeff[11] = 0.0335682733019026;
			coeff[12] = 0.0329674846877954;
			coeff[13] = 0.0313548415657183;
			coeff[14] = 0.0288410155224805;
			coeff[15] = 0.0255366781448912;
			coeff[16] = 0.0215525010197596;
			coeff[17] = 0.0169991557338949;
			coeff[18] = 0.0119873138741063;
			coeff[19] = 0.00662764702720303;
			coeff[20] = 0.00103082677999433;
			coeff[21] = -0.00469247528071061;
			coeff[22] = -0.0104315875681026;
			coeff[23] = -0.0160758384953724;
			coeff[24] = -0.0215145564757108;
			coeff[25] = -0.0266370699223085;
			coeff[26] = -0.0313327072483565;
			coeff[27] = -0.0354907968670455;
			coeff[28] = -0.0390006671915662;
			coeff[29] = -0.0417516466351094;
			coeff[30] = -0.043633063610866;
			coeff[31] = -0.0445342465320267;
			coeff[32] = -0.0443445238117823;
			coeff[33] = -0.0429532238633237;
			coeff[34] = -0.0402496750998415;
			coeff[35] = -0.0361232059345266;
			coeff[36] = -0.0304631447805698;
			coeff[37] = -0.0231588200511618;
			coeff[38] = -0.0140995601594935;
			coeff[39] = -0.00317469351875563;
			coeff[40] = 0.00972645145786103;
			coeff[41] = 0.0247145463571657;
			coeff[42] = 0.0419002627659675;
			coeff[43] = 0.0613942722710758;
			coeff[44] = 0.0833072464592997;
			coeff[45] = 0.107749856917448;
			coeff[46] = 0.134832775232331;
			coeff[47] = 0.164666672990757;
			coeff[48] = 0.197362221779536;
			coeff[49] = 0.233030093185476;
			coeff[50] = 0.271780958795387;
#else
			//3次51項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[0] = -0.0266370699223085;
			coeff[1] = -0.0210876803551609;
			coeff[2] = -0.0157647964846316;
			coeff[3] = -0.0106684183107205;
			coeff[4] = -0.00579854583342772;
			coeff[5] = -0.00115517905275319;
			coeff[6] = 0.00326168203130308;
			coeff[7] = 0.00745203741874107;
			coeff[8] = 0.0114158871095608;
			coeff[9] = 0.0151532311037622;
			coeff[10] = 0.0186640694013454;
			coeff[11] = 0.0219484020023103;
			coeff[12] = 0.025006228906657;
			coeff[13] = 0.0278375501143854;
			coeff[14] = 0.0304423656254955;
			coeff[15] = 0.0328206754399873;
			coeff[16] = 0.0349724795578609;
			coeff[17] = 0.0368977779791162;
			coeff[18] = 0.0385965707037532;
			coeff[19] = 0.0400688577317719;
			coeff[20] = 0.0413146390631724;
			coeff[21] = 0.0423339146979546;
			coeff[22] = 0.0431266846361186;
			coeff[23] = 0.0436929488776643;
			coeff[24] = 0.0440327074225917;
			coeff[25] = 0.0441459602709008;
			coeff[26] = 0.0440327074225917;
			coeff[27] = 0.0436929488776643;
			coeff[28] = 0.0431266846361186;
			coeff[29] = 0.0423339146979546;
			coeff[30] = 0.0413146390631724;
			coeff[31] = 0.0400688577317719;
			coeff[32] = 0.0385965707037532;
			coeff[33] = 0.0368977779791162;
			coeff[34] = 0.0349724795578609;
			coeff[35] = 0.0328206754399873;
			coeff[36] = 0.0304423656254955;
			coeff[37] = 0.0278375501143854;
			coeff[38] = 0.025006228906657;
			coeff[39] = 0.0219484020023104;
			coeff[40] = 0.0186640694013454;
			coeff[41] = 0.0151532311037623;
			coeff[42] = 0.0114158871095608;
			coeff[43] = 0.00745203741874108;
			coeff[44] = 0.00326168203130309;
			coeff[45] = -0.00115517905275318;
			coeff[46] = -0.00579854583342771;
			coeff[47] = -0.0106684183107205;
			coeff[48] = -0.0157647964846316;
			coeff[49] = -0.0210876803551609;
			coeff[50] = -0.0266370699223086;
#endif //�Eend
		}
	}

	// 2次の近似式
	if (order == 2){
		if (num == 11){
#if 0 //�F
			//2次11項目（手前のデータを使用）
			coeff[ 0] = 0.125874125874126;
			coeff[ 1] = 0.0139860139860142;
			coeff[ 2] = -0.0629370629370628;
			coeff[ 3] = -0.104895104895105;
			coeff[ 4] = -0.111888111888112;
			coeff[ 5] = -0.0839160839160839;
			coeff[ 6] = -0.020979020979021;
			coeff[ 7] = 0.0769230769230769;
			coeff[ 8] = 0.20979020979021;
			coeff[ 9] = 0.377622377622378;
			coeff[ 10] = 0.580419580419581;
#else
			//2次11項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[ 0] = -0.0839160839160840;
			coeff[ 1] = 0.0209790209790209;
			coeff[ 2] = 0.102564102564102;
			coeff[ 3] = 0.160839160839161;
			coeff[ 4] = 0.195804195804196;
			coeff[ 5] = 0.207459207459207;
			coeff[ 6] = 0.195804195804196;
			coeff[ 7] = 0.160839160839161;
			coeff[ 8] = 0.102564102564103;
			coeff[ 9] = 0.0209790209790209;
			coeff[10] = -0.0839160839160839;
#endif //�Fend
		}
		if (num == 25){
#if 0 //�G
			//2次25項目（手前のデータを使用）
			coeff[0] = 0.0864957264957267;
			coeff[1] = 0.0564102564102566;
			coeff[2] = 0.0297435897435899;
			coeff[3] = 0.00649572649572662;
			coeff[4] = -0.0133333333333333;
			coeff[5] = -0.0297435897435897;
			coeff[6] = -0.0427350427350427;
			coeff[7] = -0.0523076923076923;
			coeff[8] = -0.0584615384615385;
			coeff[9] = -0.0611965811965812;
			coeff[10] = -0.0605128205128205;
			coeff[11] = -0.0564102564102564;
			coeff[12] = -0.0488888888888889;
			coeff[13] = -0.0379487179487180;
			coeff[14] = -0.0235897435897436;
			coeff[15] = -0.00581196581196583;
			coeff[16] = 0.0153846153846154;
			coeff[17] = 0.0400000000000000;
			coeff[18] = 0.0680341880341881;
			coeff[19] = 0.0994871794871795;
			coeff[20] = 0.134358974358974;
			coeff[21] = 0.172649572649573;
			coeff[22] = 0.214358974358974;
			coeff[23] = 0.259487179487180;
			coeff[24] = 0.308034188034188;
#else
			//2次25項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[ 0] = -0.0488888888888889;
			coeff[ 1] = -0.0266666666666667;
			coeff[ 2] = -0.00637681159420294;
			coeff[ 3] = 0.0119806763285024;
			coeff[ 4] = 0.0284057971014492;
			coeff[ 5] = 0.0428985507246377;
			coeff[ 6] = 0.0554589371980676;
			coeff[ 7] = 0.0660869565217391;
			coeff[ 8] = 0.0747826086956522;
			coeff[ 9] = 0.0815458937198068;
			coeff[10] = 0.0863768115942029;
			coeff[11] = 0.0892753623188406;
			coeff[12] = 0.0902415458937198;
			coeff[13] = 0.0892753623188406;
			coeff[14] = 0.0863768115942029;
			coeff[15] = 0.0815458937198068;
			coeff[16] = 0.0747826086956522;
			coeff[17] = 0.0660869565217391;
			coeff[18] = 0.0554589371980676;
			coeff[19] = 0.0428985507246377;
			coeff[20] = 0.0284057971014493;
			coeff[21] = 0.0119806763285024;
			coeff[22] = -0.00637681159420291;
			coeff[23] = -0.0266666666666667;
			coeff[24] = -0.0488888888888889;
#endif //�Gend
		}
		if (num == 51){
#if 0 //�H
			//2次51項目（手前のデータを使用）
			coeff[0] = 0.0502006317766584;
			coeff[1] = 0.0420046102621019;
			coeff[2] = 0.0342354648680952;
			coeff[3] = 0.0268931955946384;
			coeff[4] = 0.0199778024417314;
			coeff[5] = 0.0134892854093742;
			coeff[6] = 0.00742764449756677;
			coeff[7] = 0.0017928797063092;
			coeff[8] = -0.00341500896439856;
			coeff[9] = -0.0081960215145565;
			coeff[10] = -0.0125501579441646;
			coeff[11] = -0.0164774182532229;
			coeff[12] = -0.0199778024417314;
			coeff[13] = -0.0230513105096901;
			coeff[14] = -0.025697942457099;
			coeff[15] = -0.027917698283958;
			coeff[16] = -0.0297105779902672;
			coeff[17] = -0.0310765815760267;
			coeff[18] = -0.0320157090412362;
			coeff[19] = -0.032527960385896;
			coeff[20] = -0.032613335610006;
			coeff[21] = -0.0322718347135661;
			coeff[22] = -0.0315034576965765;
			coeff[23] = -0.030308204559037;
			coeff[24] = -0.0286860753009477;
			coeff[25] = -0.0266370699223085;
			coeff[26] = -0.0241611884231196;
			coeff[27] = -0.0212584308033809;
			coeff[28] = -0.0179287970630923;
			coeff[29] = -0.0141722872022539;
			coeff[30] = -0.0099889012208657;
			coeff[31] = -0.00537863911892768;
			coeff[32] = -0.000341500896439843;
			coeff[33] = 0.00512251344659781;
			coeff[34] = 0.0110134039101853;
			coeff[35] = 0.0173311704943226;
			coeff[36] = 0.0240758131990097;
			coeff[37] = 0.0312473320242466;
			coeff[38] = 0.0388457269700333;
			coeff[39] = 0.0468709980363699;
			coeff[40] = 0.0553231452232562;
			coeff[41] = 0.0642021685306924;
			coeff[42] = 0.0735080679586784;
			coeff[43] = 0.0832408435072142;
			coeff[44] = 0.0934004951762999;
			coeff[45] = 0.103987022965935;
			coeff[46] = 0.115000426876121;
			coeff[47] = 0.126440706906856;
			coeff[48] = 0.138307863058141;
			coeff[49] = 0.150601895329975;
			coeff[50] = 0.16332280372236;
#else
			//2次51項目（真ん中のデータに対するフィルタ．前後のデータを使用）
			coeff[0] = -0.0266370699223086;
			coeff[1] = -0.0210876803551609;
			coeff[2] = -0.0157647964846316;
			coeff[3] = -0.0106684183107205;
			coeff[4] = -0.00579854583342771;
			coeff[5] = -0.00115517905275318;
			coeff[6] = 0.00326168203130308;
			coeff[7] = 0.00745203741874108;
			coeff[8] = 0.0114158871095608;
			coeff[9] = 0.0151532311037623;
			coeff[10] = 0.0186640694013454;
			coeff[11] = 0.0219484020023104;
			coeff[12] = 0.025006228906657;
			coeff[13] = 0.0278375501143854;
			coeff[14] = 0.0304423656254955;
			coeff[15] = 0.0328206754399873;
			coeff[16] = 0.0349724795578609;
			coeff[17] = 0.0368977779791162;
			coeff[18] = 0.0385965707037532;
			coeff[19] = 0.0400688577317719;
			coeff[20] = 0.0413146390631724;
			coeff[21] = 0.0423339146979546;
			coeff[22] = 0.0431266846361186;
			coeff[23] = 0.0436929488776643;
			coeff[24] = 0.0440327074225917;
			coeff[25] = 0.0441459602709008;
			coeff[26] = 0.0440327074225917;
			coeff[27] = 0.0436929488776643;
			coeff[28] = 0.0431266846361186;
			coeff[29] = 0.0423339146979546;
			coeff[30] = 0.0413146390631724;
			coeff[31] = 0.0400688577317719;
			coeff[32] = 0.0385965707037532;
			coeff[33] = 0.0368977779791162;
			coeff[34] = 0.0349724795578609;
			coeff[35] = 0.0328206754399873;
			coeff[36] = 0.0304423656254955;
			coeff[37] = 0.0278375501143854;
			coeff[38] = 0.025006228906657;
			coeff[39] = 0.0219484020023104;
			coeff[40] = 0.0186640694013454;
			coeff[41] = 0.0151532311037623;
			coeff[42] = 0.0114158871095608;
			coeff[43] = 0.00745203741874108;
			coeff[44] = 0.00326168203130308;
			coeff[45] = -0.00115517905275318;
			coeff[46] = -0.00579854583342771;
			coeff[47] = -0.0106684183107205;
			coeff[48] = -0.0157647964846316;
			coeff[49] = -0.0210876803551609;
			coeff[50] = -0.0266370699223085;
#endif //�Hend
		}
	}

	gx      = g_raw[0];
	gy      = g_raw[1];
	gz      = g_raw[2];
	acl[0]  = a_raw[0];
	acl[1]  = a_raw[1];
	acl[2]  = a_raw[2];

	// 移動平均が計算できるデータ数がそろうまではj(バッファーの長さ > j)を増やす
	if ( num > j ) {
		j++;
	}

	// nn = num-1
	for ( k = 0; k < nn; k++ ) {
		movAvg[k][0] = movAvg[k+1][0];
		movAvg[k][1] = movAvg[k+1][1];
		movAvg[k][2] = movAvg[k+1][2];
		movAvg[k][3] = movAvg[k+1][3];
		movAvg[k][4] = movAvg[k+1][4];
		movAvg[k][5] = movAvg[k+1][5];

	}
	//そのステップで計測した値を格納
	movAvg[nn][0] = g_raw[0];
	movAvg[nn][1] = g_raw[1];
	movAvg[nn][2] = g_raw[2];
	movAvg[nn][3] = a_raw[0];
	movAvg[nn][4] = a_raw[1];
	movAvg[nn][5] = a_raw[2];

	// 移動平均可能なデータ数がそろったステップから移動平均を計算(バッファーサイズ == j)
	if ( num == j ) {
		gx = FRIZZ_CONST_ZERO;
		gy = FRIZZ_CONST_ZERO;
		gz = FRIZZ_CONST_ZERO;
		acl[0] = FRIZZ_CONST_ZERO;
		acl[1] = FRIZZ_CONST_ZERO;
		acl[2] = FRIZZ_CONST_ZERO;
		//移動平均
		for (ii = 0; ii < num; ii++){
			gx     += (as_frizz_fp(coeff[ii]) * movAvg[ii][0]);
			gy     += (as_frizz_fp(coeff[ii]) * movAvg[ii][1]);
			gz     += (as_frizz_fp(coeff[ii]) * movAvg[ii][2]);
			acl[0] += (as_frizz_fp(coeff[ii]) * movAvg[ii][3]);
			acl[1] += (as_frizz_fp(coeff[ii]) * movAvg[ii][4]);
			acl[2] += (as_frizz_fp(coeff[ii]) * movAvg[ii][5]);
		}
	}


#if 0
#if 0 //�K
	gx = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[4];
	gy = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[5];
	gz = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[6];
	acl[0] = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[0];
	acl[1] = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[1];
	acl[2] = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[2];
#else
	gx = ((frizz_fp*)accl_data)[4];
	gy = ((frizz_fp*)accl_data)[5];
	gz = ((frizz_fp*)accl_data)[6];
	acl[0] = ((frizz_fp*)accl_data)[0];
	acl[1] = ((frizz_fp*)accl_data)[1];
	acl[2] = ((frizz_fp*)accl_data)[2];
#endif
#endif


#if 0 //�L
	//*******************//
	// ジャイロのみから姿勢推定
	//*******************//
	// クォータニオンの微分
	qDot[0] = (-q[1] * gx) - (q[2] * gy) - (q[3] * gz);
	qDot[1] = (q[0] * gx) + (q[2] * gz) - (q[3] * gy);
	qDot[2] = (q[0] * gy) - (q[1] * gz) + (q[3] * gx);
	qDot[3] = (q[0] * gz) + (q[1] * gy) - (q[2] * gx);
	*qDot4w = *qDot4w * as_frizz_fp(0.5f); // (1/2)*q

	// 積分
	*q4w = ((*qDot4w) * t) + (*q4w);

	//　スカラーの算出
	*dummy4w = (*q4w) * (*q4w); // 二乗
	norm = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)

	// normalize
	q[0] = frizz_div(q[0], scalar);
	q[1] = frizz_div(q[1], scalar);
	q[2] = frizz_div(q[2], scalar);
	q[3] = frizz_div(q[3], scalar);

#else
	//*********************
	// ジャイロと加速度で姿勢推定
	//*********************

	_2q[0] = FRIZZ_CONST_TWO * q[0];//as_frizz_fp(2.0f) * q[0];
	_2q[1] = FRIZZ_CONST_TWO * q[1];//as_frizz_fp(2.0f) * q[1];
	_2q[2] = FRIZZ_CONST_TWO * q[2];//as_frizz_fp(2.0f) * q[2];
	_2q[3] = FRIZZ_CONST_TWO * q[3];//as_frizz_fp(2.0f) * q[3];
	_4q[0] = FRIZZ_CONST_TWO *_2q[0];//as_frizz_fp(4.0f) * q[0];
	_4q[1] = FRIZZ_CONST_TWO *_2q[1];//as_frizz_fp(4.0f) * q[1];
	_4q[2] = FRIZZ_CONST_TWO *_2q[2];//as_frizz_fp(4.0f) * q[2];
	_4q[3] = FRIZZ_CONST_TWO *_2q[3];//as_frizz_fp(4.0f) * q[3];
	_8q[1] = FRIZZ_CONST_TWO *_4q[1];//as_frizz_fp(8.0f) * q[1];
	_8q[2] = FRIZZ_CONST_TWO *_4q[2];//as_frizz_fp(8.0f) * q[2];
	//(*qq) = *q * *q;
	*qq4w = (*q4w) * (*q4w);

	// 加速度の正規化
	*dummyacl4w = (*acl4w) * (*acl4w); // 二乗
	normA = frizz_sqrt(dummyacl[0] + dummyacl[1] + dummyacl[2]); // 平方根(二乗和)
	if (0 == as_float(normA) ) {
		normA = as_frizz_fp(0.001f);
	}
	//acl[0] = frizz_div( acl[0], normA );
	//acl[1] = frizz_div( acl[1], normA );
	//acl[2] = frizz_div( acl[2], normA );
	//acl[3] = frizz_div( acl[3], normA );
	normA = frizz_div(FRIZZ_CONST_ONE, normA);
	*acl4w = (*acl4w) * normA;


	// 最適化演算
	s[0] = (_4q[0] * qq[2]) + (_2q[2] * acl[0]) + (_4q[0] * qq[1]) - (_2q[1] * acl[1]);
	s[1] = (_4q[1] * qq[3]) - (_2q[3] * acl[0]) + (_4q[1] * qq[0]) - (_2q[0] * acl[1]) - (_4q[1]) + (_8q[1] * qq[1]) + (_8q[1] * qq[2]) + (_4q[1] * acl[2]);
	s[2] = (_4q[2] * qq[0]) + (_2q[0] * acl[0]) + (_4q[2] * qq[3]) - (_2q[3] * acl[1]) - (_4q[2]) + (_8q[2] * qq[1]) + (_8q[2] * qq[2]) + (_4q[2] * acl[2]);
	s[3] = (_4q[3] * qq[1]) - (_2q[1] * acl[0]) + (_4q[3] * qq[2]) - (_2q[2] * acl[1]);

	// 算出された最適値を正規化
	*dummys4w = (*s4w) * (*s4w);
	normS = frizz_sqrt(dummys[0] + dummys[1] + dummys[2] + dummys[3]); // 平方根(二乗和)
	if (0 == as_float(normS) ) {
		normS = as_frizz_fp(0.001f);
	}
	//s[0] = frizz_div( s[0], normS );
	//s[1] = frizz_div( s[1], normS );
	//s[2] = frizz_div( s[2], normS );
	//s[3] = frizz_div( s[3], normS );
	normS = frizz_div(FRIZZ_CONST_ONE, normS);
	*s4w = (*s4w) * normS;

	// クォータニオンの微分
	qDot[0] = - ( q[1] * gx) - (q[2] * gy) - (q[3] * gz);
	qDot[1] =   ( q[0] * gx) + (q[2] * gz) - (q[3] * gy);
	qDot[2] =   ( q[0] * gy) - (q[1] * gz) + (q[3] * gx);
	qDot[3] =   ( q[0] * gz) + (q[1] * gy) - (q[2] * gx);
	//*qDot4w = *qDot4w * as_frizz_fp(0.5f); // (1/2)*q

	//qDot[0] = qDot[0] * FRIZZ_CONST_HALF;
	//qDot[1] = qDot[1] * FRIZZ_CONST_HALF;
	//qDot[2] = qDot[2] * FRIZZ_CONST_HALF;
	//qDot[3] = qDot[3] * FRIZZ_CONST_HALF;
	*qDot4w = (*qDot4w) * FRIZZ_CONST_HALF;

	//*qDot4w -= *s4w;
	qDot[0] -= beta * s[0];
	qDot[1] -= beta * s[1];
	qDot[2] -= beta * s[2];
	qDot[3] -= beta * s[3];



	// クォータニオン領域での積分
	//*q4w = ((*qDot4w) * t) + (*q4w);
	//q[0] = (qDot[0] * t) + q[0];
	//q[1] = (qDot[1] * t) + q[1];
	//q[2] = (qDot[2] * t) + q[2];
	//q[3] = (qDot[3] * t) + q[3];

	*q4w = ((*qDot4w) * t) + (*q4w);


	// クォータニオンの正規化
	*dummy4w = (*q4w) * (*q4w); // 二乗
	normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	if (0 == as_float(normQ) ) {
		normQ = as_frizz_fp(0.001f);
	}
	//q_raw[0] = frizz_div(q[0], normQ);
	//q_raw[1] = frizz_div(q[1], normQ);
	//q_raw[2] = frizz_div(q[2], normQ);
	//q_raw[3] = frizz_div(q[3], normQ);
	normQ = frizz_div(FRIZZ_CONST_ONE, normQ);
	*q_raw4w = (*q_raw4w) * normQ;


	// nn = num-1
	for ( k = 0; k < nn; k++ ) {
		//buffq[k][0] = buffq[k+1][0];
		//buffq[k][1] = buffq[k+1][1];
		//buffq[k][2] = buffq[k+1][2];
		//buffq[k][3] = buffq[k+1][3];
		*buffq4w[k] = *buffq4w[k+1];
	}

	// クォータニオンにフィルタをかける
	//buffq[nn][0] = q_raw[0];
	//buffq[nn][1] = q_raw[1];
	//buffq[nn][2] = q_raw[2];
	//buffq[nn][3] = q_raw[3];
	*buffq4w[k] = *q_raw4w;


#if 1	//�Mfilter あり

	// 移動平均可能なデータ数がそろったステップから移動平均を計算(バッファーサイズ == j)
	if ( num == j ) {
		q_fil[0] = FRIZZ_CONST_ZERO;
		q_fil[1] = FRIZZ_CONST_ZERO;
		q_fil[2] = FRIZZ_CONST_ZERO;
		q_fil[3] = FRIZZ_CONST_ZERO;
		//移動平均
		for (ii = 0; ii < num; ii++){
			//q_fil[0]     += (coeffq[ii] * buffq[ii][0]);
			//q_fil[1]     += (coeffq[ii] * buffq[ii][1]);
			//q_fil[2]     += (coeffq[ii] * buffq[ii][2]);
			//q_fil[3]     += (coeffq[ii] * buffq[ii][3]);
			q_fil[0] += coeffq * buffq[ii][0];
			q_fil[1] += coeffq * buffq[ii][1];
			q_fil[2] += coeffq * buffq[ii][2];
			q_fil[3] += coeffq * buffq[ii][3];
		}
	}

	// クォータニオンの正規化
	*dummy4w = (*q_fil4w) * (*q_fil4w); // 二乗
	normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	if (0 == as_float(normQ) ) {
		normQ = as_frizz_fp(0.001f);
	}
	//q_fil[0] = frizz_div(q_fil[0], normQ);
	//q_fil[1] = frizz_div(q_fil[1], normQ);
	//q_fil[2] = frizz_div(q_fil[2], normQ);
	//q_fil[3] = frizz_div(q_fil[3], normQ);
	normQ = frizz_div(FRIZZ_CONST_ONE, normQ);
	*q_fil4w = (*q_fil4w) * normQ;

	q[0] = q_raw[0];
	q[1] = q_raw[1];
	q[2] = q_raw[2];
	q[3] = q_raw[3];

#else	//フィルタなし
	q[0] = q_raw[0];
	q[1] = q_raw[1];
	q[2] = q_raw[2];
	q[3] = q_raw[3];
#endif //�Mend

	// 検算用
	//*dummy4w = (*q4w) * (*q4w); // 二乗
	//normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)

#endif //�Lend

	//角速度の単位変換[rad/s] -> [deg/s]
#if 0 //フィルタあり
	gx_d = FRIZZ_MATH_RAD2DEG(gx);
	gy_d = FRIZZ_MATH_RAD2DEG(gy);
	gz_d = FRIZZ_MATH_RAD2DEG(gz);
#else	//フィルタなし
	gx_d = g_raw[0]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[4] * FRIZZ_MATH_RAD2DEG;
	gy_d = g_raw[1]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[5] * FRIZZ_MATH_RAD2DEG;
	gz_d = g_raw[2]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[6] * FRIZZ_MATH_RAD2DEG;
#endif

	union{
		frizz_fp frizzfp_;
		unsigned char data_ver[4];
		struct{
			unsigned char ll;
			unsigned char lh;
			unsigned char hl;
			unsigned char hh;
		}s;
	}hoge;

	if (normA > (FRIZZ_CONST_TWO + FRIZZ_CONST_ONE)){
		hoge.s.hh = 0x01;
	}else{
		hoge.s.hh = 0x00;
	};
	hoge.s.lh = 0x02;
	hoge.s.hl = 0x03;
	hoge.s.ll = flag;

#if 1 // motion sensor
	union{
		frizz_fp frizzfp_;
		unsigned char data_ver[4];
		struct{
			unsigned char ll;
			unsigned char lh;
			unsigned char hl;
			unsigned char hh;
		}ss;
	}hogehoge;
	hogehoge.ss.hh = 0x00;
	hogehoge.ss.hl = 0x00;
	hogehoge.ss.lh = 0x00;
	hogehoge.ss.ll = 0x00;

	g_device.data[ 0] = hogehoge.frizzfp_;
	// 出力された値を格納（センサ計測値はローデータ）
	// 角速度（deg/s）（ローデータ）
	static unsigned short aaa = 0;
	hogehoge.ss.hh = 0x55;
	hogehoge.ss.hl = (unsigned char)(aaa>>8);
	hogehoge.ss.lh = (unsigned char)(aaa++>>0);
	hogehoge.ss.ll = 0x55;
	g_device.data[ 1] = hogehoge.frizzfp_;//gx_d;
	g_device.data[ 2] = gy_d;
	g_device.data[ 3] = gz_d;
#else //EMG
	g_device.data[ 1] = ((frizz_fp*)accl_data)[5];
	g_device.data[ 2] = frizz_fabs(((frizz_fp*)accl_data)[5]);
	g_device.data[ 3] = frizz_div(frizz_fabs(((frizz_fp*)accl_data)[5]), FRIZZ_CONST_TWO);
#endif
	// 加速度（ローデータ）
	g_device.data[ 4] = a_raw[0] ;//((frizz_fp*)accl_data)[0];//acl[0];
	g_device.data[ 5] = a_raw[1] ;//((frizz_fp*)accl_data)[1];//acl[1];
	g_device.data[ 6] = a_raw[2] ;//((frizz_fp*)accl_data)[2];//acl[2];
	//g_device.data[ 7] = normA;//norm(accl)
	// 地磁気（ローデータ）
	g_device.data[ 7] = m_raw[0];//((frizz_fp*)accl_data)[8];
	g_device.data[ 8] = m_raw[1];//((frizz_fp*)accl_data)[9];
	g_device.data[ 9] = m_raw[2];//((frizz_fp*)accl_data)[10];

	// クォータニオン（フィルタデータ）
	g_device.data[10] = q_fil[0];
	g_device.data[11] = q_fil[1];
	g_device.data[12] = q_fil[2];
	hogehoge.ss.hh = 0xAA;
	hogehoge.ss.hl = 0xAA;
	hogehoge.ss.lh = 0xAA;
	hogehoge.ss.ll = 0xAA;
	g_device.data[13] = hogehoge.frizzfp_;//q_fil[3];
	//g_device.data[12] = hoge.frizzfp_;//on or off

	g_device.f_need = 0;

	//cycle = frizz_time_measure( cycle );
	//sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );
	//quart_out_raw( "%s", time );

////////////////////////////////////////////////////////////////////////////////////////////////////
#if 0
	typedef union{
		frizz_fp frizzfp_;
		int data_int;
		#if 0
		unsigned char byte[4];
		#else
		union {
			unsigned char u8dat;
			struct   {

				unsigned char axis_size:6;
				unsigned char proc:2;
			}axis_proc;
		} byte[4];
		#endif
	} ENDIAN_BYTE;
	ENDIAN_BYTE dat[5];

				dat[0].byte[3].u8dat = 0xC0;				//SENSOR_ID_GYRO_POSTURE_ESTIMATION
				dat[0].byte[2].u8dat = 0x0D;				//全軸数
				dat[0].byte[1].u8dat = 0x10;				//リアルタイムに送信する総バイト数：16
				dat[0].byte[0].axis_proc.proc = 0x01;		// gx_d: 1 = only memory
				dat[0].byte[0].axis_proc.axis_size = 0x04;
				dat[1].byte[3].axis_proc.proc = 0x01;		// gy_d: 1 = only memory
				dat[1].byte[3].axis_proc.axis_size = 0x04;
				dat[1].byte[2].axis_proc.proc = 0x01;		// gz_d: 1 = only memory
				dat[1].byte[2].axis_proc.axis_size = 0x04;
				dat[1].byte[1].axis_proc.proc = 0x01;		// ax: 1 = only memory
				dat[1].byte[1].axis_proc.axis_size = 0x04;
				dat[1].byte[0].axis_proc.proc = 0x01;		// ay: 1 = only memory
				dat[1].byte[0].axis_proc.axis_size = 0x04;
				dat[2].byte[3].axis_proc.proc = 0x01;		// az: 1 = only memory
				dat[2].byte[3].axis_proc.axis_size = 0x04;
				dat[2].byte[2].axis_proc.proc = 0x01;		// mx: 1 = only memory
				dat[2].byte[2].axis_proc.axis_size = 0x04;
				dat[2].byte[1].axis_proc.proc = 0x01;		// my: 1 = only memory
				dat[2].byte[1].axis_proc.axis_size = 0x04;
				dat[2].byte[0].axis_proc.proc = 0x01;		// mz: 1 = only memory
				dat[2].byte[0].axis_proc.axis_size = 0x04;
				dat[3].byte[3].axis_proc.proc = 0x03;		// q1: 3 = memory & real time
				dat[3].byte[3].axis_proc.axis_size = 0x04;
				dat[3].byte[2].axis_proc.proc = 0x03;		// q2: 3 = memory & real time
				dat[3].byte[2].axis_proc.axis_size = 0x04;
				dat[3].byte[1].axis_proc.proc = 0x03;		// q3: 3 = memory & real time
				dat[3].byte[1].axis_proc.axis_size = 0x04;
				dat[3].byte[0].axis_proc.proc = 0x03;		// q4: 3 = memory & real time
				dat[3].byte[0].axis_proc.axis_size = 0x04;
				dat[4].byte[3].u8dat = 0xFF;				//番兵
				dat[4].byte[2].u8dat = 0xFF;
				dat[4].byte[1].u8dat = 0xFF;
				dat[4].byte[0].u8dat = 0xFF;

				//g_sen_man.cb.res_cmd( sen_id, cmd_code, HUB_MGR_CMD_RET_OK, sizeof( dat ) / sizeof( ENDIAN_BYTE ), dat );
				int bbb, ccc, ddd;
				ddd = sizeof( dat );
				bbb = sizeof( dat )/sizeof( int );
				ccc = sizeof( dat )/sizeof( ENDIAN_BYTE );
				//g_sen_man.cb.res_cmd( sen_id, cmd_code, HUB_MGR_CMD_RET_OK, 5, dat );
#endif
///////////////////////////////////////////////////////////////////////////////////////////////////

	flag_return++;
	if (flag_return > 0){//間引きなし
		flag_return = 0;
		//cycle = frizz_time_measure( cycle );
		//sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );

		gpio_set_data( GPIO_NO_3, 0 );

		return 1; // 1の時結果を送り，0の時結果を送らない
	}
		gpio_set_data( GPIO_NO_3, 0 );

	return 0; // 1の時結果を送り，0の時結果を送らない

#endif
	//gpio_set_data( GPIO_NO_3, 0 );
	//return 1;

}

sensor_if_t* DEF_INIT( gyro_posture )(void) {
	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_IN );

	gpio_init( interrupt_event, 0 );
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// Interrupt Setting
	_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3_EDGE );
	//gpio_set_interrupt( ENABLING_INTERRUPT );

	// ID
	g_device.id = GYRO_POSTURE_ID;
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;	//0x80
	//g_device.par_ls[1] = SENSOR_ID_MAGNET_RAW; //0x81

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	// sampling freq & time interval を設定
	g_device.tick = 1;  //1000 Hz
	t = frizz_div(as_frizz_fp(g_device.tick), as_frizz_fp(1000.0f) );
	g_device.f_need = 0;
	g_device.ts = 0;

	return &(g_device.pif);
}

