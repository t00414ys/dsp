/*!******************************************************************************
 * @file    test_data.c
 * @brief   main routine test data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "hub_mgr.h"
#include "libsensors_id.h"
#include "frizz_util.h"

#if	!defined(TEST_DATA_USED)
#define		TEST_DATA_USED(sen_id,tick)			{ sen_id, tick },	// Use
#endif
#if	!defined(TEST_DATA_UNUSED)
#define		TEST_DATA_UNUSED(sen_id,tick)							// Not Use
#endif

EXTERN_C void test_sensor_active( void );

void test_sensor_active( void )
{
	int		i;
	struct test_input_data_t {
		unsigned char	id;
		int				tick;
	} tbls[] = {
		// Physical
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_RAW, 				 100 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_RAW,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_GYRO_RAW,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_PRESSURE_RAW,			1000 )
		TEST_DATA_UNUSED( SENSOR_ID_LIGHT_RAW,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_PROXIMITY_RAW,			1000 )
		// Application
		// Accelerometer
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_POWER,			1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_LPF,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_HPF,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_STEP_DETECTOR,	1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_PEDOMETER,		1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_LINEAR,			1000 )
		// Magnetometer
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_RAW,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_PARAMETER,		1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_CALIB_RAW,		1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_CALIB_SOFT,		1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_CALIB_HARD,		1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_LPF,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_MAGNET_UNCALIB,			1000 )
		// Gyroscope
		TEST_DATA_UNUSED( SENSOR_ID_GYRO_LPF,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_GYRO_HPF,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_GYRO_UNCALIB,			1000 )
#if 0
		TEST_DATA_UNUSED( SENSOR_ID_GYRO_POSTURE_ESTIMATION,   100 )
#else
		TEST_DATA_USED( SENSOR_ID_GYRO_POSTURE_ESTIMATION,   1 )
#endif
		// Fusion 6D
		TEST_DATA_UNUSED( SENSOR_ID_GRAVITY,				 500 )
		TEST_DATA_UNUSED( SENSOR_ID_DIRECTION,				1000 )
		// Fusion 9D
		TEST_DATA_UNUSED( SENSOR_ID_POSTURE,				1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ORIENTATION,			1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ROTATION_VECTOR,		1000 )
		// Libs
		TEST_DATA_UNUSED( SENSOR_ID_ISP,					1000 )
		// Util
		TEST_DATA_UNUSED( SENSOR_ID_CYCLIC_TIMER,			1000 )
		// Accelerometer
		TEST_DATA_UNUSED( SENSOR_ID_ACCEL_MOVE,				1000 )
		//		// Rotation
		TEST_DATA_UNUSED( SENSOR_ID_ROTATION_GRAVITY_VECTOR, 1000 )
		TEST_DATA_UNUSED( SENSOR_ID_ROTATION_LPF_VECTOR,	1000 )
	};

	for( i = 0; i < NELEMENT(tbls); i++ ) {
		hub_mgr_set_sensor_interval( tbls[i].id, tbls[i].id, tbls[i].tick );
		hub_mgr_set_sensor_active( tbls[i].id, tbls[i].id, 1 );
	}
}
