/*!******************************************************************************
 * @file    accl_raw.c
 * @brief   sample program for control accel raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "sensor_if.h"
#include "accl_driver.h"
#include "if/accl_raw_if.h"
#include "frizz_math.h"
#include "frizz_const.h"

#define DEF_INIT(x) x ## _init

EXTERN_C sensor_if_t* DEF_INIT( accl_raw )( void );

/* accel phy sensor list */
static	pdriver_if_t	g_devif[] = {
		ACCLEMU_DATA					// Emulation
		ADXL362_DATA					// ADXL362
		ADS8332_DATA					// ADS8332
		MC3413_DATA						// MC3413
		BMA2XX_DATA						// BMA2XX
		BMI160_DATA						// BMI160	(It is defined before the MPUXXXX_DATA) [I2C adder is the same ]
		MPUXXXX_DATA					// MPU9255 or MPU6505
		LSM330_DATA						// LSM330
		LSM6DS3_DATA					// LSM6DS3
		LIS2DH_DATA						// LIS2DH
		STK8313_DATA					// STK8313
		LIS2DS12_DATA					// LIS2DS12
		TBLEND_DATA						// TBLEND
};
static	pdriver_if_t	*g_pDevIF;

typedef struct {
	// ID
	unsigned char		id;
	// IF
	sensor_if_t			pif;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	unsigned int		remain_total;
	// data
	frizz_fp			data[12];	//return するデータ数
} device_sensor_t;

static device_sensor_t g_device;
accel_raw_data_t accel_data;
unsigned int	g_accel_name = 0;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	return 0;
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 12;	//data[?]の?にそろえる
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		if( g_pDevIF->ctrl != 0 ) {
			( *g_pDevIF->ctrl )( f_active );
		}
		g_device.f_active = f_active;
		g_device.remain_total = 0;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		if( g_pDevIF->get_ver != 0 ) {
			ret = ( *g_pDevIF->get_ver )();
		}
		break;
	case DEVICE_GET_NAME:
		ret = g_accel_name;
		break;
	case SENSOR_SET_DIRECTION: {
		if( g_pDevIF->set_param != 0 ) {
			setting_direction_t param_direction;
			int *p = ( int* )param;
			param_direction.map_x = p[0];
			param_direction.map_y = p[1];
			param_direction.map_z = p[2];
			param_direction.negate_x = p[3];
			param_direction.negate_y = p[4];
			param_direction.negate_z = p[5];
			ret = ( *g_pDevIF->set_param )( &param_direction );
		} else {
			ret = -1;
		}
		break;
	}
	case SENSOR_ACCL_GET_REAL_RAW_DATA: {
		if( g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] )( ( void* )param );
		}
		ret = 0;
		break;
	}
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int remain = 0;
	if( g_pDevIF->recv != 0 ) {
		remain = ( *g_pDevIF->recv )( g_device.tick );
	}
	if( remain == 0 ) {
		remain = g_device.tick;
		g_device.ts = ts;
		g_device.f_need = 1;
	} else {
		g_device.remain_total = g_device.remain_total + remain;
		if( g_device.remain_total >= g_device.tick ) {
			g_device.remain_total = 0;
			g_device.ts = ts;
			g_device.f_need = 1;
		}
	}
	return ts + remain;
}

struct data {
	frizz_fp   accl[4];
	frizz_fp   gyro[4];
	frizz_fp   magn[4];
} tmp_data;
#define MOVING_AVG_NUM 128

float deb2[3][64];
unsigned char debhead2;
float debsum2[3];
float avg[3];
float dif[3];

static int calculate( void )
{
#if 0
	int		result = 0;
	frizz_fp	*fz = ( frizz_fp* )&g_device.data;

	if( g_pDevIF->conv != 0 ) {
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		accel_data.data[0] =  fz[0];
		accel_data.data[1] =  fz[1];
		accel_data.data[2] =  fz[2];
	}
	g_device.f_need = 0;
	return result;
#else
	int		result = 0;
	//	frizz_fp	*fz = ( frizz_fp* )&g_device.data;
	frizz_fp *fz = (frizz_fp*)&tmp_data;

	static float _buff[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff = (frizz_fp4w*)_buff;
	static float _buff1[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff1 = (frizz_fp4w*)_buff1;
	static float _buff2[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff2 = (frizz_fp4w*)_buff2;
	frizz_fp4w sum4w;
	frizz_fp4w sum4w1;
	frizz_fp4w sum4w2;
	frizz_fp sum;
	int i = 0;
	static unsigned int ix;
	static float avgResult = 0;
	static float avgResult1 = 0;
	static float avgResult2 = 0;
	static unsigned char bTest = 1;

	float debug[9];

	frizz_fp result2;
	frizz_fp result3;
	frizz_fp result4;

	if( g_pDevIF->conv != 0 ) {
		//result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&tmp_data );

		//offset setting
#if 0

#if 0
		// calibration用セッティング
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = (as_frizz_fp(as_float(fz[9])));//frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = (as_frizz_fp(as_float(fz[10])));//frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =(as_frizz_fp(as_float(fz[11])));// frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#else
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.08424268305456370f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.03825108555095290f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.03922758836640120f));

		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (3.13394466362428000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (7.03404075139903000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (-0.17395932311424300f)) * FRIZZ_MATH_DEG2RAD;

		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.62000012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38900005820000f)), as_frizz_fp(1.01139946825424f));
#endif

#elif 0 //SS24-1611-0003 (20170210)
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (1.22531545179032f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (5.33309554774735f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (4.43750906036986f)) * FRIZZ_MATH_DEG2RAD;
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.04053278633080f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.00140672956751f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.30886877017945f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(3.56750023365000f)), as_frizz_fp(1.14839518874945f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.96700012685000f)), as_frizz_fp(1.10273008304299f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.565500035900000f)), as_frizz_fp(0.9691168751479890f));
#elif 0 //SS24-1611-0003 (20170210)
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (3.646784101f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (1.399541797f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (-4.848164622f)) * FRIZZ_MATH_DEG2RAD;
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.034099731f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.036980477f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.674824253f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(3.56750023365000f)), as_frizz_fp(1.14839518874945f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.96700012685000f)), as_frizz_fp(1.10273008304299f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.565500035900000f)), as_frizz_fp(0.9691168751479890f));
#elif 0 //SS24-1611-0001 (20170216)
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (5.1941121912299100f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (3.8776854970074400f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (7.8376828709883400f)) * FRIZZ_MATH_DEG2RAD;
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.0504659163596853f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.0688414907677552f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( -0.0422158715292482f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(3.56750023365000f)), as_frizz_fp(1.14839518874945f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.96700012685000f)), as_frizz_fp(1.10273008304299f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.565500035900000f)), as_frizz_fp(0.9691168751479890f));
#elif 1 //SS24-1611-0004
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.02424268305456370f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.03725108555095290f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.16112758836640120f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (4.81694466362428000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (6.95404075139903000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (7.84595932311424300f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
#elif 0//SS24-1611-0006 (20160225)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.08424268305456370f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.03825108555095290f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.03922758836640120f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (2.53394466362428000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (7.02404075139903000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (-0.31395932311424300f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.66100012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38100005820000f)), as_frizz_fp(1.01139946825424f));
#elif 0	//SS24-1611-0011
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.0724228173981944f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0207034150699966f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0799175098255956f));

		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (2.3741215902827500f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (1.5539288793602100f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (1.9488717916700300f)) * FRIZZ_MATH_DEG2RAD;

		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.62000012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38900005820000f)), as_frizz_fp(1.01139946825424f));
#elif 0//SS24-1611-0014
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.0794958607490962f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.0106921347532499f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.3703348106221890f));

		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (1.462166887f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.805689502f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (1.203465568f)) * FRIZZ_MATH_DEG2RAD;

		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.62000012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38900005820000f)), as_frizz_fp(1.01139946825424f));
#elif 0	//SS24-1611-0012
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.02134021931894550f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( -0.03700822002510920f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.15336832716741500f));

		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.56477622281457300f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (1.99136298094121000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (1.03431981079749000f)) * FRIZZ_MATH_DEG2RAD;

		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.62000012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38900005820000f)), as_frizz_fp(1.01139946825424f));
#else //SS24-1611-0013
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( -0.00304473721040279f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.01698288558946370f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( -0.01470778667278590f));

		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (1.60046381886231000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (2.81345911219096000f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.57363477068442500f)) * FRIZZ_MATH_DEG2RAD;

		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(2.64600014685000f)), as_frizz_fp(1.15685149561952f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-2.62000012400000f)), as_frizz_fp(1.08919942732470f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-1.38900005820000f)), as_frizz_fp(1.01139946825424f));

#endif

#if 0
		deb2[0][debhead2  ] = (as_float(fz[5])*3.14159265358979f)/180.0f;
		deb2[1][debhead2  ] = (as_float(fz[6])*3.14159265358979f)/180.0f;
		deb2[2][debhead2++] = (as_float(fz[7])*3.14159265358979f)/180.0f;
		if ( debhead2 >= 64 ) {
			debhead2 = 0;
		}
		debsum2[0] = 0;
		debsum2[1] = 0;
		debsum2[2] = 0;
		for ( i = 0; i < 64; i++ ) {
			debsum2[0] += deb2[0][i];
			debsum2[1] += deb2[1][i];
			debsum2[2] += deb2[2][i];
		}
		avg[0] = debsum2[0] / 64;
		avg[1] = debsum2[1] / 64;
		avg[2] = debsum2[2] / 64;
#endif

#if 1
		if ( MOVING_AVG_NUM <= ix ) {
			asm( "nop" );
		}

		//　一番古い値を最新の値で上書き
		_buff[ix] = as_float(fz[1]);
		_buff1[ix] = as_float(fz[2]);
		_buff2[ix] = as_float(fz[3]);
		ix = ix + 1;
		if ( MOVING_AVG_NUM <= ix ) {
			ix = 0;
			bTest = 1;
		}

		// １６点分のデータを足す
		sum4w = buff[0];
		sum4w1 = buff1[0];
		sum4w2 = buff2[0];
		//quart_out_raw( "%d[%f %f %f %f]\r\n", 0, as_float(((frizz_fp*)&sum4w)[0]),
		//		as_float(((frizz_fp*)&sum4w)[1]),
		//		as_float(((frizz_fp*)&sum4w)[2]),
		//		as_float(((frizz_fp*)&sum4w)[3]) );
		for ( i = 1; i < (MOVING_AVG_NUM>>2); i++ ) {
			sum4w += buff[i];	// 4w分一度に足される？
			sum4w1 += buff1[i];	// 4w分一度に足される？
			sum4w2 += buff2[i];	// 4w分一度に足される？
			//quart_out_raw( "%d[%f %f %f %f]\r\n", i, as_float(((frizz_fp*)&sum4w)[0]),
			//		as_float(((frizz_fp*)&sum4w)[1]),
			//		as_float(((frizz_fp*)&sum4w)[2]),
			//		as_float(((frizz_fp*)&sum4w)[3]) );
		}
		//quart_out_raw( "sum result = %f\r\n", as_float(frizz_tie_vreduc(sum4w)) );
		result2 = frizz_div( frizz_tie_vreduc(sum4w), as_frizz_fp(128.0f) );
		result3 = frizz_div( frizz_tie_vreduc(sum4w1), as_frizz_fp(128.0f) );
		result4 = frizz_div( frizz_tie_vreduc(sum4w2), as_frizz_fp(128.0f) );
		// 加速度Xとその移動平均の値を表示
		//quart_out_raw( "-- %f %f\r\n", as_float(fz[2]) , as_float(result2) );

		//printf( "-- %f %f\r\n", as_float(fz[1]) , as_float(result2) );
#endif
	}
#if 1
	if ( 1 == bTest ) {
		avgResult = as_float(result2);
		avgResult1 = as_float(result3);
		avgResult2 = as_float(result4);
		bTest = 0;
	}
#endif
	g_device.f_need = 0;
	return result;
#endif
}

static unsigned int condition( void )
{
	unsigned int	result = 0, res_cond;

	result = get_device_condition( g_device.id );
	if( g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] != 0 ) {
		res_cond = ( *g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] )( 0 );
		if( ( D_RAW_DEVICE_ERR_READ & res_cond ) != 0 ) {
			set_device_condition_phyerr( g_device.id );
		} else {
			reset_device_condition_phyerr( g_device.id );
		}
	}
	return result;
}

sensor_if_t* DEF_INIT( accl_raw )( void )
																						{
	// ID
	g_device.id = ACCEL_RAW_ID;

	// init hardware
	if( ( g_pDevIF = basedevice_init( &g_devif[0], NELEMENT( g_devif ), g_device.id ) ) == 0 ) {
		return 0;
	}

	if( g_pDevIF->get_name != 0 ) {
		g_accel_name = ( *g_pDevIF->get_name )();
	}

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.get.condition = condition;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	// param
	g_device.f_active = 0;
	g_device.tick = 1;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.remain_total = 0;
	// data
	//g_device.data = as_frizz_fp( 0.0f );

	return &( g_device.pif );
																						}

