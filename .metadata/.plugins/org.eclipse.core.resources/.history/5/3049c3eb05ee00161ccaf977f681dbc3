/*!******************************************************************************
 * @file    orientation.c
 * @brief   virtual orientation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include "frizz_type.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "frizz_fft.h"
#include "quaternion_base.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "orientation.h"
#include "libsensors_id.h"
#include "if/gyro_posture_if.h"
#include "gpio.h"
#include "matrix.h"
#include "../../../../libbase/libhub/inc/hub_mgr_if.h"
#include "../../../../libbase/libfrizz_driver/inc/frizz_peri.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[1];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_par;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp			data[14];//Host に送るデータ数（4bite）
} device_sensor_t;

static device_sensor_t	g_device;
static int				first = 1;
static frizz_fp			t;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 14; //host に送るデータ数（dataの変数宣言部分と数字を合わせる）
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == g_device.par_ls[0] ) {
		g_device.p_par = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_active( g_device.id, g_device.p_par->id(), g_device.f_active );
	}
	first = 1;
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void interrupt_event( void *ptr )
{
	// to change high clock mode has already changed in the int-lowpri-dispatcher.S file with an assembler (library libs/libxtos_int1.a)
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) & (~0x0800);	// internal high clock OSC power on
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) | 0x0200;	// to high clock mod
	gpio_set_interrupt( DISABLING_INTERRUPT );
	//set_active( 1 );
	//gpio_set_interrupt( ENABLING_INTERRUPT );

}

/*
 * azimuth: North:0, East:90, South:180, West:270
 * pitch: toward down is positive, toward up is negative, range : -180 <= pitch <= 180
 * roll: toward right down is negative, toward left down is positive, range: -90<= roll <= 90
 */
#define num 25 //フィルタに使うデータの数(11, 25, 51)
#define nn 24 // num - 1 (10, 24, 50)

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//姿勢制御の重み付き係数β
const float BETAf =  0.075574973f;
#define beta (*(const frizz_fp*)&BETAf)

//ただの移動平均の係数を定義
const float COEFF_value = 0.0400f;
#define coeffq_value (*(const frizz_fp*)&COEFF_value);

// Savitzky-Golay Smoothing Filter 係数の設定（Matlab で算出）
// 3次の近似式25項目
const float coeff[25] = {
		-0.048888888888889,  -0.0266666666666667,
		-0.00637681159420293, 0.0119806763285024,
		0.0284057971014493,  0.0428985507246377,
		0.0554589371980676,  0.0660869565217391,
		0.0747826086956522,  0.0815458937198068,
		0.0863768115942029,  0.0892753623188406,
		0.0902415458937198,  0.0892753623188406,
		0.0863768115942029,  0.0815458937198067,
		0.0747826086956522,  0.0660869565217391,
		0.0554589371980676,  0.0428985507246377,
		0.0284057971014493,  0.0119806763285024,
		-0.00637681159420292,-0.0266666666666667,
		-0.0488888888888889
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int calculate( void )
{
	gpio_set_data( GPIO_NO_3, 1 );
#if 1

	//変数の宣言
	frizz_fp4w_t *accl_data;	// 名前が紛らわしいですが、ads8332で取得した加速度とジャイロのデータが入ります。

	//繰り返しを数える
	static int flag = 0;
	//flag_return: 間引きを定義する変数
	static int flag_return = 0;

	// 時間を計測する変数
	//unsigned long		cycle = 0;
	//cycle = frizz_time_measure( 0 );
	//unsigned long	    cycle_fft = 0;
	//char				time[24];

	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q_fil[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qq[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		acl[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qDot[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		s[4];

	int				    size, i, k, ii, jj;
	static int         j = 0;

	frizz_fp4w			*q4w     = (frizz_fp4w *) &q[0];
	frizz_fp4w			*q_fil4w = (frizz_fp4w *) &q_fil[0];
	frizz_fp4w			*qDot4w  = (frizz_fp4w *) &qDot[0];
	frizz_fp4w			*qq4w    = (frizz_fp4w *) &qq[0];
	frizz_fp4w			*acl4w   = (frizz_fp4w *) &acl[0];
	frizz_fp4w			*s4w     = (frizz_fp4w *) &s[0];
	frizz_fp			normA, normS, normQ;
	frizz_fp			gx, gy, gz; //[rad/s]
	frizz_fp			gx_d, gy_d, gz_d; //[deg/s]
	frizz_fp			ax, ay, az;
	frizz_fp			g_raw[3];
	frizz_fp			a_raw[3];
	frizz_fp			m_raw[3];
	frizz_fp			_2q[4];
	frizz_fp			_4q[4];
	frizz_fp			_8q[4];

	frizz_fp			dummy[4];
	frizz_fp4w			*dummy4w = (frizz_fp4w *) &dummy[0];
	frizz_fp			dummyacl[4];
	frizz_fp4w			*dummyacl4w = (frizz_fp4w *) &dummyacl[0];
	frizz_fp			dummys[4];
	frizz_fp4w			*dummys4w = (frizz_fp4w *) &dummys[0];

	g_device.p_par->data( ( void** )&accl_data, &g_device.ts );

	// クォータニオンの増加分の初期値を0とする
	//qDot[0] = FRIZZ_CONST_ZERO;
	//qDot[1] = FRIZZ_CONST_ZERO;
	//qDot[2] = FRIZZ_CONST_ZERO;
	//qDot[3] = FRIZZ_CONST_ZERO;
	*qDot4w = FRIZZ_CONST_ZERO;

	//センサからのローデータを格納
	a_raw[0]  = ((frizz_fp*)accl_data)[0];
	a_raw[1]  = ((frizz_fp*)accl_data)[1];
	a_raw[2]  = ((frizz_fp*)accl_data)[2];
	g_raw[0]  = ((frizz_fp*)accl_data)[4];
	g_raw[1]  = ((frizz_fp*)accl_data)[5];
	g_raw[2]  = ((frizz_fp*)accl_data)[6];
	m_raw[0]  = ((frizz_fp*)accl_data)[8];
	m_raw[1]  = ((frizz_fp*)accl_data)[9];
	m_raw[2]  = -((frizz_fp*)accl_data)[10];//地磁気のz軸が左手座標系のため


	static frizz_fp		movAvg[num][6];	//加速度，角速度データを保存するためのnumx6の配列
	static frizz_fp		buffq[num][4];	//クォータニオンを保存するためのnumx4の配列

	frizz_fp4w			*movAvg4w = (frizz_fp4w *) &movAvg[0][0];

	static frizz_fp coeffq;
	coeffq = coeffq_value;

	gx      = g_raw[0];
	gy      = g_raw[1];
	gz      = g_raw[2];
	acl[0]  = a_raw[0];
	acl[1]  = a_raw[1];
	acl[2]  = a_raw[2];

	// 移動平均が計算できるデータ数がそろうまではj(バッファーの長さ > j)を増やす
	if ( num > j ) {
		j++;
	}

	// nn = num-1
	for ( k = 0; k < nn; k++ ) {
		movAvg[k][0] = movAvg[k+1][0];
		movAvg[k][1] = movAvg[k+1][1];
		movAvg[k][2] = movAvg[k+1][2];
		movAvg[k][3] = movAvg[k+1][3];
		movAvg[k][4] = movAvg[k+1][4];
		movAvg[k][5] = movAvg[k+1][5];
	}
	//そのステップで計測した値を格納
	movAvg[nn][0] = g_raw[0];
	movAvg[nn][1] = g_raw[1];
	movAvg[nn][2] = g_raw[2];
	movAvg[nn][3] = a_raw[0];
	movAvg[nn][4] = a_raw[1];
	movAvg[nn][5] = a_raw[2];

	// 移動平均可能なデータ数がそろったステップから移動平均を計算(バッファーサイズ == j)
	if ( num == j ) {
		gx = FRIZZ_CONST_ZERO;
		gy = FRIZZ_CONST_ZERO;
		gz = FRIZZ_CONST_ZERO;
		acl[0] = FRIZZ_CONST_ZERO;
		acl[1] = FRIZZ_CONST_ZERO;
		acl[2] = FRIZZ_CONST_ZERO;
		//移動平均
		for (ii = 0; ii < num; ii++){
			gx     += (as_frizz_fp(coeff[ii]) * movAvg[ii][0]);
			gy     += (as_frizz_fp(coeff[ii]) * movAvg[ii][1]);
			gz     += (as_frizz_fp(coeff[ii]) * movAvg[ii][2]);
			acl[0] += (as_frizz_fp(coeff[ii]) * movAvg[ii][3]);
			acl[1] += (as_frizz_fp(coeff[ii]) * movAvg[ii][4]);
			acl[2] += (as_frizz_fp(coeff[ii]) * movAvg[ii][5]);
		}
	}

	//*********************
	// ジャイロと加速度で姿勢推定
	//*********************
	// 加速度の正規化
	*dummyacl4w = (*acl4w) * (*acl4w); // 二乗
	//normA = frizz_sqrt(dummyacl[0] + dummyacl[1] + dummyacl[2]); // 平方根(二乗和)
	normA = frizz_sqrt(frizz_tie_vreduc(*dummyacl4w));
	if (0 == as_float(normA) ) {
		normA = as_frizz_fp(0.001f);
	}
	//acl[0] = frizz_div( acl[0], normA );
	//acl[1] = frizz_div( acl[1], normA );
	//acl[2] = frizz_div( acl[2], normA );
	//acl[3] = frizz_div( acl[3], normA );
	normA = frizz_div(FRIZZ_CONST_ONE, normA);
	*acl4w = (*acl4w) * normA;

	// 最初の1回だけはクォータニオンに初期値を入れる
	if ( 1 == first ) {
		first = 0;

		frizz_fp roll, pitch, yaw, s_r, c_r, s_p, c_p, s_y, c_y;
		frizz_fp tfm[3][3];
		frizz_fp q_tmp[4];
		frizz_fp4w			*q_tmp4w = (frizz_fp4w *) &q_tmp[0];

		roll = frizz_atan2(acl[1], acl[2]);
		pitch = frizz_atan2(-acl[0], frizz_sqrt(dummyacl[1] + dummyacl[2]));
		s_r = frizz_sin(roll);
		c_r = frizz_cos(roll);
		s_p = frizz_sin(pitch);
		c_p = frizz_cos(pitch);
		yaw = frizz_atan2(s_r*m_raw[2] - c_r*m_raw[1], c_p*m_raw[0] + s_r*s_p*m_raw[1] + s_p*c_r*m_raw[2]);
		s_y = frizz_sin(yaw);
		c_y = frizz_cos(yaw);
		tfm[0][0] = c_p * c_y;
		tfm[0][1] = c_p * s_y;
		tfm[0][2] = -s_p;
		tfm[1][0] = s_r * s_p * c_y - c_r * s_y;
		tfm[1][1] = s_r * s_p * s_y + c_r * c_y;
		tfm[1][2] = s_r * c_p;
		tfm[2][0] = c_r * s_p * c_y + s_r * s_y;
		tfm[2][1] = c_r * s_p * s_y - s_r * c_y;
		tfm[2][2] = c_r * c_p;

		q_tmp[0] = frizz_div( tfm[0][0] + tfm[1][1] + tfm[2][2] + FRIZZ_CONST_ONE, as_frizz_fp(4.0f));
		q_tmp[1] = frizz_div( tfm[0][0] - tfm[1][1] - tfm[2][2] + FRIZZ_CONST_ONE, as_frizz_fp(4.0f));
		q_tmp[2] = frizz_div(-tfm[0][0] + tfm[1][1] - tfm[2][2] + FRIZZ_CONST_ONE, as_frizz_fp(4.0f));
		q_tmp[3] = frizz_div(-tfm[0][0] - tfm[1][1] + tfm[2][2] + FRIZZ_CONST_ONE, as_frizz_fp(4.0f));
		if (q_tmp[0] < FRIZZ_CONST_ZERO){
			q_tmp[0] = FRIZZ_CONST_ZERO;
		}else{
			q_tmp[0] = frizz_sqrt(q_tmp[0]);
		};
		if (q_tmp[1] < FRIZZ_CONST_ZERO){
			q_tmp[1] = FRIZZ_CONST_ZERO;
		}else{
			q_tmp[1] = frizz_sqrt(q_tmp[1]);
		};
		if (q_tmp[2] < FRIZZ_CONST_ZERO){
			q_tmp[2] = FRIZZ_CONST_ZERO;
		}else{
			q_tmp[2] = frizz_sqrt(q_tmp[2]);
		};
		if (q_tmp[3] < FRIZZ_CONST_ZERO){
			q_tmp[3] = FRIZZ_CONST_ZERO;
		}else{
			q_tmp[3] = frizz_sqrt(q_tmp[3]);
		};
		if      (q_tmp[0] >= q_tmp[1] && q_tmp[0] >= q_tmp[2] && q_tmp[0] >= q_tmp[3]){
			q_tmp[1] = frizz_div(tfm[1][2] - tfm[2][1], q_tmp[0]) * as_frizz_fp(0.25f);
			q_tmp[2] = frizz_div(tfm[2][0] - tfm[0][2], q_tmp[0]) * as_frizz_fp(0.25f);
			q_tmp[3] = frizz_div(tfm[0][1] - tfm[1][0], q_tmp[0]) * as_frizz_fp(0.25f);
		}else if (q_tmp[1] >= q_tmp[0] && q_tmp[1] >= q_tmp[2] && q_tmp[1] >= q_tmp[3]){
			q_tmp[0] = frizz_div(tfm[1][2] - tfm[2][1], q_tmp[1]) * as_frizz_fp(0.25f);
			q_tmp[2] = frizz_div(tfm[0][1] - tfm[1][0], q_tmp[1]) * as_frizz_fp(0.25f);
			q_tmp[3] = frizz_div(tfm[0][2] - tfm[2][0], q_tmp[1]) * as_frizz_fp(0.25f);
		}else if (q_tmp[2] >= q_tmp[0] && q_tmp[2] >= q_tmp[1] && q_tmp[2] >= q_tmp[3]){
			q_tmp[0] = frizz_div(tfm[2][0] - tfm[0][2], q_tmp[2]) * as_frizz_fp(0.25f);
			q_tmp[1] = frizz_div(tfm[0][1] - tfm[1][0], q_tmp[2]) * as_frizz_fp(0.25f);
			q_tmp[3] = frizz_div(tfm[2][1] - tfm[1][2], q_tmp[2]) * as_frizz_fp(0.25f);
		}else if (q_tmp[3] >= q_tmp[0] && q_tmp[3] >= q_tmp[1] && q_tmp[3] >= q_tmp[2]){
			q_tmp[0] = frizz_div(tfm[0][1] - tfm[1][0], q_tmp[3]) * as_frizz_fp(0.25f);
			q_tmp[1] = frizz_div(tfm[2][0] - tfm[0][2], q_tmp[3]) * as_frizz_fp(0.25f);
			q_tmp[2] = frizz_div(tfm[2][1] - tfm[1][2], q_tmp[3]) * as_frizz_fp(0.25f);
		}else{
			q_tmp[0] = FRIZZ_CONST_ONE;
			q_tmp[1] = FRIZZ_CONST_ZERO;
			q_tmp[2] = FRIZZ_CONST_ZERO;
			q_tmp[3] = FRIZZ_CONST_ZERO;
		};
		normQ = frizz_tie_vreduc((*q_tmp4w) * (*q_tmp4w));
		normQ = frizz_div(FRIZZ_CONST_ONE, normQ);
		*q4w = (*q_tmp4w) * normA;

		frizz_fp test[3];
		test[0] = roll * FRIZZ_MATH_RAD2DEG;
		test[1] = pitch * FRIZZ_MATH_RAD2DEG;
		test[2] = yaw * FRIZZ_MATH_RAD2DEG;
	}


	_2q[0] = FRIZZ_CONST_TWO * q[0];//as_frizz_fp(2.0f) * q[0];
	_2q[1] = FRIZZ_CONST_TWO * q[1];//as_frizz_fp(2.0f) * q[1];
	_2q[2] = FRIZZ_CONST_TWO * q[2];//as_frizz_fp(2.0f) * q[2];
	_2q[3] = FRIZZ_CONST_TWO * q[3];//as_frizz_fp(2.0f) * q[3];
	_4q[0] = FRIZZ_CONST_TWO *_2q[0];//as_frizz_fp(4.0f) * q[0];
	_4q[1] = FRIZZ_CONST_TWO *_2q[1];//as_frizz_fp(4.0f) * q[1];
	_4q[2] = FRIZZ_CONST_TWO *_2q[2];//as_frizz_fp(4.0f) * q[2];
	_4q[3] = FRIZZ_CONST_TWO *_2q[3];//as_frizz_fp(4.0f) * q[3];
	_8q[1] = FRIZZ_CONST_TWO *_4q[1];//as_frizz_fp(8.0f) * q[1];
	_8q[2] = FRIZZ_CONST_TWO *_4q[2];//as_frizz_fp(8.0f) * q[2];
	//(*qq) = *q * *q;
	*qq4w = (*q4w) * (*q4w);

	// 最適化演算
	s[0] = (_4q[0] * qq[2]) + (_2q[2] * acl[0]) + (_4q[0] * qq[1]) - (_2q[1] * acl[1]);
	s[1] = (_4q[1] * qq[3]) - (_2q[3] * acl[0]) + (_4q[1] * qq[0]) - (_2q[0] * acl[1]) - (_4q[1]) + (_8q[1] * qq[1]) + (_8q[1] * qq[2]) + (_4q[1] * acl[2]);
	s[2] = (_4q[2] * qq[0]) + (_2q[0] * acl[0]) + (_4q[2] * qq[3]) - (_2q[3] * acl[1]) - (_4q[2]) + (_8q[2] * qq[1]) + (_8q[2] * qq[2]) + (_4q[2] * acl[2]);
	s[3] = (_4q[3] * qq[1]) - (_2q[1] * acl[0]) + (_4q[3] * qq[2]) - (_2q[2] * acl[1]);

	// 算出された最適値を正規化
	*dummys4w = (*s4w) * (*s4w);
	//normS = frizz_sqrt(dummys[0] + dummys[1] + dummys[2] + dummys[3]); // 平方根(二乗和)
	normS = frizz_sqrt(frizz_tie_vreduc(*dummys4w));
	if (0 == as_float(normS) ) {
		normS = as_frizz_fp(0.001f);
	}
	//s[0] = frizz_div( s[0], normS );
	//s[1] = frizz_div( s[1], normS );
	//s[2] = frizz_div( s[2], normS );
	//s[3] = frizz_div( s[3], normS );
	normS = frizz_div(FRIZZ_CONST_ONE, normS);
	*s4w = (*s4w) * normS;

	// クォータニオンの微分
	qDot[0] = - ( q[1] * gx) - (q[2] * gy) - (q[3] * gz);
	qDot[1] =   ( q[0] * gx) + (q[2] * gz) - (q[3] * gy);
	qDot[2] =   ( q[0] * gy) - (q[1] * gz) + (q[3] * gx);
	qDot[3] =   ( q[0] * gz) + (q[1] * gy) - (q[2] * gx);
	//*qDot4w = *qDot4w * as_frizz_fp(0.5f); // (1/2)*q

	//qDot[0] = qDot[0] * FRIZZ_CONST_HALF;
	//qDot[1] = qDot[1] * FRIZZ_CONST_HALF;
	//qDot[2] = qDot[2] * FRIZZ_CONST_HALF;
	//qDot[3] = qDot[3] * FRIZZ_CONST_HALF;
	*qDot4w = (*qDot4w) * FRIZZ_CONST_HALF;

	//*qDot4w -= *s4w;
	//qDot[0] -= beta * s[0];
	//qDot[1] -= beta * s[1];
	//qDot[2] -= beta * s[2];
	//qDot[3] -= beta * s[3];
	*qDot4w -= beta * (*s4w);

	// クォータニオン領域での積分
	//*q4w = ((*qDot4w) * t) + (*q4w);
	//q[0] = (qDot[0] * t) + q[0];
	//q[1] = (qDot[1] * t) + q[1];
	//q[2] = (qDot[2] * t) + q[2];
	//q[3] = (qDot[3] * t) + q[3];

	*q4w = ((*qDot4w) * t) + (*q4w);


	// クォータニオンの正規化
	*dummy4w = (*q4w) * (*q4w); // 二乗
	//normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	normQ = frizz_sqrt(frizz_tie_vreduc(*dummy4w));
	if (0 == as_float(normQ) ) {
		normQ = as_frizz_fp(0.001f);
	}
	//q_raw[0] = frizz_div(q[0], normQ);
	//q_raw[1] = frizz_div(q[1], normQ);
	//q_raw[2] = frizz_div(q[2], normQ);
	//q_raw[3] = frizz_div(q[3], normQ);
	normQ = frizz_div(FRIZZ_CONST_ONE, normQ);
	//*q_raw4w = (*q_raw4w) * normQ;
	*q4w = (*q4w) * normQ;

	// nn = num-1
	for ( k = 0; k < nn; k++ ) {
		buffq[k][0] = buffq[k+1][0];
		buffq[k][1] = buffq[k+1][1];
		buffq[k][2] = buffq[k+1][2];
		buffq[k][3] = buffq[k+1][3];
	}
	// クォータニオンにフィルタをかける
	buffq[nn][0] = q[0];
	buffq[nn][1] = q[1];
	buffq[nn][2] = q[2];
	buffq[nn][3] = q[3];



	// 移動平均可能なデータ数がそろったステップから移動平均を計算(バッファーサイズ == j)
	if ( num == j ) {
		q_fil[0] = FRIZZ_CONST_ONE;
		q_fil[1] = FRIZZ_CONST_ZERO;
		q_fil[2] = FRIZZ_CONST_ZERO;
		q_fil[3] = FRIZZ_CONST_ZERO;
		//移動平均
		for (ii = 0; ii < num; ii++){
			//q_fil[0]     += (coeffq[ii] * buffq[ii][0]);
			//q_fil[1]     += (coeffq[ii] * buffq[ii][1]);
			//q_fil[2]     += (coeffq[ii] * buffq[ii][2]);
			//q_fil[3]     += (coeffq[ii] * buffq[ii][3]);
			q_fil[0] += coeffq * buffq[ii][0];
			q_fil[1] += coeffq * buffq[ii][1];
			q_fil[2] += coeffq * buffq[ii][2];
			q_fil[3] += coeffq * buffq[ii][3];
		}
	}else{
		*q_fil4w = *q4w;
	}

	// クォータニオンの正規化
	*dummy4w = (*q_fil4w) * (*q_fil4w); // 二乗
	//normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	normQ = frizz_sqrt(frizz_tie_vreduc(*dummy4w));
	if (0 == as_float(normQ) ) {
		normQ = as_frizz_fp(0.001f);
	}
	//q_fil[0] = frizz_div(q_fil[0], normQ);
	//q_fil[1] = frizz_div(q_fil[1], normQ);
	//q_fil[2] = frizz_div(q_fil[2], normQ);
	//q_fil[3] = frizz_div(q_fil[3], normQ);
	normQ = frizz_div(FRIZZ_CONST_ONE, normQ);
	*q_fil4w = (*q_fil4w) * normQ;

	//角速度の単位変換[rad/s] -> [deg/s]
	gx_d = g_raw[0]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[4] * FRIZZ_MATH_RAD2DEG;
	gy_d = g_raw[1]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[5] * FRIZZ_MATH_RAD2DEG;
	gz_d = g_raw[2]* FRIZZ_MATH_RAD2DEG;//((frizz_fp*)accl_data)[6] * FRIZZ_MATH_RAD2DEG;

	union{
		frizz_fp frizzfp_;
		unsigned char data_ver[4];
		struct{
			unsigned char ll;
			unsigned char lh;
			unsigned char hl;
			unsigned char hh;
		}s;
	}hoge;

	if (normA > (FRIZZ_CONST_TWO + FRIZZ_CONST_ONE)){
		hoge.s.hh = 0x01;
	}else{
		hoge.s.hh = 0x00;
	};
	hoge.s.lh = 0x02;
	hoge.s.hl = 0x03;
	hoge.s.ll = flag;

	union{
		frizz_fp frizzfp_;
		unsigned char data_ver[4];
		struct{
			unsigned char ll;
			unsigned char lh;
			unsigned char hl;
			unsigned char hh;
		}ss;
	}hogehoge;
	hogehoge.ss.hh = 0x00;
	hogehoge.ss.hl = 0x00;
	hogehoge.ss.lh = 0x00;
	hogehoge.ss.ll = 0x00;

	g_device.data[ 0] = hoge.frizzfp_;
	// 出力された値を格納（センサ計測値はローデータ）
	// 角速度（deg/s）（ローデータ）
	static unsigned short aaa = 0;
	hogehoge.ss.hh = 0x55;
	hogehoge.ss.hl = (unsigned char)(aaa>>8);
	hogehoge.ss.lh = (unsigned char)(aaa++>>0);
	hogehoge.ss.ll = 0x55;
	g_device.data[ 1] = gx_d;
	g_device.data[ 2] = gy_d;
	g_device.data[ 3] = gz_d;
	// 加速度（ローデータ）
	g_device.data[ 4] = a_raw[0] ;//((frizz_fp*)accl_data)[0];//acl[0];
	g_device.data[ 5] = a_raw[1] ;//((frizz_fp*)accl_data)[1];//acl[1];
	g_device.data[ 6] = a_raw[2] ;//((frizz_fp*)accl_data)[2];//acl[2];
	// 地磁気（ローデータ）
	g_device.data[ 7] = m_raw[0];//((frizz_fp*)accl_data)[8];
	g_device.data[ 8] = m_raw[1];//((frizz_fp*)accl_data)[9];
	g_device.data[ 9] = m_raw[2];//((frizz_fp*)accl_data)[10];
	// クォータニオン（フィルタデータ）
	g_device.data[10] = q_fil[0];
	g_device.data[11] = q_fil[1];
	g_device.data[12] = q_fil[2];
	g_device.data[13] = q_fil[3];
	hogehoge.ss.hh = 0xAA;
	hogehoge.ss.hl = 0xAA;
	hogehoge.ss.lh = 0xAA;
	hogehoge.ss.ll = 0xAA;
	//g_device.data[13] = hogehoge.frizzfp_;//q_fil[3];

	g_device.f_need = 0;

	//cycle = frizz_time_measure( cycle );
	//sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );
	//quart_out_raw( "%s", time );

	flag_return++;
	if (flag_return > 0){//間引きなし
		flag_return = 0;
		//cycle = frizz_time_measure( cycle );
		//sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );

		gpio_set_data( GPIO_NO_3, 0 );

		return 1; // 1の時結果を送り，0の時結果を送らない
	}
	gpio_set_data( GPIO_NO_3, 0 );

	return 0; // 1の時結果を送り，0の時結果を送らない

#endif
	//gpio_set_data( GPIO_NO_3, 0 );
	//return 1;

}

sensor_if_t* DEF_INIT( gyro_posture )(void) {
	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_IN );

	gpio_init( interrupt_event, 0 );
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// Interrupt Setting
	_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3_EDGE );
	//gpio_set_interrupt( ENABLING_INTERRUPT );

	// ID
	g_device.id = GYRO_POSTURE_ID;
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;	//0x80
	//g_device.par_ls[1] = SENSOR_ID_MAGNET_RAW; //0x81

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	// sampling freq & time interval を設定
	g_device.tick = 1;  //1000 Hz
	t = frizz_div(as_frizz_fp(g_device.tick), as_frizz_fp(1000.0f) );
	g_device.f_need = 0;
	g_device.ts = 0;

	return &(g_device.pif);
}

