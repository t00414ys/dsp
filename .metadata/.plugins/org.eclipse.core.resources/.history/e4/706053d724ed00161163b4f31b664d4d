/*!******************************************************************************
 * @file    hub_main.c
 * @brief   main routine
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "i2c.h"
#include "gpio.h"
#include "timer.h"
#include "frizz_type.h"
#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_mem.h"

#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "hubhal_out.h"
#include "hubhal_out_txt.h"
#include "frizz_debug.h"
#include "quart.h"
#include "spi.h"
#include "timer.h"


EXTERN_C sensor_if_t* ( *init_tbl[] )( void );

//#define	D_DEFAULT_UART_BAUDRATE		(QUART_BAUDRATE_1250000)
#define	D_DEFAULT_UART_BAUDRATE		(QUART_BAUDRATE_115200)


// for SW FIFO
static unsigned int fifo_ram[128 * 1024 / 4] __attribute__( ( section( ".dram1.bss" ) ) );


#if defined (USE_DEBUG_IN)
#include "group_dummy.h"
#define USE_QUART_IN	// input via UART as QueueIF
#endif

// switch for output path
#if defined (USE_QUART_OUT)
#include "hubhal_out_txt.h"
#else
#include "hubhal_out.h"
#endif

// for command input
#include "hubhal_in.h"

// for blocking
#include "hubhal_block.h"

// for internal osc calibration
#include "hubhal_clk_calibrate.h"

// Make Version String
#if !defined(RUN_ON_PC)
#include "frizz_version.h"
static char		*frizz_version = FRIZZ_VERSION;
#endif
//
#define	D_FRIZZ_VER_STRING_MAX_LEN	(64)
static	char	frizz_ver_string[D_FRIZZ_VER_STRING_MAX_LEN];
//
unsigned int	g_ROSC1_FREQ = D_DEFAULT_ROSC1_FREQ;
unsigned int	g_ROSC2_FREQ = D_DEFAULT_ROSC2_FREQ;
EXTERN_C unsigned long check_board_clock( void );

#if	defined(USE_QUART_OUT) || defined(USE_TEST_DATA) || defined(USE_HOST_API_UART_OUT)
EXTERN_C void test_sensor_active( void );
#endif

#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
// >> Here Please put the parameter analysis processing for emulation
EXTERN_C void OptionsAnalysis( int argc,  char * const *argv );
EXTERN_C int hubhal_in_get_file_cmd( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params );
#endif


#if defined(UNUSE_BLOCKING) || defined(UNUSE_BLOCKING_TIMER)
static unsigned int g_ts = 0;

static unsigned int block( unsigned int ts )
{
	g_ts = ts;
	return ts;
}

static unsigned int get_ts( void )
{
	return g_ts;
}

static unsigned int power_mode( void )
{
	return 0;
}
#endif

#if !defined(RUN_ON_PC)
void *data_section_backup_restore( void )
{
	extern void				_data_start, _data_end, _stack_sentry;	// section information
	static volatile int		f_uninit = 1;							// .data section use mark
	volatile frizz_fp4w		*src, *dst;								// 128bit
	unsigned int			*p_data_protect;						// malloc value for .data protect.

	if( f_uninit != 0 ) {											// At the time of the first movement ?
		// backup
		f_uninit = 0;												// .data section not use mark reset !!

		src = ( frizz_fp4w* )&_data_start;							// backup  [.data -> top of the stack]
		dst = ( frizz_fp4w* )&_stack_sentry;
		while( src < ( frizz_fp4w* )&_data_end ) {
			*dst = *src;
			dst++;
			src++;
		}
	} else {
		// restore
		src = ( frizz_fp4w* )&_data_start;							// restore [top of the stack -> .data]
		dst = ( frizz_fp4w* )&_stack_sentry;
		while( src < ( frizz_fp4w* )&_data_end ) {
			*src = *dst;
			dst++;
			src++;
		}
	}
	p_data_protect = ( unsigned int* ) frizz_malloc( &_data_end - &_data_start );				// .data protection of heap area for malloc

	return p_data_protect;
}
#endif

volatile	int		uart_init_flag = 0;



int main( int argc, char * const* argv )
{
	hub_mgr_callback_t	cb;
	void				*p_data_protect;

	int					i;
#if defined(USE_OSC_CALIBRATION) && !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
	unsigned long		ext_clock = 0;
#endif

#if !defined(RUN_ON_PC)
	// backup .data
	p_data_protect = data_section_backup_restore();

	strncpy( &frizz_ver_string[0], frizz_version, sizeof( frizz_ver_string ) );
#else
	p_data_protect = frizz_malloc( sizeof( int ) );

	strncpy( &frizz_ver_string[0], "RUN_ON_PC", sizeof( frizz_ver_string ) );
#endif

#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
	// >> Here Please put the parameter analysis processing for emulation
	OptionsAnalysis( argc, argv );
	// <<
#endif

	memcpy( ( char * ) &frizz_use_mode, ( char * ) &frizz_ini_mode0, sizeof( frizz_ini_mode0 ) );
	g_frizz_version = frizz_use_mode.frizz_version;

	// system dependency
#if	defined(UNUSE_BLOCKING)
	cb.block = block;
	cb.get_ts = get_ts;
	cb.set_power_mode = power_mode;
#elif defined(UNUSE_BLOCKING_TIMER)
	cb.block = block;
	cb.get_ts = get_timer_ts;
	cb.set_power_mode = power_mode;
#else
	cb.block = hubhal_block_exec;
	cb.get_ts = hubhal_block_get_ts;
	cb.set_power_mode = hubhal_set_power_mode;
#endif

#if	defined(USE_QUART_OUT)
	cb.get_cmd = hubhal_in_get_cmd;
	cb.res_cmd = hubhal_out_txt_res_cmd;
	cb.output = hubhal_out_txt_snd_data;
	cb.set_int_no = hubhal_out_txt_set_int_no;

	hubhal_out_txt_init( g_ROSC2_FREQ, D_DEFAULT_UART_BAUDRATE );
	uart_init_flag = 1;
#elif defined(USE_HOST_API_UART_OUT)
	cb.get_cmd = hubhal_in_get_cmd;
	cb.res_cmd = hubhal_out_res_cmd;
	cb.output = hubhal_out_snd_data;
	cb.set_int_no = hubhal_out_init;
	hubhal_out_txt_init( g_ROSC2_FREQ, D_DEFAULT_UART_BAUDRATE );
	uart_init_flag = 1;
#elif	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
	cb.get_cmd = hubhal_in_get_file_cmd;
	cb.res_cmd = hubhal_out_txt_res_cmd;
	cb.output = hubhal_out_txt_snd_data;
	cb.set_int_no = hubhal_out_txt_set_int_no;

	hubhal_out_txt_init( g_ROSC2_FREQ, D_DEFAULT_UART_BAUDRATE );
	uart_init_flag = 1;
#else
	cb.get_cmd = hubhal_in_get_cmd;
	cb.res_cmd = hubhal_out_res_cmd;
	cb.output = hubhal_out_snd_data;
	cb.set_int_no = hubhal_out_init;

	hubhal_out_init( FRIZZ_TO_HOST_GPIO, FRIZZ_TO_HOST_DEFALT_DEVEL );	// GPIO Sig is disabled at initialize

#if defined(USE_QUART_USER_DEBUG)
	quart_init( g_ROSC2_FREQ, D_DEFAULT_UART_BAUDRATE, 1 );
	uart_init_flag = 1;
#endif
#endif
	DEBUG_PRINT( "*** INIT START ****\r\n" );

	// set default frequency
	g_ROSC1_FREQ = D_DEFAULT_ROSC1_FREQ;
	g_ROSC2_FREQ = D_DEFAULT_ROSC2_FREQ;

#if defined(USE_OSC_CALIBRATION) && !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
#define D_PULSE_WIDTH   (10)           // unit ms
	if( frizz_use_mode.clock_source != 0 ) {		// Internal Clock
		st_internal_osc_t	osc;
		int 				ret;

		osc.rosc1 = D_DEFAULT_ROSC1_FREQ;
		osc.rosc2 = D_DEFAULT_ROSC2_FREQ;

		ret = set_calibration_param( D_PULSE_WIDTH );
		if( ret == CALIB_RET_S_SUCCESS ) {
			ret = clk_calibrate( &osc );
			if( ret == CALIB_RET_S_SUCCESS ) {
				g_ROSC1_FREQ = osc.rosc1;
				g_ROSC2_FREQ = osc.rosc2;
			} else {
				DEBUG_PRINT( "errno%d 1:%d 2:%d\r\n", ret, osc.rosc1 , osc.rosc2 );
			}
		}
	}
#endif

#if	!defined(USE_DEBUG_IN)
	// initialize i2c master
	i2c_init( g_ROSC2_FREQ, 400000 ); // -> 400KHz
	spi_init( g_ROSC2_FREQ, 4000000, 0, 0 );	//Dummy for passing Build.
#endif

	// initialize input path
	hubhal_in_init( HUBHAL_IN_FROM_MES );

	// initialize hub manager
	hub_mgr_init( &cb, fifo_ram, sizeof( fifo_ram ) / sizeof( int ) );

	// Registration
	for( i = 0; init_tbl[i] != ( void * ) 0 ; i++ ) {
		sensor_if_t* pRet = init_tbl[i]();
		if( hub_mgr_regist_sensor( pRet ) ) {
#ifdef _DEBUG
			printf( "error regist: tbl no.%d\r\n", i );
#endif
			DEBUG_PRINT( "SENSOR init fail = 0x%x \r\n", i );

		}
	}
#if defined(USE_QUART_IN)
	dummy_init( quart_in_pop );
#endif
	hub_mgr_regist_fix();

	mdelay( 0 );				// because it does not generate the linkerr. .

#if !defined(USE_OSC_CALIBRATION)
	{
#else
	if( frizz_use_mode.clock_source == 0 ) {		// External Clock
#endif

#if defined(USE_OSC_CALIBRATION) && !defined(RUN_ON_PC_EXEC) && !defined(RUN_ON_XPLORER)
		// Check External Clock Setting
		ext_clock = check_board_clock();
		if( ext_clock != 0 ) {
			if( g_ROSC2_FREQ != ext_clock ) {
				g_ROSC2_FREQ = ext_clock;
#if	!defined(USE_DEBUG_IN)
				// initialize i2c master
				i2c_init( g_ROSC2_FREQ, 400000 ); // -> 400KHz
#endif
				if( uart_init_flag != 0 ) {
					quart_init( g_ROSC2_FREQ, D_DEFAULT_UART_BAUDRATE, 1 );
				}
			}
		}
#endif
	}

	// initialize blocking timer
#if	defined(UNUSE_BLOCKING)
	g_ts = 0;
#elif defined(UNUSE_BLOCKING_TIMER)
	timer_init( TIMER_ID_UNUSE_BLOCKING, 0, 0 );
	timer_start( TIMER_ID_UNUSE_BLOCKING, g_ROSC2_FREQ, 1 );
#else

	if( frizz_use_mode.clock_source != 0 ) {		// Internal Clock
		hubhal_block_init( g_ROSC1_FREQ );
	} else {
		hubhal_block_init( g_ROSC2_FREQ );
	}
#endif

	DEBUG_PRINT( "*** MAIN START ****\r\n" );


#if	defined(USE_QUART_OUT) || defined(USE_TEST_DATA) || defined(USE_HOST_API_UART_OUT)
	test_sensor_active();
#endif

	// hub manager main routine
	hub_mgr_run();

	hub_mgr_end();

#if defined(RUN_ON_PC_EXEC)
	printf( " $$$ frizz END $$$\n" );
#elif defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)
	printf( " $$$ USE HEAP SIZE = %ld\n", ( unsigned long ) g_frizz_malloc_max - ( unsigned long ) p_data_protect );
	printf( " $$$ frizz END $$$\n" );
#endif
	frizz_free( p_data_protect );
	return 0;
}
