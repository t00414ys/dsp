/*!******************************************************************************
 * @file    orientation.c
 * @brief   virtual orientation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "quaternion_base.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "orientation.h"
#include "libsensors_id.h"
#include "if/gyro_posture_if.h"

//#define TESTDATA

#if defined( TESTDATA )
#	include "gyro_testdata.c"
#endif

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[1];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_par;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp			data[12];
} device_sensor_t;

static device_sensor_t	g_device;
static int				first = 1;
static frizz_fp				t;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 12;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == g_device.par_ls[0] ) {
		g_device.p_par = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_par->id(), g_device.tick );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_par->id(), g_device.f_active );
		t = frizz_div( as_frizz_fp(g_device.tick), as_frizz_fp(1000.0f) );
	}
	first = 1;
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.f_need = 1;
	return ts + g_device.tick;
}
/*
 * azimuth: North:0, East:90, South:180, West:270
 * pitch: toward down is positive, toward up is negative, range : -180 <= pitch <= 180
 * roll: toward right down is negative, toward left down is positive, range: -90<= roll <= 90
 */
static int calculate( void )
{

	int				size,i;
	frizz_fp4w_t *accl_data;	// 名前が紛らわしいですが、ads8332で取得した加速度とジャイロのデータが入ります。
	//
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		q[4];			// aligne 128bit
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qq[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		acl[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		qDot[4];
	__attribute__( ( aligned( 16 ) ) ) frizz_fp				cq[4];
	__attribute__( ( aligned( 16 ) ) ) frizz_fp				eq[4];
	__attribute__( ( aligned( 16 ) ) ) frizz_fp				aq[4];

	static frizz_fp		movAvg[5][6];
	frizz_fp4w			*movAvg4w = (frizz_fp4w *) &movAvg[0][0];
	static int			movAvgIx = 0;

	frizz_fp4w			*q4w     = (frizz_fp4w *) &q[0];
	frizz_fp4w			*qDot4w  = (frizz_fp4w *) &qDot[0];
	frizz_fp4w			*qq4w    = (frizz_fp4w *) &qq[0];
	frizz_fp4w			*acl4w    = (frizz_fp4w *) &acl[0];
	frizz_fp4w			*cq4w    = (frizz_fp4w *) &cq[0];
	frizz_fp4w			*eq4w    = (frizz_fp4w *) &eq[0];
	frizz_fp4w			*aq4w    = (frizz_fp4w *) &aq[0];
	frizz_fp			normA, normS, normQ;

	frizz_fp			gx, gy, gz;
	frizz_fp			ax, ay, az;
	frizz_fp			_2q[4];
	frizz_fp			_4q[4];
	frizz_fp			_8q[4];
	__attribute__( ( aligned( 16 ) ) ) static frizz_fp		s[4];
	frizz_fp4w			*s4w = (frizz_fp4w *)&s[0];
	frizz_fp			beta = 1;
	frizz_fp			dummy[4];
	frizz_fp4w			*dummy4w = (frizz_fp4w *) &dummy[0];

	frizz_fp			data[12];

	unsigned long		cycle = 0;
	char				time[24];

	cycle = frizz_time_measure( 0 );
	g_device.p_par->data( ( void** )&accl_data, &g_device.ts );

	movAvg[movAvgIx][0] = ((frizz_fp*)accl_data)[4];
	movAvg[movAvgIx][1] = ((frizz_fp*)accl_data)[5];
	movAvg[movAvgIx][2] = ((frizz_fp*)accl_data)[6];
	movAvg[movAvgIx][3] = ((frizz_fp*)accl_data)[0];
	movAvg[movAvgIx][4] = ((frizz_fp*)accl_data)[1];
	movAvg[movAvgIx][5] = ((frizz_fp*)accl_data)[2];
	movAvgIx++;

	if ( 1 == first ) {
		if ( 5 <= movAvgIx ) {
			first = 0;
			movAvgIx = 0;
			// 最初の1回だけはクォータニオンに初期値を入れる
			q[0] = as_frizz_fp(1.0f);
			q[1] = FRIZZ_CONST_ZERO;
			q[2] = FRIZZ_CONST_ZERO;
			q[3] = FRIZZ_CONST_ZERO;
		}
		else {
			return 0;
		}
	}
	else {
		if ( 5 <= movAvgIx ) {
			movAvgIx = 0;
		}
	}


	qDot[0] = FRIZZ_CONST_ZERO;
	qDot[1] = FRIZZ_CONST_ZERO;
	qDot[2] = FRIZZ_CONST_ZERO;
	qDot[3] = FRIZZ_CONST_ZERO;
#if 0
	//**************************//
	// 移動平均
	//**************************//
	// ジャイロデータ
	gx = (as_frizz_fp(-3) * movAvg[0][0]) + (as_frizz_fp(12) * movAvg[1][0]) + (as_frizz_fp(17) * movAvg[2][0]) + (as_frizz_fp(12) * movAvg[3][0]) - (as_frizz_fp(3) * movAvg[4][0]);
	gy = (as_frizz_fp(-3) * movAvg[0][1]) + (as_frizz_fp(12) * movAvg[1][1]) + (as_frizz_fp(17) * movAvg[2][1]) + (as_frizz_fp(12) * movAvg[3][1]) - (as_frizz_fp(3) * movAvg[4][1]);
	gz = (as_frizz_fp(-3) * movAvg[0][2]) + (as_frizz_fp(12) * movAvg[1][2]) + (as_frizz_fp(17) * movAvg[2][2]) + (as_frizz_fp(12) * movAvg[3][2]) - (as_frizz_fp(3) * movAvg[4][2]);
	gx = frizz_div( gx, 35 );
	gy = frizz_div( gy, 35 );
	gz = frizz_div( gz, 35 );

	//　加速度データ
	acl[0] = (as_frizz_fp(-3) * movAvg[0][3]) + (as_frizz_fp(12) * movAvg[1][3]) + (as_frizz_fp(17) * movAvg[2][3]) + (as_frizz_fp(12) * movAvg[3][3]) - (as_frizz_fp(3) * movAvg[4][3]);
	acl[1] = (as_frizz_fp(-3) * movAvg[0][4]) + (as_frizz_fp(12) * movAvg[1][4]) + (as_frizz_fp(17) * movAvg[2][4]) + (as_frizz_fp(12) * movAvg[3][4]) - (as_frizz_fp(3) * movAvg[4][4]);
	acl[2] = (as_frizz_fp(-3) * movAvg[0][5]) + (as_frizz_fp(12) * movAvg[1][5]) + (as_frizz_fp(17) * movAvg[2][5]) + (as_frizz_fp(12) * movAvg[3][5]) - (as_frizz_fp(3) * movAvg[4][5]);
	acl[0] = frizz_div( acl[0], 35 );
	acl[1] = frizz_div( acl[1], 35 );
	acl[2] = frizz_div( acl[2], 35 );

#else
	gx = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[4];
	gy = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[5];
	gz = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[6];

	//　加速度データ
	acl[0] = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[0];
	acl[1] = FRIZZ_CONST_ZERO;//((frizz_fp*)accl_data)[1];
	acl[2] = 0;//((frizz_fp*)accl_data)[2];
#endif

#if 0
	//*******************//
	// ジャイロのみから姿勢推定
	//*******************//
	// クォータニオンの微分
	qDot[0] = (-q[1] * gx) - (q[2] * gy) - (q[3] * gz);
	qDot[1] = (q[0] * gx) + (q[2] * gz) - (q[3] * gy);
	qDot[2] = (q[0] * gy) - (q[1] * gz) + (q[3] * gx);
	qDot[3] = (q[0] * gz) + (q[1] * gy) - (q[2] * gx);
	*qDot4w = *qDot4w * as_frizz_fp(0.5f); // (1/2)*q

	// 積分
	*q4w = ((*qDot4w) * t) + (*q4w);

	//　スカラーの算出
	*dummy4w = (*q4w) * (*q4w); // 二乗
	norm = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)

	// normalize
	q[0] = frizz_div(q[0], scalar);
	q[1] = frizz_div(q[1], scalar);
	q[2] = frizz_div(q[2], scalar);
	q[3] = frizz_div(q[3], scalar);

#else
	//*********************
	// ジャイロと加速度で姿勢推定
	//*********************

	_2q[0] = as_frizz_fp(2.0f) * q[0];
	_2q[1] = as_frizz_fp(2.0f) * q[1];
	_2q[2] = as_frizz_fp(2.0f) * q[2];
	_2q[3] = as_frizz_fp(2.0f) * q[3];
	_4q[0] = as_frizz_fp(4.0f) * q[0];
	_4q[1] = as_frizz_fp(4.0f) * q[1];
	_4q[2] = as_frizz_fp(4.0f) * q[2];
	_8q[1] = as_frizz_fp(8.0f) * q[1];
	_8q[2] = as_frizz_fp(8.0f) * q[2];
	//(*qq) = *q * *q;
	qq[0] = q[0] * q[0];
	qq[1] = q[1] * q[1];
	qq[2] = q[2] * q[2];
	qq[3] = q[3] * q[3];


	// 加速度の正規化
	*dummy4w = (*acl4w) * (*acl4w); // 二乗
	normA = frizz_sqrt(dummy[0] + dummy[1] + dummy[2]); // 平方根(二乗和)
	if (0 == as_float(normA) ) {
		normA = as_frizz_fp(0.001f);
	}
	acl[0] = frizz_div( acl[0], normA );
	acl[1] = frizz_div( acl[1], normA );
	acl[2] = frizz_div( acl[2], normA );
	acl[3] = frizz_div( acl[3], normA );

	// 最適化演算
	s[0] = (_4q[0] * qq[2]) + (_2q[2] * acl[0]) + (_4q[0]*qq[1]) - (_2q[1] * acl[1]);
	s[1] = (_4q[1] * qq[3]) - (_2q[3] + acl[1]) + (as_frizz_fp(4.0f) * qq[0] * q[1]) - (_2q[0] * acl[1]) - (_4q[1]) + (_8q[1] * qq[1]) + (_8q[1] * qq[2]) + (_4q[1]+acl[2]);
	s[2] = (as_frizz_fp(4.0f) * qq[0] * q[2]) + (_2q[0] + acl[0]) + (_4q[2] * qq[3]) - (_2q[3] * acl[1]) - (_4q[2]) + (_8q[2] * qq[1]) + (_8q[2] * qq[2]) + (_4q[2]+acl[2]);
	s[3] = (as_frizz_fp(4.0f) * qq[1] * q[3]) - (_2q[1] * acl[0]) + (as_frizz_fp(4.0f) * qq[2] * q[3]) - (_2q[2]*acl[1]);

	// 算出された最適値を正規化
	*dummy4w = (*s4w) + (*s4w);
	normS = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	if (0 == as_float(normS) ) {
		normS = as_frizz_fp(0.001f);
	}

	s[0] = frizz_div( s[0], normS );
	s[1] = frizz_div( s[1], normS );
	s[2] = frizz_div( s[2], normS );
	s[3] = frizz_div( s[3], normS );

	// クォータニオンの微分
	qDot[0] = (-q[1] * gx) - (q[2] * gy) - (q[3] * gz);
	qDot[1] = ( q[0] * gx) + (q[2] * gz) - (q[3] * gy);
	qDot[2] = ( q[0] * gy) - (q[1] * gz) + (q[3] * gx);
	qDot[3] = ( q[0] * gz) + (q[1] * gy) - (q[2] * gx);
	//*qDot4w = *qDot4w * as_frizz_fp(0.5f); // (1/2)*q
	qDot[0] = qDot[0] * as_frizz_fp(0.5f);
	qDot[1] = qDot[1] * as_frizz_fp(0.5f);
	qDot[2] = qDot[2] * as_frizz_fp(0.5f);
	qDot[3] = qDot[3] * as_frizz_fp(0.5f);

	//*qDot4w -= *s4w;
	qDot[0] -= s[0];
	qDot[1] -= s[1];
	qDot[2] -= s[2];
	qDot[3] -= s[3];


	// クォータニオン領域での積分
	//*q4w = ((*qDot4w) * t) + (*q4w);
	qDot[0] = s[0];
	qDot[1] = s[1];
	qDot[2] = s[2];
	qDot[3] = s[3];


	q[0] = (qDot[0] * t) + q[0];
	q[1] = (qDot[1] * t) + q[1];
	q[2] = (qDot[2] * t) + q[2];
	q[3] = (qDot[3] * t) + q[3];

	// クォータニオンの正規化
	*dummy4w = (*q4w) * (*q4w); // 二乗
	normQ = frizz_sqrt(dummy[0] + dummy[1] + dummy[2] + dummy[3]); // 平方根(二乗和)
	if (0 == as_float(normQ) ) {
		normQ = as_frizz_fp(0.001f);
	}
	q[0] = frizz_div(q[0], normQ);
	q[1] = frizz_div(q[1], normQ);
	q[2] = frizz_div(q[2], normQ);
	q[3] = frizz_div(q[3], normQ);

	// 共役クォータニオンを計算するための変数準備
	cq[0] = q[0];
	cq[1] = as_frizz_fp(-1.0f) * q[1];
	cq[2] = as_frizz_fp(-1.0f) * q[2];
	cq[3] = as_frizz_fp(-1.0f) * q[3];

	// クォータニオン同時の掛け算。共役クォータニオン
	eq[0] = as_frizz_fp(1.0f);
	eq[1] = as_frizz_fp(0.0f);
	eq[2] = as_frizz_fp(0.0f);
	eq[3] = as_frizz_fp(0.0f);
	aq[0] = (cq[0] * eq[0]) - (cq[1] * eq[1]) - (cq[2] * eq[2]) - (cq[3] * eq[3]);
	aq[1] = (cq[0] * eq[1]) + (cq[1] * eq[0]) + (cq[2] * eq[3]) - (cq[3] * eq[2]);
	aq[2] = (cq[0] * eq[2]) - (cq[1] * eq[3]) + (cq[2] * eq[0]) + (cq[3] * eq[1]);
	aq[3] = (cq[0] * eq[3]) + (cq[1] * eq[2]) - (cq[2] * eq[1]) + (cq[3] * eq[0]);

	// クォータニオンから回転行列を算出


#endif
	// 出力された値を格納
	g_device.data[0] = gx;
	g_device.data[1] = gy;
	g_device.data[2] = gz;
	g_device.data[3] = acl[0];
	g_device.data[4] = acl[1];
	g_device.data[5] = acl[2];
	g_device.data[6] = 0;
	g_device.data[7] = q[0];
	g_device.data[8] = q[1];
	g_device.data[9] = q[2];
	g_device.data[10] = q[3];
	g_device.data[11] = 0;

	g_device.f_need = 0;

	cycle = frizz_time_measure( cycle );
	sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );
	return 1;
}

sensor_if_t* DEF_INIT( gyro_posture )(void) {
	// ID
	g_device.id = GYRO_POSTURE_ID;
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	g_device.tick = 10;
	g_device.f_need = 0;
	g_device.ts = 0;

	return &(g_device.pif);
}

