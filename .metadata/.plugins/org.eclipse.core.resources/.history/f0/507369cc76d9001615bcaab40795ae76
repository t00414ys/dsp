/*!******************************************************************************
 * @file    accl_raw.c
 * @brief   sample program for control accel raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "sensor_if.h"
#include "accl_driver.h"
#include "if/accl_raw_if.h"

#define DEF_INIT(x) x ## _init

EXTERN_C sensor_if_t* DEF_INIT( accl_raw )( void );

/* accel phy sensor list */
static	pdriver_if_t	g_devif[] = {
	ACCLEMU_DATA					// Emulation
	ADXL362_DATA					// ADXL362
	ADS8332_DATA					// ADS8332
	MC3413_DATA						// MC3413
	BMA2XX_DATA						// BMA2XX
	BMI160_DATA						// BMI160	(It is defined before the MPUXXXX_DATA) [I2C adder is the same ]
	MPUXXXX_DATA					// MPU9255 or MPU6505
	LSM330_DATA						// LSM330
	LSM6DS3_DATA					// LSM6DS3
	LIS2DH_DATA						// LIS2DH
	STK8313_DATA					// STK8313
	LIS2DS12_DATA					// LIS2DS12
	TBLEND_DATA						// TBLEND
};
static	pdriver_if_t	*g_pDevIF;

typedef struct {
	// ID
	unsigned char		id;
	// IF
	sensor_if_t			pif;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	unsigned int		remain_total;
	// data
	frizz_fp			data[8];
} device_sensor_t;

static device_sensor_t g_device;
accel_raw_data_t accel_data;
unsigned int	g_accel_name = 0;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	return 0;
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 8;
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		if( g_pDevIF->ctrl != 0 ) {
			( *g_pDevIF->ctrl )( f_active );
		}
		g_device.f_active = f_active;
		g_device.remain_total = 0;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		if( g_pDevIF->get_ver != 0 ) {
			ret = ( *g_pDevIF->get_ver )();
		}
		break;
	case DEVICE_GET_NAME:
		ret = g_accel_name;
		break;
	case SENSOR_SET_DIRECTION: {
			if( g_pDevIF->set_param != 0 ) {
				setting_direction_t param_direction;
				int *p = ( int* )param;
				param_direction.map_x = p[0];
				param_direction.map_y = p[1];
				param_direction.map_z = p[2];
				param_direction.negate_x = p[3];
				param_direction.negate_y = p[4];
				param_direction.negate_z = p[5];
				ret = ( *g_pDevIF->set_param )( &param_direction );
			} else {
				ret = -1;
			}
			break;
		}
	case SENSOR_ACCL_GET_REAL_RAW_DATA: {
			if( g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] != 0 ) {
				( *g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] )( ( void* )param );
			}
			ret = 0;
			break;
		}
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int remain = 0;
	if( g_pDevIF->recv != 0 ) {
		remain = ( *g_pDevIF->recv )( g_device.tick );
	}
	if( remain == 0 ) {
		remain = g_device.tick;
		g_device.ts = ts;
		g_device.f_need = 1;
	} else {
		g_device.remain_total = g_device.remain_total + remain;
		if( g_device.remain_total >= g_device.tick ) {
			g_device.remain_total = 0;
			g_device.ts = ts;
			g_device.f_need = 1;
		}
	}
	return ts + remain;
}

struct data {
	frizz_fp   accl[4];
	frizz_fp   gyro[4];
} tmp_data;
#define MOVING_AVG_NUM 128

float deb2[3][64];
unsigned char debhead2;
float debsum2[3];
float avg[3];
float dif[3];

static int calculate( void )
{
#if 0
	int		result = 0;
	frizz_fp	*fz = ( frizz_fp* )&g_device.data;

	if( g_pDevIF->conv != 0 ) {
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		accel_data.data[0] =  fz[0];
		accel_data.data[1] =  fz[1];
		accel_data.data[2] =  fz[2];
	}
	g_device.f_need = 0;
	return result;
#else
	int		result = 0;
//	frizz_fp	*fz = ( frizz_fp* )&g_device.data;
	frizz_fp *fz = (frizz_fp*)&tmp_data;

	static float _buff[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff = (frizz_fp4w*)_buff;
	static float _buff1[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff1 = (frizz_fp4w*)_buff1;
	static float _buff2[MOVING_AVG_NUM] = {0};
	frizz_fp4w *buff2 = (frizz_fp4w*)_buff2;
	frizz_fp4w sum4w;
	frizz_fp4w sum4w1;
	frizz_fp4w sum4w2;
	frizz_fp sum;
	int i = 0;
	static unsigned int ix;
	static float avgResult = 0;
	static float avgResult1 = 0;
	static float avgResult2 = 0;
	static unsigned char bTest = 1;

	float debug[9];

	frizz_fp result2;
	frizz_fp result3;
	frizz_fp result4;

	if( g_pDevIF->conv != 0 ) {
		//result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&tmp_data );

//offset (acceleration, gyro[rad])
#if 0
		debug[0] = (as_float(fz[5])/180)*3.14159265358979f;
		debug[1] = (as_float(fz[6])/180)*3.14159265358979f;
		debug[2] = (as_float(fz[7])/180)*3.14159265358979f;
		debug[3] = ((as_float(fz[5]) - 0.13f)/180)*3.14159265358979f;
		debug[4] = ((as_float(fz[6]) - 0.026f)/180)*3.14159265358979f;
		debug[5] = ((as_float(fz[7]) - 0.056f)/180)*3.14159265358979f;
		debug[6] = as_float(fz[5]);
		debug[7] = as_float(fz[6]);
		debug[8] = as_float(fz[7]);
#endif
#if 1 // debug 基板
		// calibration用セッティング
		//g_device.data[0] =  (fz[1] - as_frizz_fp(0.00));
		//g_device.data[1] =  (fz[2] + as_frizz_fp(0.00));
		//g_device.data[2] =  (fz[3] - as_frizz_fp(0.0));
		//g_device.data[4] = (as_frizz_fp((((as_float(fz[5]) + 0.0f )/180)*3.14159265358979f)));//0.132119745f
		//g_device.data[5] = (as_frizz_fp((((as_float(fz[6]) - 0.0f )/180)*3.14159265358979f)));//0.0277914219f
		//g_device.data[6] = (as_frizz_fp((((as_float(fz[7]) - 0.0f )/180)*3.14159265358979f)));//0.059203282f

		//2016/NOV/07
		//g_device.data[0] =  (fz[1] - as_frizz_fp(-0.03017592505f));
		//g_device.data[1] =  (fz[2] - as_frizz_fp(-0.051244599f));
		//g_device.data[2] =  (fz[3] - as_frizz_fp(0.314392112f));
		//g_device.data[4] = (as_frizz_fp((((as_float(fz[5]) - (-0.273257495f) )/180)*3.14159265358979f)));//-0.00476924306f
		//g_device.data[5] = (as_frizz_fp((((as_float(fz[6]) - (2.250895008f) )/180)*3.14159265358979f)));//0.03928552903f
		//g_device.data[6] = (as_frizz_fp((((as_float(fz[7]) - (4.258736964f) )/180)*3.14159265358979f)));//0.07432898283f

		//20170113 (Yellow)
		//g_device.data[0] =  (fz[1] - as_frizz_fp(-0.00995305830212501f));
		//g_device.data[1] =  (fz[2] - as_frizz_fp( 0.03292657656075000f));
		//g_device.data[2] =  (fz[3] - as_frizz_fp( 0.20368045936325000f));
		//gyro [rad]
		//g_device.data[4] = (as_frizz_fp((((as_float(fz[5]) - (5.962356348f) )/180)*3.14159265358979f)));
		//g_device.data[5] = (as_frizz_fp((((as_float(fz[6]) - (3.017434487f) )/180)*3.14159265358979f)));
		//g_device.data[6] = (as_frizz_fp((((as_float(fz[7]) - (6.944341562f) )/180)*3.14159265358979f)));


		//20170113 (black)
		g_device.data[0] =  (fz[1] - as_frizz_fp(-0.05897702676f));
		g_device.data[1] =  (fz[2] - as_frizz_fp(-0.03806326129f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( -0.96045343474f));
		//gyro [rad]
		g_device.data[4] = (as_frizz_fp((((as_float(fz[5]) - (-1.81372849265831f) )/180)*3.14159265358979f)));
		g_device.data[5] = (as_frizz_fp((((as_float(fz[6]) - (3.23664257221365f) )/180)*3.14159265358979f)));
		g_device.data[6] = (as_frizz_fp((((as_float(fz[7]) - (5.11196799549327f) )/180)*3.14159265358979f)));

		//2016 Sep
		//acceleration
		//g_device.data[0] =  (fz[1] - as_frizz_fp(0.00646018516));
		//g_device.data[1] =  (fz[2] + as_frizz_fp(0.0167059936));
		//g_device.data[2] =  (fz[3] - as_frizz_fp(0.2834239));
		//gyro
		//g_device.data[4] = (as_frizz_fp((((as_float(fz[5]) + 0.642264485f )/180)*3.14159265358979f)));//0.132119745f
		//g_device.data[5] = (as_frizz_fp((((as_float(fz[6]) - 1.83674169f )/180)*3.14159265358979f)));//0.0277914219f
		//g_device.data[6] = (as_frizz_fp((((as_float(fz[7]) - 4.06922436f )/180)*3.14159265358979f)));//0.059203282f
#elif 1	// 展示会基板
		//20161107
		g_device.data[0] =  fz[1] - as_frizz_fp(-0.1452084705f);
		g_device.data[1] =  fz[2] - as_frizz_fp(-0.09134782f);
		g_device.data[2] =  fz[3] - as_frizz_fp(0.3914056540f);
		g_device.data[4] = as_frizz_fp((((as_float(fz[5]) - 7.405862181f )/180)*3.14159265358979f));//0.1292566791f
		g_device.data[5] = as_frizz_fp((((as_float(fz[6]) - 1.287862675f )/180)*3.14159265358979f));//0.02247744463f
		g_device.data[6] = as_frizz_fp((((as_float(fz[7]) - 3.182426235f )/180)*3.14159265358979f));//0.05554381609f
#else
		g_device.data[4] = as_frizz_fp(as_float(fz[5]));//0.132119745f
		g_device.data[5] = as_frizz_fp(as_float(fz[6]));//0.0277914219f
		g_device.data[6] = as_frizz_fp(as_float(fz[7]));//0.059203282f
#endif
#if 0
		deb2[0][debhead2  ] = (as_float(fz[5])*3.14159265358979f)/180.0f;
		deb2[1][debhead2  ] = (as_float(fz[6])*3.14159265358979f)/180.0f;
		deb2[2][debhead2++] = (as_float(fz[7])*3.14159265358979f)/180.0f;
		if ( debhead2 >= 64 ) {
			debhead2 = 0;
		}
		debsum2[0] = 0;
		debsum2[1] = 0;
		debsum2[2] = 0;
		for ( i = 0; i < 64; i++ ) {
			debsum2[0] += deb2[0][i];
			debsum2[1] += deb2[1][i];
			debsum2[2] += deb2[2][i];
		}
		avg[0] = debsum2[0] / 64;
		avg[1] = debsum2[1] / 64;
		avg[2] = debsum2[2] / 64;
#endif

#if 1
		if ( MOVING_AVG_NUM <= ix ) {
			asm( "nop" );
		}

		//　一番古い値を最新の値で上書き
		_buff[ix] = as_float(fz[1]);
		_buff1[ix] = as_float(fz[2]);
		_buff2[ix] = as_float(fz[3]);
		ix = ix + 1;
		if ( MOVING_AVG_NUM <= ix ) {
			ix = 0;
			bTest = 1;
		}

		// １６点分のデータを足す
		sum4w = buff[0];
		sum4w1 = buff1[0];
		sum4w2 = buff2[0];
		//quart_out_raw( "%d[%f %f %f %f]\r\n", 0, as_float(((frizz_fp*)&sum4w)[0]),
		//		as_float(((frizz_fp*)&sum4w)[1]),
		//		as_float(((frizz_fp*)&sum4w)[2]),
		//		as_float(((frizz_fp*)&sum4w)[3]) );
		for ( i = 1; i < (MOVING_AVG_NUM>>2); i++ ) {
			sum4w += buff[i];	// 4w分一度に足される？
			sum4w1 += buff1[i];	// 4w分一度に足される？
			sum4w2 += buff2[i];	// 4w分一度に足される？
			//quart_out_raw( "%d[%f %f %f %f]\r\n", i, as_float(((frizz_fp*)&sum4w)[0]),
			//		as_float(((frizz_fp*)&sum4w)[1]),
			//		as_float(((frizz_fp*)&sum4w)[2]),
			//		as_float(((frizz_fp*)&sum4w)[3]) );
		}
		//quart_out_raw( "sum result = %f\r\n", as_float(frizz_tie_vreduc(sum4w)) );
		result2 = frizz_div( frizz_tie_vreduc(sum4w), as_frizz_fp(128.0f) );
		result3 = frizz_div( frizz_tie_vreduc(sum4w1), as_frizz_fp(128.0f) );
		result4 = frizz_div( frizz_tie_vreduc(sum4w2), as_frizz_fp(128.0f) );
		// 加速度Xとその移動平均の値を表示
		//quart_out_raw( "-- %f %f\r\n", as_float(fz[2]) , as_float(result2) );

		//printf( "-- %f %f\r\n", as_float(fz[1]) , as_float(result2) );
#endif
	}
#if 1
	if ( 1 == bTest ) {
		avgResult = as_float(result2);
		avgResult1 = as_float(result3);
		avgResult2 = as_float(result4);
		bTest = 0;
	}
#endif
	g_device.f_need = 0;
	return result;
#endif
}

static unsigned int condition( void )
{
	unsigned int	result = 0, res_cond;

	result = get_device_condition( g_device.id );
	if( g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] != 0 ) {
		res_cond = ( *g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] )( 0 );
		if( ( D_RAW_DEVICE_ERR_READ & res_cond ) != 0 ) {
			set_device_condition_phyerr( g_device.id );
		} else {
			reset_device_condition_phyerr( g_device.id );
		}
	}
	return result;
}

sensor_if_t* DEF_INIT( accl_raw )( void )
{
	// ID
	g_device.id = ACCEL_RAW_ID;

	// init hardware
	if( ( g_pDevIF = basedevice_init( &g_devif[0], NELEMENT( g_devif ), g_device.id ) ) == 0 ) {
		return 0;
	}

	if( g_pDevIF->get_name != 0 ) {
		g_accel_name = ( *g_pDevIF->get_name )();
	}

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.get.condition = condition;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	// param
	g_device.f_active = 0;
	g_device.tick = 1000;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.remain_total = 0;
	// data
	//g_device.data = as_frizz_fp( 0.0f );

	return &( g_device.pif );
}

