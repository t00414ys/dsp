/*!******************************************************************************
 * @file frizz_env.h
 * @brief for frizz Environment definition file
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_ENV_H__
#define __FRIZZ_ENV_H__


/////////////////////////////////////////////////////////////////////
//
//
//		By your environment, you must edit this file
//
//


/*
 * This is Version information.
 */
// Customer Name (MAX 4 Byte)
#define		FRIZZ_CUSTOMER_ID			0x11100016

// Frizz Version (hexadecimal number, Total 4 Byte)
//   Please describe a decimal number
#define		FRIZZ_VER_MAJOR				1				// Major Version  (hexadecimal number, MAX 1 Byte)
#define		FRIZZ_VER_MINOR				2				// Minor Version  (hexadecimal number, MAX 1 Byte)
#define		FRIZZ_VER_DETAIL			0				// Detail Version (hexadecimal number, MAX 2 Byte)


/*
 * This is H/W setting. Please make sure customer's design
 *(DEFINITED) : use internal OSC,
 *(~DEFINITED): use external OSC
 */
//#define USE_INTERNAL_CLOCK			(1)

#if defined(USE_INTERNAL_CLOCK)
/*
 * This is use internal XOSC awake timer cycle
 */
#define	D_AWAKE_INT_CYCLE			(1)			// Awake Cycle 10ms
#define	D_AWAKE_INT_CYCLE_MAX		(1000)			// Awake Cycle 1000ms . Max.
/*
*/
//#define USE_MODE_SLEEP_LOW_CLOCK
//#define USE_MODE_STOP_LOW_CLOCK


/*
 * This is GPIO to be notified from host to frizz(use low sleep mode) (2 or 3)
 */
#define	LOW_SPEED_GPIO				3
/*
*/
#define	AWAKER_OSC1_ENABLE_GPIO		0


/*
 * This specifies whether or not to the PUSH-CHANGE-STATE at the time of start-up.
 *(DEFINITED) : REG_FIFO_CNR is not 0 to HIGH SPEED SLEEP MODE
 *(~DEFINITED): REG_FIFO_CNR is not 0 to LOW SPEED SLEEP MODE
 */
//#define	USE_LOW_SPEED_FIFO_CHK
/*
 * This specifies to decrease the current further with non-activated sensors
 *(DEFINITED) : use to save power consumption further. From 180uA to 150uA
 *(~DEFINITED): Not to use.
 */
//#define	USE_SUPER_POWER_SAVING				// test version

#else
/*
 * This is use external XOSC awake timer cycle
 */
#define	D_AWAKE_INT_CYCLE			(1)				// Awake Cycle 1ms
#define	D_AWAKE_INT_CYCLE_MAX		(1000)			// Awake Cycle 1000ms . Max.
#endif


/*
 * This is use Internal OSC Calibration
 *(DEFINITED) : use OSC Calibration
 *(~DEFINITED): disable
 */
//#define USE_OSC_CALIBRATION			(1)

#if !defined(USE_INTERNAL_CLOCK)
#undef	USE_OSC_CALIBRATION
#endif
#if defined(USE_OSC_CALIBRATION)
	#define	CALIBRATION_HOST_TO_FRIZZ	GPIO_NO_3
	#define	CALIBRATION_FRIZZ_TO_HOST	GPIO_NO_0
#define	CALIBRATION_HOST_GPIO
#endif

/*
 * This is GPIO to be notified from frizz to host
 */
#define	FRIZZ_TO_HOST_GPIO				(-1)				// -1:Not Use
#define	FRIZZ_TO_HOST_DEFALT_DEVEL		(0)					//



/*
 * This is use FIFO break code(0x00FF8FFF)
 */
//#define	USE_FIFO_BREAK_CODE

/*
 * This is Timer used information
 */

#define	TIMER_ID_DELAY					(0)
#define	TIMER_ID_CALIBRATION			(0)
#define	TIMER_ID_HABHALOUT				(2)
#define	TIMER_ID_UNUSE_BLOCKING			(1)



/*
 * This specifies whether or not to the PUSH-CHANGE-STATE at the time of start-up.
 * 0 : It is not used at startup
 * !0: It is used at startup
 */
#define	USE_DEFAULT_PUSH_CHANGE_STATE	(0)



/*
 * This is private test flow. Please DISABLE it when you release code.
 *(DEFINITED) : use uart as host interface
 *(~DEFINITED): disable
 */
//#define	USE_HOST_API_UART_IN


/*
 * This is private test flow. Please DISABLE it when you release code.(Output FIFO and UART)
 *(DEFINITED) : show message like Release mode
 *(~DEFINITED): disable
 */
//#define	USE_HOST_API_UART_OUT


/*
 * This is private test flow. Please DISABLE it when you release code.
 *(DEFINITED) : use uart to show message
 *(~DEFINITED): disable
 */
//#define USE_QUART_OUT		// output via UART as ascii string


/*
 * This is private test flow. It would help you to debug in the release mode
 *(DEFINITED) : use uart to show message
 *(~DEFINITED): disable
 */
//#define USE_QUART_OUT_RELEASE


/*
 * This is private test flow. Please DISABLE it when you release code.
 * UNUSE_BLOCKING: use counter to count time
 * UNUSE_BLOCKING_TIMER: use internal timer to count time
 */
//#define UNUSE_BLOCKING			// without blocking via counter(+10)
//#define UNUSE_BLOCKING_TIMER		// without blocking via timer


/*
 * This is input via QueueIF
 *(DEFINITED) : input via QueueIF
 *(~DEFINITED): disable
 */
//#define USE_DEBUG_IN		// input via QueueIF


/*
 * This is use activate and interval test data.(use self boot)
 *(DEFINITED) : use activate and interval test data
 *(~DEFINITED): disable
 */
#define USE_TEST_DATA		// use activate and interval test data

/*
 * This is use Time bomb to testing firmware
 *(DEFINITED) : use time bomb for testing firmware
 *(~DEFINITED): disable
 */
//#define	USE_TIME_BOMB_FOR_TEST_FW
#if defined(USE_TIME_BOMB_FOR_TEST_FW)
#define	TEST_FW_TIMER_MS	(48*60*60*1000)  // 48 hours to set time bomb
#endif

/*
 * This is use to record sensor status. Basically, it would waste 30uA while running.
 *(DEFINITED) : use this to record information. If customers need these, please set
 *(~DEFINITED): disable
 */
//#define	USE_GET_SENSOR_OPE_STATE


/*
 * This is the structure that defines the internal operation
 */
struct st_frizz_mode {
	__attribute__( ( aligned( 16 ) ) )
	unsigned int	start_delimiter;			// search for mark
	unsigned int	customer_id;				// customer number
	unsigned int	frizz_version;				// frizz_version
	unsigned int	clock_source;				// external(0) or internal(!0)
	unsigned int	internal_mode;				// high sleep(0,other) , low sleep(1) , low stop(2)
	unsigned int	awake_cycle;				// 1~1000(D_AWAKE_INT_CYCLE_MAX)
	unsigned int	high_sleep_switch_source;	// GPIO to determine the switching of the high-speed processing(0~3)
	unsigned int	awaker_osc1_enable_gpio;	// change low speed mode gpio
	unsigned int	bomb_for_test;				// use Time bomb to testing firmware
	unsigned int	test_fw_timer_ms;			// bomb timer
	char			guard_string[16];			// guard string
	unsigned int	stopt_delimiter;			// search for mark;
};

extern const	struct st_frizz_mode	frizz_ini_mode0;
extern volatile	struct st_frizz_mode	frizz_use_mode;

#define	D_NO_BOMB_MARK	0x73616665

//
// Nummber to String change Marco
//
#define TO_STRING(s)	STRING(s)
#define STRING(s)		#s


#endif//__FRIZZ_ENV_H__
