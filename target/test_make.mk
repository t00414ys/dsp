#######
#
# frizz subdir Makefile (HUB)
#

####
# set global macro
ifndef WORKSPACE_LOC
PWD   := $(shell pwd)
WORKSPACE_LOC =$(PWD)/..
endif
include $(WORKSPACE_LOC)/make.rules

####
# set target
CWD				:= $(shell pwd)

ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
TARGET_DIR		= bin
else
ifneq ($(X86_64),-m32)
TARGET_DIR		= x64_obj
else
TARGET_DIR		= x86_obj
endif
endif

ifeq ($(ENVIRONMENT),RUN_ON_FRIZZ)
LIBDIR			= $(WORKSPACE_LOC)/objs/bin
else
LIBDIR			= $(WORKSPACE_LOC)/objs/bin/$(TARGET_DIR)
endif

TARGET			= $(TARGET_DIR)/HUB_DEMO


####
# set soruce file
include ./source_file.inc



##########
# The rule is set. 
SRCS_FNAME			= $(notdir $(SRCS))
OBJS				= $(SRCS:%.c=$(TARGET_DIR)/%.o)
OBJS_REAL			= $(SRCS_FNAME:%.c=$(TARGET_DIR)/%.o)


all:	$(TARGET)

$(TARGET): $(OBJS)
	@mkdir -p $(TARGET_DIR)
	$(CC)  -o $@ $(OBJS_REAL) $(X86_64) -Wl,--no-whole-archive $(LIBS) $(GENLIB)

$(TARGET_DIR)/%.o: %.c
	@mkdir -p $(TARGET_DIR)
	$(CC)  $(INCS) $(CFLAGS) -c $(CWD)/$*.c   -o $(CWD)/$(TARGET_DIR)/$(*F).o

$(TARGET_DIR)/%.o: %.cpp
	@mkdir -p $(TARGET_DIR)
	$(CXX) $(INCS) $(CFLAGS) -c $(CWD)/$*.coo -o $(CWD)/$(TARGET_DIR)/$(*F).o

clean:
	rm -rf $(TARGET_DIR) $(LIBRARIES)

