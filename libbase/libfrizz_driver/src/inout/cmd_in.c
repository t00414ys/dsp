/*!******************************************************************************
 * @file cmd_in.c
 * @brief input for command to FrizzFW
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/tie/frizz_simd4w_que.h>
#include "command.h"
#include "mes.h"
#include "dataform.h"
#include "data_out.h"
#include "cmd_in.h"

/**
 * @brief command reading state
 */
typedef enum {
	CMD_IN_STATE_NONE = 0,		///< idle state
	CMD_IN_STATE_READING,		///< reading state
	CMD_IN_STATE_CMD,			///< command complete
	CMD_IN_STATE_WAIT_RES,		///< wait for response
	CMD_IN_STATE_NOTSUPPORT,	///< command not support
} eCmdIn_State;

/**
 * @brief command management information
 */
typedef struct {
	CMD_IN_INPUT_TYPE		type;	///< input type
	eCmdIn_State			state;	///< read command state
	unsigned int			params;	///< the number of the parameter acquisition
	FRIZZ_COMMAND_HEADER	head;	///< getting command header
	FRIZZ_COMMAND_FORM		data;	///< general purpose parameter format
} stCmdIn_Info;

stCmdIn_Info g_cmd_info;

static int cmd_in_sub_check_id_params( FRIZZ_COMMAND_ID id, unsigned int params )
{
	int ret = 0;
	switch( id ) {
	// none
	case FRIZZ_COMMAND_ID_GET_FW_VERSION:
	case FRIZZ_COMMAND_ID_INIT:
	case FRIZZ_COMMAND_ID_END:
	case FRIZZ_COMMAND_ID_CALIB_START:
	case FRIZZ_COMMAND_ID_CALIB_STOP:
	case FRIZZ_COMMAND_ID_PDR_START:
	case FRIZZ_COMMAND_ID_PDR_STOP:
	case FRIZZ_COMMAND_ID_PDR_SET_STRIDE_MODE:
	case FRIZZ_COMMAND_ID_PDR_SET_HOLD_MODE:
	case FRIZZ_COMMAND_ID_PDR_CLEAR_STEP_CNT:
	case FRIZZ_COMMAND_ID_BREAK:
		if( params != 0 ) {
			ret = -1;
		}
		break;
	// x1
	case FRIZZ_COMMAND_ID_SET_DELAY:
	case FRIZZ_COMMAND_ID_PDR_SET_FIX_STRIDE:
	case FRIZZ_COMMAND_ID_PDR_SET_YAW_OFST:
		if( params != 1 ) {
			ret = -1;
		}
		break;
	// x2
	case FRIZZ_COMMAND_ID_PDR_SET_WALK_PARAM:
	case FRIZZ_COMMAND_ID_PDR_SET_POS_OFST:
		if( params != 2 ) {
			ret = -1;
		}
		break;
	// x3
	case FRIZZ_COMMAND_ID_PDR_SET_GEOMAG_PARAM:
	case FRIZZ_COMMAND_ID_CALIB_SET_HARDIRON_PARAM:
		if( params != 3 ) {
			ret = -1;
		}
		break;
	// x9
	case FRIZZ_COMMAND_ID_CALIB_SET_SOFTIRON_PARAM:
		if( params != 9 ) {
			ret = -1;
		}
		break;
	default:
		ret = -1;
		break;
	}
	return ret;
}

/**
 * @brief Message register interrupt handler
 *
 * @param p command management information
 * @param data Message register content
 */
void cmd_in_sub_irq( void* p, unsigned int data )
{
	stCmdIn_Info *obj = ( stCmdIn_Info* )p;

	if( data == FRIZZ_COMMAND_BREAK_CODE ) {
		obj->params = 0;
		obj->head.cmd_id = FRIZZ_COMMAND_ID_BREAK;
		obj->head.param_num = 0;
		obj->state = CMD_IN_STATE_CMD;
	} else if( obj->state == CMD_IN_STATE_READING ) {
		obj->data.w[obj->params] = data;
		obj->params++;
		if( obj->head.param_num == obj->params ) {
			obj->state = CMD_IN_STATE_CMD;
		}
	} else if( obj->state == CMD_IN_STATE_NONE ) {
		obj->params = 0;
		obj->head.w = data;
		if( cmd_in_sub_check_id_params( ( FRIZZ_COMMAND_ID ) obj->head.cmd_id, obj->head.param_num ) ) {
			obj->state = CMD_IN_STATE_NOTSUPPORT;
		} else {
			if( obj->head.param_num == obj->params ) {
				obj->state = CMD_IN_STATE_CMD;
			} else {
				obj->state = CMD_IN_STATE_READING;
			}
		}
	}
}

static void cmd_int_sub_response( unsigned short imm )
{
	FRIZZ_DATA_HEADER head;
	FRIZZ_DATA_FORM_RES data;

	// header
	head.prefix = FRIZZ_DATA_PREFIX;
	head.data_id = FRIZZ_DATA_ID_RES;
	head.num = sizeof( FRIZZ_DATA_FORM_RES ) / sizeof( int );
	// data
	data.cmd_id = g_cmd_info.head.cmd_id;
	data.param_num = g_cmd_info.params;
	data.imm = imm;
	// output
	oq_sensor_out_push( head.w );
	oq_sensor_out_push( data.w[0] );
}

/**
 * @brief initialize
 *
 * @param input type: #CMD_IN_INPUT_TYPE
 */
void cmd_in_init( CMD_IN_INPUT_TYPE type )
{
	g_cmd_info.state = CMD_IN_STATE_NONE;
	g_cmd_info.type = type;
	if( type == CMD_IN_INPUT_MES ) {
		mes_init( cmd_in_sub_irq, &g_cmd_info );
	}
}

/**
 * @brief get frizz command
 *
 * @param id command ID
 * @param imm immediate
 * @param data output data
 *
 * @return 0: command exist, 1:nothing command
 */
int cmd_in_get_cmd( FRIZZ_COMMAND_ID *id, unsigned short *imm, FRIZZ_COMMAND_FORM **data )
{
	int res = 0;

	if( g_cmd_info.type == CMD_IN_INPUT_QUEUE ) {
		if( iq_sensor_in_is_ready() == 0 ) {
			return res;
		} else {
			cmd_in_sub_irq( &g_cmd_info, iq_sensor_in_pop() );
		}
	}
	if( g_cmd_info.state == CMD_IN_STATE_CMD ) {
		if( g_cmd_info.head.cmd_id == FRIZZ_COMMAND_ID_BREAK ) {
			cmd_int_sub_response( 0 );
			g_cmd_info.state = CMD_IN_STATE_NONE;
		} else {
			res = 1;
			*id = ( FRIZZ_COMMAND_ID ) g_cmd_info.head.cmd_id;
			*imm = g_cmd_info.head.imm;
			*data = &( g_cmd_info.data );
			g_cmd_info.state = CMD_IN_STATE_WAIT_RES;
		}
	} else if( g_cmd_info.state == CMD_IN_STATE_READING ) {
		cmd_int_sub_response( FRIZZ_DATA_ID_RES_CODE_CONTINUE );
	} else if( g_cmd_info.state == CMD_IN_STATE_NOTSUPPORT ) {
		cmd_int_sub_response( FRIZZ_DATA_ID_RES_CODE_NOTSUPPORT );
		g_cmd_info.state = CMD_IN_STATE_NONE;
	}

	return res;
}

/**
 * @brief output response for command
 *
 * @param imm result 0x8000 -> changed -1 value (don't use)
 *
 * @note if getting command in cmd_in_get_cmd(),this function must call.
 *  the command is not accepted until I carry it out.
 */
void cmd_in_res_cmd( unsigned short imm )
{
	// state check
	if( g_cmd_info.state == CMD_IN_STATE_WAIT_RES ) {
		// para check
		if( imm == FRIZZ_DATA_ID_RES_CODE_CONTINUE ) {
			imm = -1;
		}
		cmd_int_sub_response( imm );
		g_cmd_info.state = CMD_IN_STATE_NONE;
	}
}
