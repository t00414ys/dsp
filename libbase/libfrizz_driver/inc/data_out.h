/*!******************************************************************************
 * @file data_out.h
 * @brief output function for frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __DATA_OUT_H__
#define __DATA_OUT_H__

#include <xtensa/tie/frizz_simd4w_que.h>
#include "command.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief general purpose output
 *
 * @param id data ID #FRIZZ_DATA_ID
 * @param num payload word number
 * @param p payload
 */
void data_out_generic( unsigned char id, unsigned char num, void *p );

/**
 * @brief FW version output
 *
 * @param ver firmware version
 */
void data_out_fw_version( unsigned int ver );

/**
 * @brief 9D sensor output
 *
 * @param gyro[3] gyro 0:x, 1:y, 2:z
 * @param accl[3] accl 0:x, 1:y, 2:z
 * @param magn[3] magn 0:x, 1:y, 2:z
 * @param dtm sampling time
 */
void data_out_9dsensors( frizz_tie_fp gyro[3], frizz_tie_fp accl[3], frizz_tie_fp magn[3], frizz_tie_fp dtm );

/**
 * @brief 3D accel  sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_accl( frizz_tie_fp data[3] );

/**
 * @brief 3D magnet sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_magn( frizz_tie_fp data[3] );

/**
 * @brief 3D gyro sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_gyro( frizz_tie_fp data[3] );

/**
 * @brief pressure sensor output
 *
 * @param data[3] 0:x, 1:y, 2:z
 */
void data_out_baro( frizz_tie_fp data );

/**
 * @brief slope sensor output
 *
 * @param data[3] 0:yaw, 1:pitch, 2:roll
 */
void data_out_orientation( frizz_tie_fp data[3] );

/**
 * @brief PDR positioning result output
 *
 * @param loc[2] relative position 0:x, 1:y
 * @param vel[2] relative speed 0:x, 1:y
 * @param state PDR state
 * @param step_cnt step count
 * @param total_dst accumulation distance(m)
 */
void data_out_pdr_output( frizz_tie_fp loc[2], frizz_tie_fp vel[2], unsigned int state, unsigned int step_cnt, frizz_tie_fp total_dst );

/**
 * @brief Hard-Iron parameter output
 *
 * @param data[3] Hard-Iron parameter
 */
void data_out_hardiron_param( frizz_tie_fp data[3] );

/**
 * @brief Soft-Iron parameter output
 *
 * @param data[9] Soft-Iron parameter
 */
void data_out_softiron_param( frizz_tie_fp data[9] );

#ifdef __cplusplus
}
#endif

#endif//__DATA_OUT_H__
