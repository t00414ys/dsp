/*!******************************************************************************
 * @file mcc_mem.c
 * @brief MCC Library for dynamic memory allocation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "mcc_mem.h"

// structure for memory manage table
typedef struct {
	unsigned char* top_addr;
	unsigned char* end_addr; // addr + size
	unsigned char* next;
} sMccMemTable;

static sMccMemTable g_mcc_mem_table;

// define
#define TOP		(g_mcc_mem_table.top_addr)
#define END		(g_mcc_mem_table.end_addr)
#define NEXT	(g_mcc_mem_table.next)

int mcc_mem_init( void* addr, const unsigned int size )
{
	// check parameter
	if( addr == 0 || size == 0 ) {
		return -2;
	}
	// calc offset
	TOP = ( unsigned char* )( addr );
	NEXT = TOP;
	END = TOP + size;
	// success
	return 0;
}

void* mcc_mem_malloc( const unsigned int align, const unsigned int size )
{
	unsigned char *addr = NEXT;
	unsigned long mask, ofst;
	// check parameter
	if( align == 0 || size == 0 ) {
		return 0;
	}
	if( ( align ^ ( align - 1 ) ) + 1 != align << 1 ) {
		return 0;
	}
	// calc address
	mask = align - 1;
	ofst = ( unsigned long )addr & mask;
	ofst--;
	ofst = mask & ~ofst;
	addr += ofst;
	// check boundary
	if( END < ( addr + size ) ) {
		addr = 0;
	} else {
		NEXT = addr + size;
	}
	return ( void* )addr;
}

void mcc_mem_free( void *ptr )
{
	return;
}

#ifdef MCC_MEM_DEBUG
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ALLOC_SIZE	(256)
unsigned char g_allocate_area[ALLOC_SIZE];

int main( int argc, char* argv )
{
	int i;

	srand( ( unsigned int )time( NULL ) );
	mcc_mem_init( g_allocate_area + 1, sizeof( g_allocate_area ) - 1 );
	printf( "TOP: %08x,\tEND: %08x,\tNEXT: %08x\n", ( unsigned int ) TOP, ( unsigned int ) END, ( unsigned int ) NEXT );

	for( i = 0; i < 40; i++ ) {
		unsigned int size, align;
		size = ( unsigned int )( ( ( double )rand() ) / RAND_MAX * 256 );
		do {
			align = ( unsigned int )( ( ( double )rand() ) / RAND_MAX * 256 );
		} while( ( align ^ ( align - 1 ) ) + 1 != align << 1 );
		void* addr = mcc_mem_malloc( align, size );
		//if(addr == NULL) continue;
		printf( "TOP: %08x,\tEND: %08x,\tNEXT: %08x,\taddr: %08x,\tsize: %x,\talign: %x\n",
				( unsigned int )TOP, ( unsigned int )END, ( unsigned int )NEXT, ( unsigned int ) addr, ( unsigned int ) size, ( unsigned int ) align );
	}
	return 0;
}
#endif

