/*!******************************************************************************
 * @file frizz_fft.c
 * @brief const value table
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include "frizz_type.h"
#include "frizz_mem.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "frizz_fft.h"

typedef union {
	// cppcheck-suppress unusedStructMember
	frizz_fp	f;
	// cppcheck-suppress unusedStructMember
	int			i;
} frizz_fft_1w_conv_t;

typedef union {
	frizz_fp4w		w;
	frizz_fft_com_t c[2];
	// cppcheck-suppress unusedStructMember
	frizz_fp		f[4];
	// cppcheck-suppress unusedStructMember
	int				i[4];
} frizz_fft_4w_conv_t;

#ifndef FRIZZ_TIE_VER00002000
#define frizz_tie_im_mull(d0, s0, s1)						\
{															\
	frizz_fft_com_t d, *r0, *r1;							\
	int h = 0;												\
	r0 = (frizz_fft_com_t*)&(s0);							\
	r1 = (frizz_fft_com_t*)&(s1);							\
	d.re = r0[h].re * r1[h].re - r0[h].im * r1[h].im;		\
	d.im = r0[h].re * r1[h].im + r0[h].im * r1[h].re;		\
	((frizz_fft_com_t*)&(d0))[h].re = d.re;					\
	((frizz_fft_com_t*)&(d0))[h].im = d.im;					\
}

#define frizz_tie_im_mulh(d0, s0, s1)						\
{															\
	frizz_fft_com_t d, *r0, *r1;							\
	int h = 1;												\
	r0 = (frizz_fft_com_t*)&(s0);							\
	r1 = (frizz_fft_com_t*)&(s1);							\
	d.re = r0[h].re * r1[h].re - r0[h].im * r1[h].im;		\
	d.im = r0[h].re * r1[h].im + r0[h].im * r1[h].re;		\
	((frizz_fft_com_t*)&(d0))[h].re = d.re;					\
	((frizz_fft_com_t*)&(d0))[h].im = d.im;					\
}

#define frizz_tie_butterfly1(x)			\
{										\
	frizz_fft_4w_conv_t s, d;			\
	s.w = x;							\
	d.c[0].re = s.c[0].re + s.c[1].re;	\
	d.c[0].im = s.c[0].im + s.c[1].im;	\
	d.c[1].re = s.c[0].re - s.c[1].re;	\
	d.c[1].im = s.c[0].im - s.c[1].im;	\
	x = d.w;							\
}

frizz_fp4w frizz_tie_as_frizz_tie_fp( int x )
{
	frizz_fft_1w_conv_t s;
	frizz_fp4w w;
	s.i = x;
	// cppcheck-suppress uninitvar
	w *= FRIZZ_CONST_ZERO;
	w += s.f;
	return w;
}
#endif // FRIZZ_TIE_VER00002000

static void frizz_sub_butterfly_complex( frizz_fft_info_t *info, frizz_fft_com_t *data, int level, int f_forward )
{
	frizz_fp4w* f4w = ( frizz_fp4w* )data;

	int i, num;
	num = info->tap_num >> ( level + 2 );

	for( i = 0; i < num; i++ ) {
		frizz_fp4w lo = f4w[i] + f4w[i + num];
		frizz_fp4w hi = f4w[i] - f4w[i + num];
		f4w[i] = lo;
		f4w[i + num] = hi;
	}

	// mul coeff
	int inc = frizz_tie_bitswap( ( 1 << level ) - 1 ) >> ( 32 - info->tap_log );
	frizz_fp4w *coef = ( frizz_fp4w* )&info->tbl[inc];
	for( i = 0; i < num; i++ ) {
		frizz_fp4w tmp = f_forward ? coef[i] : frizz_tie_vneg( coef[i], 0xA );
		frizz_tie_im_mull( f4w[i + num], f4w[i + num], tmp );
		frizz_tie_im_mulh( f4w[i + num], f4w[i + num], tmp );
	}

	level++;
	if( 1 < num ) {
		frizz_sub_butterfly_complex( info, &data[0], level, f_forward );
		frizz_sub_butterfly_complex( info, &data[num << 1], level, f_forward );
	} else {
		frizz_tie_butterfly1( f4w[0] );
		frizz_tie_butterfly1( f4w[1] );
	}
}

static void frizz_fft_twiddle( frizz_fft_info_t *info, frizz_fft_com_t *data )
{

	int o_i, r_i, num_h, shift;
	frizz_fft_com_t tmp;

	num_h = info->tap_num >> 1;
	shift = 32 - info->tap_log;

	for( o_i = 0; o_i < num_h; o_i += 2 ) {
		r_i = frizz_tie_bitswap( o_i ) >> shift;
		if( o_i < r_i ) {
			// swapping the element of index [o_i] for [r_i]
			tmp.re = data[o_i].re;
			tmp.im = data[o_i].im;
			data[o_i].re = data[r_i].re;
			data[o_i].im = data[r_i].im;
			data[r_i].re = tmp.re;
			data[r_i].im = tmp.im;
			// swapping the element of index [o_i + num_h + 1] for [r_i + num_h + 1]
			tmp.re = data[o_i + num_h + 1].re;
			tmp.im = data[o_i + num_h + 1].im;
			data[o_i + num_h + 1].re = data[r_i + num_h + 1].re;
			data[o_i + num_h + 1].im = data[r_i + num_h + 1].im;
			data[r_i + num_h + 1].re = tmp.re;
			data[r_i + num_h + 1].im = tmp.im;
		}
		// swapping the element of index [o_i + num_h] for [r_i + 1]
		tmp.re = data[o_i + num_h].re;
		tmp.im = data[o_i + num_h].im;
		data[o_i + num_h].re = data[r_i + 1].re;
		data[o_i + num_h].im = data[r_i + 1].im;
		data[r_i + 1].re = tmp.re;
		data[r_i + 1].im = tmp.im;
	}

}

static void frizz_fft_scale( frizz_fft_info_t *info, frizz_fft_com_t *data )
{
	frizz_fp4w* f4w = ( frizz_fp4w* )data;
	frizz_fp4w scale;
	int i, num;
	int exp = 127 - info->tap_log;

	scale = frizz_tie_as_frizz_tie_fp( exp << 23 );
	num = info->tap_num >> 1;
	for( i = 0; i < num; i++ ) {
		f4w[i] *= scale;
	}
}

/**
 * @brief Create FFT Handler
 *
 * @param tap_log <number of points> = 2^tap_log
 *
 * @return handler for FFT Lib
 */
frizz_fft_info_t *frizz_fft_create( int tap_log )
{
	int i, num;
	frizz_fp N, theta;
	frizz_fft_info_t *info = ( frizz_fft_info_t* )frizz_malloc( sizeof( frizz_fft_info_t ) );

	if( info == 0 ) {
		return 0;
	}

	N = as_frizz_fp( 1 << tap_log );
	info->tap_log = tap_log;
	info->tap_num = 1 << tap_log;

	num = info->tap_num - 2;

	info->tbl = ( frizz_fft_com_t* )frizz_malloc( sizeof( frizz_fft_com_t ) * num );
	info->window = ( frizz_fp* )frizz_malloc( sizeof( frizz_fp ) * ( info->tap_num >> 1 ) );

	if( info->tbl == 0 || info->window == 0 ) {
		frizz_free( info->tbl );
		frizz_free( info->window );
		frizz_free( info );
	}

	theta = - ( FRIZZ_MATH_PI_x2 / N );
	int j, k = 0;
	for( i = 1; i < tap_log; i++ ) {
		for( j = 0; j < info->tap_num >> i; j++ ) {
			info->tbl[k + j].re = frizz_cos( as_frizz_fp( j ) * theta );
			info->tbl[k + j].im = frizz_sin( as_frizz_fp( j ) * theta );
		}
		k += info->tap_num >> i;
		theta *= as_frizz_fp( 2 );
	}

	for( i = 0; i < ( info->tap_num >> 1 ); i++ ) {	// Hanning window
		theta = FRIZZ_MATH_PI_x2 * as_frizz_fp( i ) / as_frizz_fp( ( info->tap_num >> 1 ) - 1 );
		info->window[i] = as_frizz_fp( 0.5 ) - as_frizz_fp( 0.5 ) * frizz_cos( theta );
	}

	return info;
}

/**
 * @brief Destroy FFT Handler
 *
 * @param [in] info handler for FFT Lib
 */
void frizz_fft_destroy( frizz_fft_info_t *info )
{
	if( info ) {
		frizz_free( info->tbl );
		frizz_free( info->window );
		frizz_free( info );
	}
}

/**
 * @brief FFT Transform for complex number data
 *
 * @param [inout] info handler for FFT Lib
 * @param [inout] data input complex number data
 */
void frizz_fft_transform( frizz_fft_info_t *info, frizz_fft_com_t *data )
{
	frizz_sub_butterfly_complex( info, data, 0, 1 );
	frizz_fft_twiddle( info, data );
}

/**
 * @brief FFT Inverse Transform
 *
 * @param [inout] info handler for FFT Lib
 * @param [inout] data input complex number data
 */
void frizz_fft_inverse( frizz_fft_info_t *info, frizz_fft_com_t *data )
{
	frizz_sub_butterfly_complex( info, data, 0, 0 );
	frizz_fft_twiddle( info, data );
	frizz_fft_scale( info, data );
}
