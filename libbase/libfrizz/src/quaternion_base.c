/*!******************************************************************************
 * @file    quaternion_base.c
 * @brief   virtual quaternion_base sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math.h"
#include "frizz_const.h"
#include "quaternion_base.h"

/**
 * @brief Create DCM (Rotation Matrix) from Quaternion
 *
 * @param [in] pq quaternion
 * @param [out] dcm[3] DCM
 */
void quaternion_base_create_dcm( frizz_fp4w_t *pq, frizz_fp4w_t dcm[3] )
{
	frizz_fp4w_t q, qq;
	q.w = pq->w;
	qq.w = pq->w * pq->w;
	// DCM: rn = DCM * rb
	// set X
	dcm[0].z[0] = qq.z[0] + qq.z[1] - qq.z[2] - qq.z[3];
	dcm[0].z[1] = FRIZZ_CONST_TWO * ( q.z[1] * q.z[2] - q.z[0] * q.z[3] );
	dcm[0].z[2] = FRIZZ_CONST_TWO * ( q.z[1] * q.z[3] + q.z[0] * q.z[2] );
	dcm[0].z[3] = FRIZZ_CONST_ZERO;
	// set Y
	dcm[1].z[0] = FRIZZ_CONST_TWO * ( q.z[1] * q.z[2] + q.z[0] * q.z[3] );
	dcm[1].z[1] = qq.z[0] - qq.z[1] + qq.z[2] - qq.z[3];
	dcm[1].z[2] = FRIZZ_CONST_TWO * ( q.z[2] * q.z[3] - q.z[0] * q.z[1] );
	dcm[1].z[3] = FRIZZ_CONST_ZERO;
	// set Z
	dcm[2].z[0] = FRIZZ_CONST_TWO * ( q.z[1] * q.z[3] - q.z[0] * q.z[2] );
	dcm[2].z[1] = FRIZZ_CONST_TWO * ( q.z[2] * q.z[3] + q.z[0] * q.z[1] );
	dcm[2].z[2] = qq.z[0] - qq.z[1] - qq.z[2] + qq.z[3];
	dcm[2].z[3] = FRIZZ_CONST_ZERO;
}

/**
 * @brief Generate Base quaternion from Acceleromater and Magnetometer
 *
 * @param [out] pq quaternion
 * @param [in] pa accelerometer
 * @param [in] pm magnetometer
 */
void quaternion_base_am_generate( frizz_fp4w_t *pq, frizz_fp4w *pa, frizz_fp4w *pm )
{
	frizz_fp4w_t dcm[4];
	pq->w *= FRIZZ_CONST_ZERO;
	pq->z[0] = FRIZZ_CONST_ONE;
	quaternion_base_create_dcm( pq, dcm );
	if( pa ) {
		frizz_fp4w_t v;
		frizz_fp cc;
		// accl
		v.w = *pa;
		v.z[3] = FRIZZ_CONST_ZERO;
		v.w *= frizz_inv_sqrt( frizz_tie_vreduc( v.w * v.w ) );
		pq->z[0] =
			pq->z[3] = FRIZZ_CONST_ZERO;
		pq->z[1] =  v.z[1];
		pq->z[2] = -v.z[0];
		cc = frizz_tie_vreduc( pq->w * pq->w );
		if( as_frizz_fp( 1.0e-5 ) < cc ) {
			pq->w *= frizz_inv_sqrt( cc );
		}
		// cos(v/2)^2 = (cos(v) + 1) / 2;
		cc = FRIZZ_CONST_HALF + FRIZZ_CONST_HALF * v.z[2];
		if( cc < as_frizz_fp( 1.0e-5 ) ) {
			pq->z[1] = FRIZZ_CONST_ONE;
		}
		pq->w *= frizz_sqrt( FRIZZ_CONST_ONE - cc );	// x sin(v/2)
		pq->z[0] = frizz_sqrt( cc );
		pq->w *= frizz_inv_sqrt( frizz_tie_vreduc( pq->w * pq->w ) );
		quaternion_base_create_dcm( pq, dcm );
		// magnet
		if( pm ) {
			v.w = *pm;
			v.z[3] = FRIZZ_CONST_ZERO;
			v.w -= frizz_tie_vreduc( v.w * dcm[2].w ) * dcm[2].w;
			cc = frizz_tie_vreduc( v.w * v.w );
			if( as_frizz_fp( 1.0e-5 ) < cc ) {
				frizz_fp4w_t qm, mat[4];
				v.w *= frizz_inv_sqrt( cc );
				// make angle half
				v.w += dcm[1].w;
				cc = frizz_tie_vreduc( v.w * v.w );
				if( as_frizz_fp( 1.0e-5 ) < cc ) {
					v.w *= frizz_inv_sqrt( cc );
					// axis of rotation : vector product of the unit vector => sin
					qm.z[0] = FRIZZ_CONST_ZERO;
					qm.z[1] = v.z[1] * dcm[1].z[2] - v.z[2] * dcm[1].z[1];
					qm.z[2] = v.z[2] * dcm[1].z[0] - v.z[0] * dcm[1].z[2];
					qm.z[3] = v.z[0] * dcm[1].z[1] - v.z[1] * dcm[1].z[0];
					qm.z[0] = frizz_sqrt( FRIZZ_CONST_ONE - frizz_tie_vreduc( qm.w * qm.w ) );
				} else {
					qm.z[0] = FRIZZ_CONST_ZERO;
					qm.z[1] = dcm[2].z[0];
					qm.z[2] = dcm[2].z[1];
					qm.z[3] = dcm[2].z[2];
				}
				/* update */
				// q = q x qm
				mat[0].z[0] =  pq->z[0];
				mat[0].z[1] = -pq->z[1];
				mat[0].z[2] = -pq->z[2];
				mat[0].z[3] = -pq->z[3];
				mat[1].z[0] =  pq->z[1];
				mat[1].z[1] =  pq->z[0];
				mat[1].z[2] = -pq->z[3];
				mat[1].z[3] =  pq->z[2];
				mat[2].z[0] =  pq->z[2];
				mat[2].z[1] =  pq->z[3];
				mat[2].z[2] =  pq->z[0];
				mat[2].z[3] = -pq->z[1];
				mat[3].z[0] =  pq->z[3];
				mat[3].z[1] = -pq->z[2];
				mat[3].z[2] =  pq->z[1];
				mat[3].z[3] =  pq->z[0];
				pq->z[0] = frizz_tie_vreduc( mat[0].w * qm.w );
				pq->z[1] = frizz_tie_vreduc( mat[1].w * qm.w );
				pq->z[2] = frizz_tie_vreduc( mat[2].w * qm.w );
				pq->z[3] = frizz_tie_vreduc( mat[3].w * qm.w );
				pq->w *= frizz_inv_sqrt( frizz_tie_vreduc( pq->w * pq->w ) );
			}
		}
	}
}

/**
 * @brief Update quaternion with Gyroscope
 *
 * @param [in,out] pq quaternion
 * @param [in] pg gyroscope
 * @param [in] dt sampling period [sec]
 */
void quaternion_base_g_update( frizz_fp4w_t *pq, frizz_fp4w *pg, frizz_fp dt )
{
	// Gradient descent
	frizz_fp4w_t dq, g, tmp[4];
	frizz_fp dmp;
	g.w = *pg * dt;
	frizz_tie_vshift_l( dmp, g.w, FRIZZ_CONST_ZERO );
	/* quaternion matrix; dot_q = 0.5 * Qm * omega */
	// set
	tmp[0].z[0] =  pq->z[0];
	tmp[0].z[1] = -pq->z[1];
	tmp[0].z[2] = -pq->z[2];
	tmp[0].z[3] = -pq->z[3];

	tmp[1].z[0] =  pq->z[1];
	tmp[1].z[1] =  pq->z[0];
	tmp[1].z[2] = -pq->z[3];
	tmp[1].z[3] =  pq->z[2];

	tmp[2].z[0] =  pq->z[2];
	tmp[2].z[1] =  pq->z[3];
	tmp[2].z[2] =  pq->z[0];
	tmp[2].z[3] = -pq->z[1];

	tmp[3].z[0] =  pq->z[3];
	tmp[3].z[1] = -pq->z[2];
	tmp[3].z[2] =  pq->z[1];
	tmp[3].z[3] =  pq->z[0];
	// update by gyro
	dq.z[0] = frizz_tie_vreduc( tmp[0].w * g.w );
	dq.z[1] = frizz_tie_vreduc( tmp[1].w * g.w );
	dq.z[2] = frizz_tie_vreduc( tmp[2].w * g.w );
	dq.z[3] = frizz_tie_vreduc( tmp[3].w * g.w );
	dq.w *= FRIZZ_CONST_HALF;
	pq->w += dq.w;
	pq->w *= frizz_inv_sqrt( frizz_tie_vreduc( pq->w * pq->w ) );
}

/**
 * @brief Update quaternion with Acceleromater and Magnetometer and Gyroscope
 *
 * @param [in,out] pq quaternion
 * @param [in] pa accelerometer
 * @param [in] pm magnetometer
 * @param [in] pg gyroscope
 * @param [in] dt sampling period [sec]
 */
void quaternion_base_amg_update( frizz_fp4w_t *pq, frizz_fp4w *pa, frizz_fp4w *pm, frizz_fp4w *pg, frizz_fp dt )
{
	frizz_fp4w_t dcm[3], gv, mv;
	quaternion_base_g_update( pq, pg, dt );
	quaternion_base_create_dcm( pq, dcm );
	if( pa ) {
		gv.w = *pa;
		gv.z[3] = FRIZZ_CONST_ZERO;
		gv.w *= frizz_inv_sqrt( frizz_tie_vreduc( gv.w * gv.w ) );
		gv.w = ( FRIZZ_CONST_ONE - dt ) * dcm[2].w + dt * gv.w;
	} else {
		gv.w = dcm[2].w;
	}
	if( pm ) {
		mv.w = *pm;
		mv.z[3] = FRIZZ_CONST_ZERO;
		mv.w *= frizz_inv_sqrt( frizz_tie_vreduc( mv.w * mv.w ) );
		mv.w = ( FRIZZ_CONST_ONE - dt ) * dcm[1].w + dt * mv.w;
	} else {
		mv.w = dcm[1].w;
	}
	quaternion_base_am_generate( pq, &gv.w, &mv.w );
}

