/*!******************************************************************************
 * @file  frizz_fp.h
 * @brief Frizz Float Type Header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_FP_H__
#define __FRIZZ_FP_H__

#if !defined(RUN_ON_PC)
#include <xtensa/tie/frizz_simd4w_que.h>
#ifdef __cplusplus
extern "C" {
#endif

frizz_tie_fp frizz_div( frizz_tie_fp, frizz_tie_fp );

#ifdef __cplusplus
}
#endif
// /
static inline frizz_tie_fp __xt_operator_TRUNC_DIV( frizz_tie_fp a, frizz_tie_fp b )
{
	return frizz_div( a, b );
}
#ifndef FRIZZ_TIE_VER00002000
#include "frizz_op.h"	// for upper compatible
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(RUN_ON_PC)
typedef frizz_tie_fp frizz_fp;
static inline frizz_fp as_frizz_fp( float f )
{
	return *( ( frizz_fp* )( &f ) );
}
static inline float as_float( frizz_fp z )
{
	return *( ( float* )( &z ) );
}

#else
typedef float frizz_fp;
static inline frizz_fp as_frizz_fp( float f )
{
	return f;
}
static inline float as_float( frizz_fp z )
{
	return z;
}

static inline unsigned int frizz_tie_bitswap( unsigned int x )
{
	x = ( ( ( x & 0xaaaaaaaa ) >> 1 ) | ( ( x & 0x55555555 ) << 1 ) );
	x = ( ( ( x & 0xcccccccc ) >> 2 ) | ( ( x & 0x33333333 ) << 2 ) );
	x = ( ( ( x & 0xf0f0f0f0 ) >> 4 ) | ( ( x & 0x0f0f0f0f ) << 4 ) );
	x = ( ( ( x & 0xff00ff00 ) >> 8 ) | ( ( x & 0x00ff00ff ) << 8 ) );
	return( ( x >> 16 ) | ( x << 16 ) );
}

#endif	// defined(RUN_ON_XT) && defined(ENABLE_TIE_SIMD4W)

typedef union {
	frizz_fp	z;
	float		f;
} frizz_fp_t;

#ifdef __cplusplus
}
#endif

#endif	// __FRIZZ_FP_H__

