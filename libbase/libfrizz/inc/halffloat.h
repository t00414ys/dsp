/*!******************************************************************************
 * @file  halffloat.h
 * @brief MCC Library for halffloat
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HALFFLOAT_H__
#define __HALFFLOAT_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief floatToShort
 *
 * @param float f
 * @param unsigned short data
 **/
extern unsigned short floatToShort( float f );

/**
 * @brief shortToFloat
 *
 * @param hunsigned short s
 * @param float data
 **/
extern float shortToFloat( unsigned short s );


#ifdef __cplusplus
}
#endif

#endif//__HALFFLOAT_H__
