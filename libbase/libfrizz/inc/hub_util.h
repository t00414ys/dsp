/*!******************************************************************************
 * @file  hub_util.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUB_UTIL_H__
#define __HUB_UTIL_H__

#define		RESULT_ERR_INIT				(-1)		// Raw Sensor Init Error
#define		RESULT_SUCCESS_INIT			(0)			// Raw Sensor Init Success

#define		RESULT_ERR_CONV				(0)			// CONV Error
#define		RESULT_SUCCESS_CONV			(1)			// CONV Success

#define		RESULT_ERR_CMD				(-4)		// setparam : NO SUCH COMMAND
#define		RESULT_ERR_SET				(-1)		// setparam Error
#define		RESULT_SUCCESS_SET			(0)			// setparam Success


#define		NELEMENT(a)					(sizeof(a)/sizeof(a[0]))


inline static unsigned int make_version( unsigned char major, unsigned char minor, unsigned short detail )
{
	unsigned int	version;
	version =	( ( major  << 24 ) | ( minor  << 16 ) | ( detail <<  0 ) );
	return version;
}

extern		unsigned int				g_frizz_version;

#endif
