/*!******************************************************************************
 * @file    hmc5883.c
 * @brief   Mc3413 physical sensor driver
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "gpio.h"
#include "frizz_peri.h"
#include "spi.h"
#include "timer.h"
#include "hmc5883.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_ADS8332

#define		DRIVER_VER_MAJOR		(1)				// Major Version
#define		DRIVER_VER_MINOR		(0)				// Minor Version
#define		DRIVER_VER_DETAIL		(0)				// Detail Version

struct {
	frizz_fp			scale_magn;		// scaler
	unsigned char		buff[12];	// transmission buffer
	unsigned char		spi_mode;
	unsigned int		cs_no;
	unsigned int		target_freq;
	setting_direction_t	setting;	// direction of device
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[6];
	//
} g_hmc5883;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char	g_init_done = D_INIT_NONE;


//declare a constant for hmc5883_reg_write and hmc5883_reg_read function.
enum spi_cs_keep {CS_TURN_UP = 0, CS_KEEP = 1};
static const unsigned int cmd_size = 1;
static const unsigned int addr_size = 1;
#if 0
static void hmc5883_reg_write( unsigned char addr, unsigned char* tx_data, unsigned char length )
{
	unsigned char write_command = 0x0a;
	unsigned char* no_read = 0;
	spi_trans_data( &write_command, no_read, cmd_size, g_hmc5883.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_hmc5883.cs_no, CS_KEEP );
	spi_trans_data( tx_data, no_read, length, g_hmc5883.cs_no, CS_TURN_UP );
}

static void hmc5883_reg_read( unsigned char addr, unsigned char* rx_data, unsigned char length )
{
	unsigned char read_command = 0x0b;
	unsigned char* no_write = 0;
	unsigned char* no_read = 0;
	spi_trans_data( &read_command, no_read, cmd_size, g_hmc5883.cs_no, CS_KEEP );
	spi_trans_data( &addr, no_read, addr_size, g_hmc5883.cs_no, CS_KEEP );
	spi_trans_data( no_write, rx_data, length, g_hmc5883.cs_no, CS_TURN_UP );
}
#endif
int hmc5883_init( unsigned int param )
{
	int dummy = 0;
	unsigned char read_data[4];
	unsigned int reg_dummy = 0;

	i2c_init( g_ROSC2_FREQ, 400000 );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void hmc5883_ctrl_magn( int f_ena )
{

}
void hmc5883_get_magn_data( unsigned char *buf )
{
	unsigned char read_data[6];


	g_hmc5883.recv_result = 0;
}

unsigned int hmc5883_rcv_magn( unsigned int tick )
{
	const unsigned char status_reg_addr = 0x0b;
	const unsigned char axis_data_reg_addr = 0x0e;
	const unsigned char data_ready_bit = 0x01;
	unsigned char status;

	hmc5883_get_magn_data( g_hmc5883.buff );

	return 0;
}

int hmc5883_conv_magn( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff1;			// x:0, y:1, z:2
	frizz_fp* fz1 = ( frizz_fp* )&f4w_buff1;
	float* fp1 = ( float* )&f4w_buff1;

	frizz_fp4w f4w_buff2;			// x:0, y:1, z:2
	frizz_fp* fz2 = ( frizz_fp* )&f4w_buff2;
	float* fp2 = ( float* )&f4w_buff2;

	sensor_util_half_t s_buff[6];	// x:0, y:1, z:2

	if( g_hmc5883.recv_result != 0 ) {
		data[0] = g_hmc5883.lasttime_data[0];
		data[1] = g_hmc5883.lasttime_data[1];
		data[2] = g_hmc5883.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using hmc5883 */
	s_buff[0].ubyte[0] = g_hmc5883.buff[ 0];	// XOUT_EX_L
	s_buff[0].ubyte[1] = g_hmc5883.buff[ 1];	// XOUT_EX_H
	s_buff[1].ubyte[0] = g_hmc5883.buff[ 2];	// YOUT_EX_L
	s_buff[1].ubyte[1] = g_hmc5883.buff[ 3];	// YOUT_EX_H
	s_buff[2].ubyte[0] = g_hmc5883.buff[ 4];	// ZOUT_EX_L
	s_buff[2].ubyte[1] = g_hmc5883.buff[ 5];	// ZOUT_EX_H

	/* data for Gyro using hmc5883 */
	s_buff[3].ubyte[0] = g_hmc5883.buff[ 6];	// XOUT_EX_L
	s_buff[3].ubyte[1] = g_hmc5883.buff[ 7];	// XOUT_EX_H
	s_buff[4].ubyte[0] = g_hmc5883.buff[ 8];	// YOUT_EX_L
	s_buff[4].ubyte[1] = g_hmc5883.buff[ 9];	// YOUT_EX_H
	s_buff[5].ubyte[0] = g_hmc5883.buff[10];	// ZOUT_EX_L
	s_buff[5].ubyte[1] = g_hmc5883.buff[11];	// ZOUT_EX_H

#if 1
	// convert magn
	data[1] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[0].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
	data[2] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[1].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
	data[3] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[2].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
	// convet magn
	data[5] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[3].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
	data[6] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[4].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
	data[7] = ((as_frizz_fp(3.3)*as_frizz_fp(s_buff[5].half)/as_frizz_fp(65535))-as_frizz_fp(1.65))/as_frizz_fp(0.0627);
#endif
#if 1
	fp1[0] = (float)s_buff[0].half;
	fp1[1] = (float)s_buff[1].half;
	fp1[2] = (float)s_buff[2].half;
	fp2[0] = (float)s_buff[3].half;
	fp2[1] = (float)s_buff[4].half;
	fp2[2] = (float)s_buff[5].half;
#if 0
	data[0] = (float)(((3.3f*fp1[0])/65535)-1.65f)/0.0627f;
	data[1] = (float)(((3.3f*fp1[1])/65535)-1.65f)/0.0627f;
	data[2] = (float)(((3.3f*fp1[2])/65535)-1.65f)/0.0627;

	data[3] = (float)(((3.3f*fp2[0]/65535))-1.50f)/0.0008f;
	data[4] = (float)(((3.3f*fp2[1]/65535))-1.50f)/0.0008f;
	data[5] = (float)(((3.3f*fp2[2]/65535))-1.50f)/0.0008f;
#endif
#endif
	g_hmc5883.lasttime_data[0] = data[0];
	g_hmc5883.lasttime_data[1] = data[1];
	g_hmc5883.lasttime_data[2] = data[2];
	g_hmc5883.lasttime_data[3] = data[3];
	g_hmc5883.lasttime_data[4] = data[4];
	g_hmc5883.lasttime_data[5] = data[5];


	return RESULT_SUCCESS_CONV;
}

int hmc5883_setparam_magn( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_hmc5883.setting.map_x		= setting->map_x;
		g_hmc5883.setting.map_y		= setting->map_y;
		g_hmc5883.setting.map_z		= setting->map_z;
		g_hmc5883.setting.negate_x	= setting->negate_x;
		g_hmc5883.setting.negate_y	= setting->negate_y;
		g_hmc5883.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int hmc5883_magn_get_condition( void *data )
{
	return g_hmc5883.device_condition;
}

int hmc5883_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_hmc5883.buff[1] << 8 ) | g_hmc5883.buff[0] );
	temp_buffer[1] = ( short )( ( g_hmc5883.buff[3] << 8 ) | g_hmc5883.buff[2] );
	temp_buffer[2] = ( short )( ( g_hmc5883.buff[5] << 8 ) | g_hmc5883.buff[4] );


	return	RESULT_SUCCESS_SET;
}

unsigned int hmc5883_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int hmc5883_get_name()
{
	return	D_DRIVER_NAME;
}


