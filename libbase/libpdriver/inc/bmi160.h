/*!******************************************************************************
 * @file    bmi160.h
 * @brief   bmi160 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMI160_H__
#define __BMI160_H__
#include "bmi160_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */

//#define ACC_2G_RANGE
//#define ACC_4G_RANGE
//#define ACC_8G_RANGE
#define ACC_16G_RANGE


/*
 * This is use gyro sensor range (To select either one)
 *(DEFINITED) : Enable  use gyro sensor range
 *(~DEFINITED): Disable  use gyro sensor range
 */
//#define GYR_2K_DIG_PAR_SEC_RANGE
#define GYR_1K_DIG_PAR_SEC_RANGE
//#define GYR_500_DIG_PAR_SEC_RANGE
//#define GYR_250_DIG_PAR_SEC_RANGE
//#define GYR_125_DIG_PAR_SEC_RANGE







/*
 *
 *		The rest of this is okay not change
 *
 */

#if defined(ACC_2G_RANGE)
#define BMI160_ACCL_RANGE				(0x03)
#define BMI160_ACCL_PER_LSB				(1./16384.)
#endif
#if defined(ACC_4G_RANGE)
#define BMI160_ACCL_RANGE				(0x05)
#define BMI160_ACCL_PER_LSB				(1./8192.)
#endif
#if defined(ACC_8G_RANGE)
#define BMI160_ACCL_RANGE				(0x08)
#define BMI160_ACCL_PER_LSB				(1./4096.)
#endif
#if defined(ACC_16G_RANGE)
#define BMI160_ACCL_RANGE				(0x0C)
#define BMI160_ACCL_PER_LSB				(1./2048.)
#endif

#if defined(GYR_2K_DIG_PAR_SEC_RANGE)
#define BMI160_GYRO_RANGE				(0x00)
#define RESOLUTION						(1./16.4)
#endif
#if defined(GYR_1K_DIG_PAR_SEC_RANGE)
#define BMI160_GYRO_RANGE				(0x01)
#define RESOLUTION						(1./32.8)
#endif
#if defined(GYR_500_DIG_PAR_SEC_RANGE)
#define BMI160_GYRO_RANGE				(0x02)
#define BMI160_GYRO_PER_LSB				(1./65.6)
#endif
#if defined(GYR_250_DIG_PAR_SEC_RANGE)
#define BMI160_GYRO_RANGE				(0x03)
#define RESOLUTION						(1./131.2)
#endif
#if defined(GYR_125_DIG_PAR_SEC_RANGE)
#define BMI160_GYRO_RANGE				(0x04)
#define RESOLUTION						(1./262.4)
#endif

#define DEG_TO_RAD_MODULUS				(0.0174532925)
#define BMI160_GYRO_PER_LSB				(RESOLUTION * DEG_TO_RAD_MODULUS)

#define BMI160_TEMPERATURE_PER_LSB		(1./512.)
#define BMI160_TEMPERATURE_OFFSET		(23.0)

#define CTRL_ACTIVATE					(1)
#define CTRL_DEACTIVATE					(0)

#define BMI160_I2C_ADDRESS_L			(0x68)		///< Accel & Gyro Sensor(made from BMI)
#define BMI160_I2C_ADDRESS_H			(0x69)		///< Accel & Gyro Sensor(made from BMI)

#define BMI160_REG_ID					(0x00)		// Product ID
#define BMI160_REG_CMD					(0x7E)
#define BMI160_REG_ACC					(0x12)
#define BMI160_REG_GYR					(0x0C)
#define BMI160_REG_TMP					(0x20)
#define BMI160_REG_ACC_RANGE			(0x41)
#define BMI160_REG_GYR_RANGE			(0x43)

#define BMI160_WHOAMI_ID				(0xD1)
#define BMI160_ACC_SUSPEND_MODE			(0x10)
#define BMI160_ACC_NOMAL_MODE			(0x11)
#define BMI160_ACC_LOWPOWER_MODE		(0x12)
#define BMI160_GYR_SUSPEND_MODE			(0x14)
#define BMI160_GYR_NOMAL_MODE			(0x15)

#endif
