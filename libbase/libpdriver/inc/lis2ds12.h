/*!******************************************************************************
 * @file    lis2ds12.h
 * @brief   lis2ds12 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __LIS2DS12_H__
#define __LIS2DS12_H__

#include "lis2ds12_api.h"



/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */
//#define		ACC_2G_RANGE
//#define		ACC_4G_RANGE
//#define		ACC_8G_RANGE
#define		ACC_16G_RANGE

#if defined(ACC_2G_RANGE)
#define LIS2DS12_ACCEL_ACC_RANGE		LIS2DS12_FS_2G
#define LIS2DS12_ACCEL_PER_LSB			(1./256.)
#endif
#if defined(ACC_4G_RANGE)
#define LIS2DS12_ACCEL_ACC_RANGE		LIS2DS12_FS_4G
#define LIS2DS12_ACCEL_PER_LSB			(1./128.)
#endif
#if defined(ACC_8G_RANGE)
#define LIS2DS12_ACCEL_ACC_RANGE		LIS2DS12_FS_8G
#define LIS2DS12_ACCEL_PER_LSB			(1./64.)
#endif
#if defined(ACC_16G_RANGE)
#define LIS2DS12_ACCEL_ACC_RANGE		LIS2DS12_FS_16G
#define LIS2DS12_ACCEL_PER_LSB			(1./32.)
#endif



#define LIS2DS12_I2C_ADDRESS_H 		0x1D
#define LIS2DS12_I2C_ADDRESS_L 		0x1E


//Registers
#define LIS2DS12_Module_8bit 			0x0C
#define LIS2DS12_WHO_AM_I 				0x0F
#define LIS2DS12_CTRL_REG1 				0x20
#define LIS2DS12_CTRL_REG2 				0x21
#define LIS2DS12_CTRL_REG3 				0x22
#define LIS2DS12_CTRL_REG4 				0x23
#define LIS2DS12_CTRL_REG5 				0x24
#define LIS2DS12_FIFO_CTRL 				0x25
#define LIS2DS12_OUT_T	 				0x26
#define LIS2DS12_STATUS		 			0x27
#define LIS2DS12_OUT_X_L 				0x28
#define LIS2DS12_OUT_X_H 				0x29
#define LIS2DS12_OUT_Y_L 				0x2A
#define LIS2DS12_OUT_Y_H 				0x2B
#define LIS2DS12_OUT_Z_L 				0x2C
#define LIS2DS12_OUT_Z_H 				0x2D
#define LIS2DS12_FIFO_THS	 			0x2E
#define LIS2DS12_FIFO_SRC	 			0x2F
#define LIS2DS12_FIFO_SAMPLES			0x30
#define LIS2DS12_TAP_6D_THS 			0x31
#define LIS2DS12_INT1_DUR 				0x32
#define LIS2DS12_WAKE_UP_THS 			0x33
#define LIS2DS12_WAKE_UP_DUR			0x34
#define LIS2DS12_FREE_FALL	 			0x35
#define LIS2DS12_STATUS_DUP				0x36
#define LIS2DS12_WAKE_UP_SRC 			0x37
#define LIS2DS12_TAP_SRC 				0x38
#define LIS2DS12_6D_SRC 				0x39
#define LIS2DS12_STEP_COUNTER_MINTHS	0x3A
#define LIS2DS12_STEP_COUNTER_L			0x3B
#define LIS2DS12_STEP_COUNTER_H 		0x3C
#define LIS2DS12_FUNC_CK_GATE 			0x3D
#define LIS2DS12_FUNC_SRC 				0x3E
#define LIS2DS12_FUNC_CTRL				0x3F

//Register Masks

//WHO_AM_I masks
#define LIS2DS12_I_AM_MASK 				0x43

// CTRL1 masks
#define LIS2DS12_ODR_MASK 				0xF0
#define LIS2DS12_FS_MASK 				0x0C
#define LIS2DS12_HF_ODR_MASK 			0x02
#define LIS2DS12_BDU_MASK 				0x01

#define LIS2DS12_PowerDown				0x00
/* resolution : 10bit */
#define LIS2DS12_ODR_LP_1Hz				0x80
#define LIS2DS12_ODR_LP_12q5Hz			0x90
#define LIS2DS12_ODR_LP_25Hz			0xA0
#define LIS2DS12_ODR_LP_50Hz			0xB0
#define LIS2DS12_ODR_LP_100Hz			0xC0
#define LIS2DS12_ODR_LP_200Hz			0xD0
#define LIS2DS12_ODR_LP_400Hz			0xE0
#define LIS2DS12_ODR_LP_800Hz			0xF0
/* resolution : 14bit HF_ODR = 0 */
#define LIS2DS12_ODR_HR_12q5Hz			0x10
#define LIS2DS12_ODR_HR_25Hz			0x20
#define LIS2DS12_ODR_HR_50Hz			0x30
#define LIS2DS12_ODR_HR_100Hz			0x40
#define LIS2DS12_ODR_HR_200Hz			0x50
#define LIS2DS12_ODR_HR_400Hz			0x60
#define LIS2DS12_ODR_HR_800Hz			0x70
/* resolution : 12bit HF_ODR = 1 */
#define LIS2DS12_ODR_HF_1600Hz			0x50
#define LIS2DS12_ODR_HF_3200Hz			0x60
#define LIS2DS12_ODR_HF_6400Hz			0x70

#define LIS2DS12_FS_2G					0x00
#define LIS2DS12_FS_16G					0x04
#define LIS2DS12_FS_4G					0x08
#define LIS2DS12_FS_8G					0x0C

// CTRL2 masks
#define LIS2DS12_BOOT_MASK 				0x80
#define LIS2DS12_SOFT_RESET_MASK 		0x40
#define LIS2DS12_FDS_SLOPE_MASK 		0x08
#define LIS2DS12_IF_ADD_INC_MASK 		0x04
#define LIS2DS12_I2C_DISABLE_MASK 		0x02
#define LIS2DS12_SIM_MASK 				0x01

// CTRL3 masks
#define LIS2DS12_ST		 				0xC0
#define LIS2DS12_TAP_X_EN 				0x20
#define LIS2DS12_TAP_Y_EN 				0x10
#define LIS2DS12_TAP_Z_EN 				0x08
#define LIS2DS12_LIR 					0x04
#define LIS2DS12_H_LACTIVE 				0x02
#define LIS2DS12_PP_OD		 			0x01

/* self test mode */
#define LIS2DS12_ST_NORMAL				0x00
#define LIS2DS12_ST_POSITIVE			0x40
#define LIS2DS12_ST_NEGATIVE			0x80
#define LIS2DS12_ST_NOTALLOWED			0xC0

// CTRL4 masks
#define LIS2DS12_INT1_S_TAP				0x40
#define LIS2DS12_INT1_WU 				0x20
#define LIS2DS12_INT1_FF				0x10
#define LIS2DS12_INT1_TAP				0x08
#define LIS2DS12_INT1_6D				0x04
#define LIS2DS12_INT1_FTH 				0x02
#define LIS2DS12_INT1_DRDY 				0x01

// CTRL5 masks
#define LIS2DS12_DRDY_PULSED			0x80
#define LIS2DS12_INT2_BOOT	 			0x40
#define LIS2DS12_INT2_ONINT1			0x20
#define LIS2DS12_INT2_TILT				0x10
#define LIS2DS12_INT2_SIG_MOT_DET		0x08
#define LIS2DS12_INT2_STEP_DET 			0x04
#define LIS2DS12_INT2_FTH				0x02
#define LIS2DS12_INT2_DRDY				0x01

// FIFO_CTRL masks
#define LIS2DS12_FMODE_MASK 			0xE0
#define LIS2DS12_MODULE_TO_FIFO_MASK 	0x08
#define LIS2DS12_IF_CS_PU_DIS_MASK 		0x01

#define LIS2DS12_FMODE_BYPASS				0x00
#define LIS2DS12_FMODE_STOP_ON_FULL			0x01
#define LIS2DS12_FMODE_CONTINUOS_TO_FIFO	0x03
#define LIS2DS12_FMODE_BYPASS_TO_CONTINUOS	0x04
#define LIS2DS12_FMODE_CONTINUOS			0x06

// STATUS masks
//#define LIS2DS12_FIFO_THS 				0x80
#define LIS2DS12_WU_IA		 			0x40
#define LIS2DS12_SLEEP_STATE 			0x20
#define LIS2DS12_DOUBLE_TAP 			0x10
#define LIS2DS12_SINGLE_TAP 			0x08
#define LIS2DS12_6D_IA		 			0x04
#define LIS2DS12_FF_IA	 				0x02
#define LIS2DS12_DRDY		 			0x01

// FIFO_SRC masks
#define LIS2DS12_FTH					0x80
#define LIS2DS12_FIFO_OVR		 		0x40
#define LIS2DS12_DIFF8	 				0x20

// TAP_6D_THS masks
#define LIS2DS12_4D_EN					0x80
#define LIS2DS12_6D_THS_MASK	 		0x60
#define LIS2DS12_TAP_THS_MASK			0x1F

#define LIS2DS12_6D_80					0x00
#define LIS2DS12_6D_70					0x20
#define LIS2DS12_6D_60					0x40
#define LIS2DS12_6D_50					0x60

// INT_DUR masks
#define LIS2DS12_LAT_MASK 				0xF0
#define LIS2DS12_QUIET_MASK 			0x0C
#define LIS2DS12_SHOCK_MASK 			0x03

// WAKE_UP_THS masks
#define LIS2DS12_SINGLE_DOUBLE_TAP		0x80
#define LIS2DS12_SLEEP_ON 				0x40
#define LIS2DS12_WU_THS	 				0x3F

// WAKE_UP_DUR masks
#define LIS2DS12_FF_DUR5				0x80
#define LIS2DS12_WU_DUR_MASK			0x60
#define LIS2DS12_INT1_FSS7				0x10
#define LIS2DS12_SLEEP_DUR_MASK			0x0F

// FREE_FALL masks
#define LIS2DS12_FF_DUR_MASK			0xF8
#define LIS2DS12_FF_THS_MASK			0x07

#define LIS2DS12_FF_THS_5				0x00
#define LIS2DS12_FF_THS_7				0x01
#define LIS2DS12_FF_THS_8				0x02
#define LIS2DS12_FF_THS_10				0x03
#define LIS2DS12_FF_THS_11				0x04
#define LIS2DS12_FF_THS_13				0x05
#define LIS2DS12_FF_THS_15				0x06
#define LIS2DS12_FF_THS_16				0x07

// STATUS_DUP masks
#define LIS2DS12_OVR	 				0x80
#define LIS2DS12_WU_IA		 			0x40
#define LIS2DS12_SLEEP_STATE 			0x20
#define LIS2DS12_DOUBLE_TAP 			0x10
#define LIS2DS12_SINGLE_TAP 			0x08
#define LIS2DS12_6D_IA		 			0x04
#define LIS2DS12_FF_IA	 				0x02
#define LIS2DS12_DRDY		 			0x01

// WAKE_UP_SRC masks
//#define LIS2DS12_FF_IA		 			0x20
#define LIS2DS12_SLEEP_STATE_IA			0x10
//#define LIS2DS12_WU_IA		 			0x08
#define LIS2DS12_X_WU		 			0x04
#define LIS2DS12_Y_WU	 				0x02
#define LIS2DS12_Z_WU		 			0x01

// TAP_SRC masks
#define LIS2DS12_TAP_IA		 			0x40
#define LIS2DS12_SINGLETAP	 			0x20
#define LIS2DS12_DOUBLETAP				0x10
#define LIS2DS12_TAP_SIGN	 			0x08
#define LIS2DS12_X_TAP		 			0x04
#define LIS2DS12_Y_TAP	 				0x02
#define LIS2DS12_Z_TAP		 			0x01

// STEP_COUNTER_MINTHS masks
#define LIS2DS12_RST_nSTEP	 			0x80
#define LIS2DS12_PEDO4g		 			0x40
#define LIS2DS12_SC_MTHS_MASK			0x3F

// FUNC_CK_GATE masks
#define LIS2DS12_TILT_INT	 			0x80
#define LIS2DS12_FS_SRC_MASK		 	0x60
#define LIS2DS12_SIG_MOT_DETECT			0x10
#define LIS2DS12_RST_SIGN_MOT 			0x08
#define LIS2DS12_RST_PEDO	 			0x04
#define LIS2DS12_STEP_DETECT			0x02
#define LIS2DS12_CK_GATE_FUNC 			0x01

#define LIS2DS12_FS_SRC_2G				0x20
#define LIS2DS12_FS_SRC_4G				0x40

// FUNC_SRC masks
#define LIS2DS12_RST_TILT	 			0x40
#define LIS2DS12_MODULE_READY		 	0x20

//FUNC_CTRL masks
#define LIS2DS12_MODULE_ON	 			0x20
#define LIS2DS12_TILT_ON				0x10
#define LIS2DS12_TUD_EN		 			0x08
#define LIS2DS12_MASTER_ON	 			0x04
#define LIS2DS12_SIGN_MOT_ON			0x02
#define LIS2DS12_STEP_CNT_ON 			0x01

#endif	// __LIS2DS12_H__
