/*!******************************************************************************
 * @file	hub_mgr.h
 * @brief	HUB Manager
 * @par		Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUB_MGR_H__
#define __HUB_MGR_H__
#include "sensor_if.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/** @struct hub_mgr_callback_t
 *  @brief Callback function from hub_mgr
 */
typedef struct {
	unsigned int ( *get_ts )( void );
	unsigned int ( *block )( unsigned int wait_for_ts, unsigned int target_output_num );
	int ( *get_cmd )( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params );
	void ( *res_cmd )( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data );
	int ( *output )( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block, int f_notify );
	void ( *set_int_no )( int int_no, int level );
	int ( *set_power_mode )( unsigned int use_mode,  unsigned int gpio_no );
} hub_mgr_callback_t;

/** @name SENSOR_ATTR_FLAGS
 * Attribute to parent sensor
 */
/*@{*/
#define SENSOR_ATTR_FLAGS_SYNC_ACTIVATE				(1<< 0)		/// Let it be sync status of activation to child sensor
#define SENSOR_ATTR_FLAGS_SYNC_INTERVAL				(1<< 1)		/// Let it be sync value of interval which child sensor requested
#define SENSOR_ATTR_FLAGS_NOTIFY_UPDATED_ACTIVATE	(1<< 8)		/// Notify at updated status of activation of it
#define SENSOR_ATTR_FLAGS_NOTIFY_UPDATED_INTERVAL	(1<< 9)		/// Notify at updated value of interval of it
#define SENSOR_ATTR_FLAGS_NOTIFY_UPDATED_DATA		(1<<10)		/// Notify at updated data of it

#define SENSOR_ATTR_FLAGS_SYNC_SENSOR	(SENSOR_ATTR_FLAGS_SYNC_ACTIVATE | SENSOR_ATTR_FLAGS_SYNC_INTERVAL | SENSOR_ATTR_FLAGS_NOTIFY_UPDATED_DATA)
/*@}*/

/** \defgroup GRP_HUB_MANAGER HUB Manager
 *  @{
 */

/**
 * @brief	Initialize HUB Manager.<BR>
 *			This function must be called at the beginning of registration once.
 *
 * @param	[in] pf			Callback function from HUB Manager
 * @param	[in] fifo_addr	The address of Software FIFO
 * @param	[in] fifo_num	Number of wards of free space
 * @retval	0	Success
 * @retval	-1	Param error
 * @retval	-3	Malloc error
 */
int hub_mgr_init( hub_mgr_callback_t *pf, unsigned int *fifo_addr, unsigned int fifo_num );

/**
 * @brief	End HUB Manager.
 *
 * @return void
 */
void hub_mgr_end( void );

/**
 * @brief	Register Sensor Library in HUB Manager.<BR>
 *			HUB Manager manages the Sensor Library that is registered.<BR>
 *			It is necessary to call the function the same times as the number of Sensor Libraries to register.
 *
 * @param	[in] pif Sensor Library Control function
 * @retval	0	Success
 * @retval	-1	Param error
 */
int hub_mgr_regist_sensor( sensor_if_t *pif );

/**
 *	@brief	Fix the registration of Sensor Libraries once.<BR>
 *			Call this function after calling hub_mgr_regist_sensor.
 *
 * @retval	0	Success
 * @retval	-3	Malloc error
 */
int hub_mgr_regist_fix( void );



/**
 *	@brief	Notice the errors that occur when the host.<BR>
 *
 * @retval	none
 */
void hub_mgr_err_notifi( void );


/**
 *	@brief	Notice the errors that occur when the host.<BR>
 *
 * @param	[in] num	data size
 * @param	[in] data	data
 *
 * @retval	none
 */
void hub_data_output( int num, void *data );

/**
 * @brief	Start Sensor Libraries operation by HUB Manager.<BR>
 *			This function is infinite loop processing.
 *
 * @retval	1	End HUB Manager
 * @retval	-1	Param error
 */
int hub_mgr_run( void );

/**
 * @brief	Set attribute to specified depended sensor
 *
 * @param	[in] id			Caller sensor ID
 * @param	[in] par_id		Target sensor ID to activate/deactivate
 * @param	[in] attr		#SENSOR_ATTR_FLAGS
 *
 * @retval	0	Success
 * @retval	-1	Param error
 */
int hub_mgr_set_sensor_attr( unsigned char id, unsigned char par_id, int attr );

/**
 * @def hub_mgr_set_sensor_active(id, par_id, f_active)
 * Activate/Deactivate specified Sensor Library.
 *
 * @param	[in] id			Caller sensor ID
 * @param	[in] par_id		Target sensor ID to activate/deactivate
 * @param	[in] f_active	0:Deactivate the sensor, Other:Activate the sensor
 *
 * @retval	0	Success
 * @retval	-1	Param error
 * */
#define hub_mgr_set_sensor_active(id, par_id, f_active) \
	hub_mgr_set_sensor_attr(id,par_id,SENSOR_ATTR_FLAGS_SYNC_SENSOR&(-(!!f_active)))

/**
 * @brief	Set update interval to specified Sensor Library.<BR>
 * 			HUB Manager gets the value of Sensor Library at this interval.
 *
 * @param	[in] id			Caller sensor ID
 * @param	[in] par_id		Target sensor ID to set interval
 * @param	[in] tick_num	Interval[ms]
 * @retval	0	Success
 * @retval	-1	Param error
 */
int hub_mgr_set_sensor_interval( unsigned char id, unsigned char par_id, int tick_num );


/**
 * @brief	Get interval to specified Sensor Library.<BR>
 *
 * @param	[in] id			Target sensor ID to get interval
 * @retval	0	Interval[ms]
 */
int hub_mgr_get_sensor_interval( unsigned char id );

/**
 * @brief Send command code to Parent Sensor Library.
 *
 * @param [in] id		Caller sensor ID
 * @param [in] par_id	Target sensor ID to send command
 * @param [in] cmd_code	Command code
 * @param [in] param	Command paramaeter
 * @return Response code from parent Sensor Library
 */
int hub_mgr_snd_cmd( unsigned char id, unsigned char par_id, unsigned int cmd_code, void* param );

/**
 * @brief  Wakeup parent sensor to call its notify_ts()
 *
 * @param [in] id		Caller sensor ID
 * @param [in] par_id	Target sensor ID to send command
 *
 * @retval	0	Success
 * @retval	-1	Param error
 */
int hub_mgr_wakeup_sensor( unsigned char id, unsigned char par_id );

/** @} */

/**
 * @brief	Force Stop & End HUB Manager for Debug
 *
 * @return void
 */
void hub_mgr_debug_end( void );

/**
 * @return SW FIFO Count
 */
int hub_mgr_pull_sensor_data( void );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __HUB_MGR_H__ */
